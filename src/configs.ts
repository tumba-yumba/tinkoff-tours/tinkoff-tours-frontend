export const MAX_FILES_PER_TOUR = 10

export const MAX_FILES_PER_REVIEW = 3

export const MAX_FILE_SIZE_MB = 10

export const PAGE_SIZE = 5
