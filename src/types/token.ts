import { Role } from "./user"

export type TokenTypes = "access" | "refresh"
export interface TokenPayload {
    email: string
    role: Role
    token_type: TokenTypes
    id: string
    exp: number
}
