import { DtoGuideOut } from "../store/api/exportedApi"

export type User = {
    data?: DtoGuideOut
    userId: string
    role: Role
    email: string
}

export enum Role {
    "guide" = "guide",
    "user" = "user",
    "admin" = "admin",
}
