import React from "react"
import ReactDOM from "react-dom/client"
import "./index.css"
import store from "./store"
import { Provider } from "react-redux"
import { ChakraProvider, StyleFunctionProps } from "@chakra-ui/react"
import { extendTheme } from "@chakra-ui/react"
import { BrowserRouter } from "react-router-dom"
import "@fontsource/jost/400.css"
import Pages from "./routes/Pages"
import TimeAgo, { LocaleData } from "javascript-time-ago"
import { ruDays } from "./utils/customTimeFromat"
import SwiperCore, { Navigation, Pagination, Controller } from "swiper"
import "swiper/swiper-bundle.css"

SwiperCore.use([Navigation, Pagination, Controller])

TimeAgo.addDefaultLocale(ruDays as LocaleData)

const theme = extendTheme({
    components: {
        Button: {
            baseStyle: {
                color: "#695B12",
            },
            variants: {
                solid: (props: StyleFunctionProps) => ({
                    color:
                        props.colorScheme === "tinkoff"
                            ? "#695B12"
                            : props.colorScheme === "gray"
                            ? "#6E6E6C"
                            : "white",
                }),
            },
            defaultProps: {
                colorScheme: "tinkoff",
            },
        },
    },
    fonts: {
        body: `'Jost', sans-serif`,
    },
    colors: {
        tinkoff: {
            50: "#fffcef",
            100: "#fff9da",
            200: "#fff5c6",
            300: "#ffed93",
            400: "#ffe460",
            500: "#FFDD2D",
            600: "#f9cf00",
            700: "#c6a500",
            800: "#937a00",
            900: "#605000",
        },
        purpleT: {
            50: "#f6f4fc",
            100: "#f6f4fc",
            200: "#e2dbf6",
            300: "#c2b2ec",
            400: "#a18ae1",
            500: "#8061D7",
            600: "#6038cd",
            700: "#4c2aa8",
            800: "#39207f",
            900: "#271656",
        },
    },
})

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement)

root.render(
    <React.StrictMode>
        <ChakraProvider theme={theme}>
            <Provider store={store}>
                <BrowserRouter>
                    <Pages />
                </BrowserRouter>
            </Provider>
        </ChakraProvider>
    </React.StrictMode>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
