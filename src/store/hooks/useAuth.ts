import { useMemo } from "react"
import { useSelector } from "react-redux"
import { selectCurrentUser, selectUserId } from "../slices/authSlice"

export const useAuth = () => {
    const user = useSelector(selectCurrentUser)

    return useMemo(() => ({ user }), [user])
}

export const useUserId = () => {
    const userId = useSelector(selectUserId)

    return useMemo(() => ({ userId }), [userId])
}
