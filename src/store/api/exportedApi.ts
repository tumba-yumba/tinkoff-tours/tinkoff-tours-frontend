import { emptySplitApi as api } from "./emptyApi"
const injectedRtkApi = api.injectEndpoints({
    endpoints: (build) => ({
        postAuthLogin: build.mutation<
            PostAuthLoginApiResponse,
            PostAuthLoginApiArg
        >({
            query: (queryArg) => ({
                url: `/auth/login`,
                method: "POST",
                body: queryArg.dtoLoginIn,
            }),
        }),
        postAuthRefresh: build.mutation<
            PostAuthRefreshApiResponse,
            PostAuthRefreshApiArg
        >({
            query: (queryArg) => ({
                url: `/auth/refresh`,
                method: "POST",
                body: queryArg.dtoTokenIn,
            }),
        }),
        postAuthRegister: build.mutation<
            PostAuthRegisterApiResponse,
            PostAuthRegisterApiArg
        >({
            query: (queryArg) => ({
                url: `/auth/register`,
                method: "POST",
                body: queryArg.dtoRegistrationIn,
            }),
        }),
        deleteBookingsById: build.mutation<
            DeleteBookingsByIdApiResponse,
            DeleteBookingsByIdApiArg
        >({
            query: (queryArg) => ({
                url: `/bookings/${queryArg.id}`,
                method: "DELETE",
            }),
        }),
        postFeedbacksPhotos: build.mutation<
            PostFeedbacksPhotosApiResponse,
            PostFeedbacksPhotosApiArg
        >({
            query: (queryArg) => ({
                url: `/feedbacks/photos`,
                method: "POST",
                body: queryArg.body,
            }),
        }),
        deleteFeedbacksPhotos: build.mutation<
            DeleteFeedbacksPhotosApiResponse,
            DeleteFeedbacksPhotosApiArg
        >({
            query: (queryArg) => ({
                url: `/feedbacks/photos`,
                method: "DELETE",
                body: queryArg.dtoFeedbackPhotoIn,
            }),
        }),
        getGuide: build.query<GetGuideApiResponse, GetGuideApiArg>({
            query: () => ({ url: `/guide` }),
        }),
        putGuide: build.mutation<PutGuideApiResponse, PutGuideApiArg>({
            query: (queryArg) => ({
                url: `/guide`,
                method: "PUT",
                body: queryArg.dtoGuideIn,
            }),
        }),
        postGuideAvatar: build.mutation<
            PostGuideAvatarApiResponse,
            PostGuideAvatarApiArg
        >({
            query: (queryArg) => ({
                url: `/guide/avatar`,
                method: "POST",
                body: queryArg.body,
            }),
        }),
        deleteGuideAvatar: build.mutation<
            DeleteGuideAvatarApiResponse,
            DeleteGuideAvatarApiArg
        >({
            query: () => ({ url: `/guide/avatar`, method: "DELETE" }),
        }),
        getGuideTours: build.query<
            GetGuideToursApiResponse,
            GetGuideToursApiArg
        >({
            query: (queryArg) => ({
                url: `/guide/tours`,
                params: { limit: queryArg.limit, offset: queryArg.offset },
            }),
        }),
        getGuidesById: build.query<
            GetGuidesByIdApiResponse,
            GetGuidesByIdApiArg
        >({
            query: (queryArg) => ({ url: `/guides/${queryArg.id}` }),
        }),
        getGuidesByIdEstimations: build.query<
            GetGuidesByIdEstimationsApiResponse,
            GetGuidesByIdEstimationsApiArg
        >({
            query: (queryArg) => ({
                url: `/guides/${queryArg.id}/estimations`,
            }),
        }),
        getGuidesByIdTours: build.query<
            GetGuidesByIdToursApiResponse,
            GetGuidesByIdToursApiArg
        >({
            query: (queryArg) => ({
                url: `/guides/${queryArg.id}/tours`,
                params: { limit: queryArg.limit, offset: queryArg.offset },
            }),
        }),
        getModerationFeedbacks: build.query<
            GetModerationFeedbacksApiResponse,
            GetModerationFeedbacksApiArg
        >({
            query: () => ({ url: `/moderation/feedbacks` }),
        }),
        getModerationGuides: build.query<
            GetModerationGuidesApiResponse,
            GetModerationGuidesApiArg
        >({
            query: (queryArg) => ({
                url: `/moderation/guides`,
                params: { limit: queryArg.limit, offset: queryArg.offset },
            }),
        }),
        postModerationGuidesById: build.mutation<
            PostModerationGuidesByIdApiResponse,
            PostModerationGuidesByIdApiArg
        >({
            query: (queryArg) => ({
                url: `/moderation/guides/${queryArg.id}`,
                method: "POST",
                body: queryArg.dtoModerateIn,
            }),
        }),
        getModerationTours: build.query<
            GetModerationToursApiResponse,
            GetModerationToursApiArg
        >({
            query: (queryArg) => ({
                url: `/moderation/tours`,
                params: { limit: queryArg.limit, offset: queryArg.offset },
            }),
        }),
        postModerationToursById: build.mutation<
            PostModerationToursByIdApiResponse,
            PostModerationToursByIdApiArg
        >({
            query: (queryArg) => ({
                url: `/moderation/tours/${queryArg.id}`,
                method: "POST",
                body: queryArg.dtoModerateIn,
            }),
        }),
        getTags: build.query<GetTagsApiResponse, GetTagsApiArg>({
            query: () => ({ url: `/tags` }),
        }),
        getTours: build.query<GetToursApiResponse, GetToursApiArg>({
            query: (queryArg) => ({
                url: `/tours`,
                params: {
                    city: queryArg.city,
                    country: queryArg.country,
                    end_date: queryArg.endDate,
                    end_time: queryArg.endTime,
                    limit: queryArg.limit,
                    offset: queryArg.offset,
                    participants: queryArg.participants,
                    price_limit: queryArg.priceLimit,
                    start_date: queryArg.startDate,
                    start_time: queryArg.startTime,
                    tags: queryArg.tags,
                },
            }),
        }),
        postTours: build.mutation<PostToursApiResponse, PostToursApiArg>({
            query: (queryArg) => ({
                url: `/tours`,
                method: "POST",
                body: queryArg.dtoTourCreateIn,
            }),
        }),
        postToursPhotos: build.mutation<
            PostToursPhotosApiResponse,
            PostToursPhotosApiArg
        >({
            query: (queryArg) => ({
                url: `/tours/photos`,
                method: "POST",
                body: queryArg.body,
            }),
        }),
        deleteToursPhotos: build.mutation<
            DeleteToursPhotosApiResponse,
            DeleteToursPhotosApiArg
        >({
            query: (queryArg) => ({
                url: `/tours/photos`,
                method: "DELETE",
                body: queryArg.dtoPhotoIn,
            }),
        }),
        getToursById: build.query<GetToursByIdApiResponse, GetToursByIdApiArg>({
            query: (queryArg) => ({ url: `/tours/${queryArg.id}` }),
        }),
        putToursById: build.mutation<
            PutToursByIdApiResponse,
            PutToursByIdApiArg
        >({
            query: (queryArg) => ({
                url: `/tours/${queryArg.id}`,
                method: "PUT",
                body: queryArg.dtoTourCreateIn,
            }),
        }),
        deleteToursById: build.mutation<
            DeleteToursByIdApiResponse,
            DeleteToursByIdApiArg
        >({
            query: (queryArg) => ({
                url: `/tours/${queryArg.id}`,
                method: "DELETE",
            }),
        }),
        getToursByIdEstimations: build.query<
            GetToursByIdEstimationsApiResponse,
            GetToursByIdEstimationsApiArg
        >({
            query: (queryArg) => ({ url: `/tours/${queryArg.id}/estimations` }),
        }),
        getToursByIdFeedbacks: build.query<
            GetToursByIdFeedbacksApiResponse,
            GetToursByIdFeedbacksApiArg
        >({
            query: (queryArg) => ({ url: `/tours/${queryArg.id}/feedbacks` }),
        }),
        postToursByIdFeedbacks: build.mutation<
            PostToursByIdFeedbacksApiResponse,
            PostToursByIdFeedbacksApiArg
        >({
            query: (queryArg) => ({
                url: `/tours/${queryArg.id}/feedbacks`,
                method: "POST",
                body: queryArg.dtoFeedbackIn,
                params: { booking_id: queryArg.bookingId },
            }),
        }),
        getToursByTourIdTimesAndTimeIdBookings: build.query<
            GetToursByTourIdTimesAndTimeIdBookingsApiResponse,
            GetToursByTourIdTimesAndTimeIdBookingsApiArg
        >({
            query: (queryArg) => ({
                url: `/tours/${queryArg.tourId}/times/${queryArg.timeId}/bookings`,
                params: { limit: queryArg.limit, offset: queryArg.offset },
            }),
        }),
        postToursByTourIdTimesAndTimeIdBookings: build.mutation<
            PostToursByTourIdTimesAndTimeIdBookingsApiResponse,
            PostToursByTourIdTimesAndTimeIdBookingsApiArg
        >({
            query: (queryArg) => ({
                url: `/tours/${queryArg.tourId}/times/${queryArg.timeId}/bookings`,
                method: "POST",
                body: queryArg.dtoBookingCredentials,
            }),
        }),
        getToursByTourIdTimesAndTimeIdBookingsCount: build.query<
            GetToursByTourIdTimesAndTimeIdBookingsCountApiResponse,
            GetToursByTourIdTimesAndTimeIdBookingsCountApiArg
        >({
            query: (queryArg) => ({
                url: `/tours/${queryArg.tourId}/times/${queryArg.timeId}/bookings/count`,
            }),
        }),
    }),
    overrideExisting: false,
})
export { injectedRtkApi as exportedApi }
export type PostAuthLoginApiResponse = /** status 200 OK */ DtoTokensOut
export type PostAuthLoginApiArg = {
    /** User credentials */
    dtoLoginIn: DtoLoginIn
}
export type PostAuthRefreshApiResponse = /** status 200 OK */ DtoTokensOut
export type PostAuthRefreshApiArg = {
    /** Refresh tokens */
    dtoTokenIn: DtoTokenIn
}
export type PostAuthRegisterApiResponse = /** status 200 OK */ DtoTokensOut
export type PostAuthRegisterApiArg = {
    /** User credentials */
    dtoRegistrationIn: DtoRegistrationIn
}
export type DeleteBookingsByIdApiResponse =
    /** status 200 OK */ ResponsesSuccessOk
export type DeleteBookingsByIdApiArg = {
    /** Booking ID */
    id: string
}
export type PostFeedbacksPhotosApiResponse =
    /** status 200 OK */ DtoFeedbackPhotoOut
export type PostFeedbacksPhotosApiArg = {
    body: {
        photo: Blob
    }
}
export type DeleteFeedbacksPhotosApiResponse =
    /** status 200 OK */ ResponsesSuccessOk
export type DeleteFeedbacksPhotosApiArg = {
    /** Photo Data */
    dtoFeedbackPhotoIn: DtoFeedbackPhotoIn
}
export type GetGuideApiResponse = /** status 200 OK */ DtoSelfGuideOut
export type GetGuideApiArg = void
export type PutGuideApiResponse = /** status 200 OK */ DtoGuideOut
export type PutGuideApiArg = {
    /** Updated Fields */
    dtoGuideIn: DtoGuideIn
}
export type PostGuideAvatarApiResponse = /** status 200 OK */ DtoAvatarOut
export type PostGuideAvatarApiArg = {
    body: {
        avatar: Blob
    }
}
export type DeleteGuideAvatarApiResponse = /** status 200 OK */ DtoAvatarOut
export type DeleteGuideAvatarApiArg = void
export type GetGuideToursApiResponse = /** status 200 OK */ DtoToursListOut
export type GetGuideToursApiArg = {
    limit: number
    offset: number
}
export type GetGuidesByIdApiResponse = /** status 200 OK */ DtoGuideOut
export type GetGuidesByIdApiArg = {
    /** Guide ID */
    id: string
}
export type GetGuidesByIdEstimationsApiResponse =
    /** status 200 OK */ DtoGuideEstimationsOut
export type GetGuidesByIdEstimationsApiArg = {
    /** Guide ID */
    id: string
}
export type GetGuidesByIdToursApiResponse = /** status 200 OK */ DtoToursListOut
export type GetGuidesByIdToursApiArg = {
    /** Guide ID */
    id: string
    limit: number
    offset: number
}
export type GetModerationFeedbacksApiResponse =
    /** status 200 OK */ DtoFeedbackListOut
export type GetModerationFeedbacksApiArg = void
export type GetModerationGuidesApiResponse =
    /** status 200 OK */ DtoGuidesListOut
export type GetModerationGuidesApiArg = {
    limit: number
    offset: number
}
export type PostModerationGuidesByIdApiResponse =
    /** status 200 OK */ ResponsesSuccessOk
export type PostModerationGuidesByIdApiArg = {
    /** Guide ID */
    id: string
    /** Moderate guide */
    dtoModerateIn: DtoModerateIn
}
export type GetModerationToursApiResponse = /** status 200 OK */ DtoToursListOut
export type GetModerationToursApiArg = {
    limit: number
    offset: number
}
export type PostModerationToursByIdApiResponse =
    /** status 200 OK */ ResponsesSuccessOk
export type PostModerationToursByIdApiArg = {
    /** Tour ID */
    id: string
    /** Moderate tour */
    dtoModerateIn: DtoModerateIn
}
export type GetTagsApiResponse = /** status 200 OK */ DtoTagsOut
export type GetTagsApiArg = void
export type GetToursApiResponse = /** status 200 OK */ DtoToursListOut
export type GetToursApiArg = {
    city?: string
    country?: string
    endDate?: string
    endTime?: string
    limit: number
    offset: number
    participants?: number
    priceLimit?: number
    startDate?: string
    startTime?: string
    tags?: string[]
}
export type PostToursApiResponse = /** status 200 OK */ DtoTourOut
export type PostToursApiArg = {
    /** Tour data */
    dtoTourCreateIn: DtoTourCreateIn
}
export type PostToursPhotosApiResponse = /** status 200 OK */ DtoPhotoOut
export type PostToursPhotosApiArg = {
    body: {
        photo: Blob
    }
}
export type DeleteToursPhotosApiResponse =
    /** status 200 OK */ ResponsesSuccessOk
export type DeleteToursPhotosApiArg = {
    /** Photo Data */
    dtoPhotoIn: DtoPhotoIn
}
export type GetToursByIdApiResponse = /** status 200 OK */ DtoTourOut
export type GetToursByIdApiArg = {
    /** Tour ID */
    id: string
}
export type PutToursByIdApiResponse = /** status 200 OK */ ResponsesSuccessOk
export type PutToursByIdApiArg = {
    /** Tour ID */
    id: string
    /** Tour data */
    dtoTourCreateIn: DtoTourCreateIn
}
export type DeleteToursByIdApiResponse = /** status 200 OK */ ResponsesSuccessOk
export type DeleteToursByIdApiArg = {
    /** Tour ID */
    id: string
}
export type GetToursByIdEstimationsApiResponse =
    /** status 200 OK */ DtoTourEstimationsOut
export type GetToursByIdEstimationsApiArg = {
    /** Tour ID */
    id: string
}
export type GetToursByIdFeedbacksApiResponse =
    /** status 200 OK */ DtoFeedbackListOut
export type GetToursByIdFeedbacksApiArg = {
    /** Tour ID */
    id: string
}
export type PostToursByIdFeedbacksApiResponse =
    /** status 200 OK */ ResponsesSuccessOk
export type PostToursByIdFeedbacksApiArg = {
    /** Tour ID */
    id: string
    /** Booking ID */
    bookingId: string
    /** Feedback Data */
    dtoFeedbackIn: DtoFeedbackIn
}
export type GetToursByTourIdTimesAndTimeIdBookingsApiResponse =
    /** status 200 OK */ DtoBookingListOut
export type GetToursByTourIdTimesAndTimeIdBookingsApiArg = {
    /** Tour ID */
    tourId: string
    /** Time ID */
    timeId: string
    limit: number
    offset: number
}
export type PostToursByTourIdTimesAndTimeIdBookingsApiResponse =
    /** status 200 OK */ ResponsesSuccessOk
export type PostToursByTourIdTimesAndTimeIdBookingsApiArg = {
    /** Tour ID */
    tourId: string
    /** Time ID */
    timeId: string
    /** Booking Credentials */
    dtoBookingCredentials: DtoBookingCredentials
}
export type GetToursByTourIdTimesAndTimeIdBookingsCountApiResponse =
    /** status 200 OK */ DtoBookingCountOut
export type GetToursByTourIdTimesAndTimeIdBookingsCountApiArg = {
    /** Tour ID */
    tourId: string
    /** Time ID */
    timeId: string
}
export type DtoTokensOut = {
    access_token?: string
    refresh_token?: string
}
export type DtoLoginIn = {
    email: string
    password: string
}
export type DtoTokenIn = {
    refresh_token: string
}
export type DtoRegistrationIn = {
    email: string
    first_name: string
    last_name: string
    password: string
}
export type ResponsesSuccessOk = {
    success?: string
}
export type DtoFeedbackPhotoOut = {
    id?: string
    path?: string
}
export type DtoFeedbackPhotoIn = {
    id?: string
}
export type DtoSelfGuideOut = {
    avatar?: string
    bio?: string
    created_at?: string
    email?: string
    first_name?: string
    id?: string
    last_name?: string
    moderate_status?: string
}
export type DtoGuideOut = {
    avatar?: string
    bio?: string
    created_at?: string
    first_name?: string
    id?: string
    last_name?: string
    moderate_status?: string
}
export type DtoGuideIn = {
    bio?: string
    first_name?: string
    last_name?: string
}
export type DtoAvatarOut = {
    id?: string
    path?: string
}
export type DtoTourInListOut = {
    city?: string
    country?: string
    end_time?: string
    id?: string
    photo?: string
    price?: number
    short_description?: string
    start_time?: string
    title?: string
}
export type DtoToursListOut = {
    count?: number
    items?: DtoTourInListOut[]
}
export type DtoEstimationsCountOut = {
    count?: number
}
export type DtoGuideEstimationsOut = {
    boring?: DtoEstimationsCountOut
    entertainment?: DtoEstimationsCountOut
    guide_rating?: number
    interesting?: DtoEstimationsCountOut
    know_everything?: DtoEstimationsCountOut
    know_nothing?: DtoEstimationsCountOut
    not_heard?: DtoEstimationsCountOut
}
export type DtoFeedbackInList = {
    adrenaline?: boolean
    bad_weather?: boolean
    comfortable?: boolean
    comment?: string
    created_at?: string
    id?: string
    name?: string
    not_impressed?: boolean
    photos?: string[]
    picturesque?: boolean
    tour_rating?: number
    uncomfortable?: boolean
}
export type DtoFeedbackListOut = {
    items?: DtoFeedbackInList[]
}
export type DtoGuidesListOut = {
    count?: number
    items?: DtoGuideOut[]
}
export type DtoModerateIn = {
    comment?: string
    status: "approved" | "rejected"
}
export type DtoTagOut = {
    id?: string
    title?: string
}
export type DtoTagsOut = {
    tags?: DtoTagOut[]
}
export type DtoPhotoOut = {
    id?: string
    path?: string
}
export type DtoTourTagOut = {
    id?: string
    title?: string
}
export type DtoTourTimeOut = {
    end_time?: string
    id?: string
    start_time?: string
}
export type DtoTourOut = {
    country?: string
    description?: string
    email_message?: string
    guide_id?: string
    id?: string
    max_participants?: number
    moderation_status?: string
    photos?: DtoPhotoOut[]
    price?: number
    region?: string
    short_description?: string
    tags?: DtoTourTagOut[]
    times?: DtoTourTimeOut[]
    title?: string
}
export type DtoPhotoIn = {
    id: string
}
export type DtoTagIn = {
    id: string
}
export type DtoTimeIn = {
    end_time: string
    start_time: string
}
export type DtoTourCreateIn = {
    city: string
    country: string
    description: string
    email_message: string
    max_participants: number
    photos?: DtoPhotoIn[]
    price: number
    short_description: string
    tags?: DtoTagIn[]
    times: DtoTimeIn[]
    title: string
}
export type DtoTourEstimationsOut = {
    adrenaline?: DtoEstimationsCountOut
    bad_weather?: DtoEstimationsCountOut
    comfortable?: DtoEstimationsCountOut
    not_impressed?: DtoEstimationsCountOut
    picturesque?: DtoEstimationsCountOut
    tour_rating?: number
    uncomfortable?: DtoEstimationsCountOut
}
export type DtoEstimationsIn = {
    adrenaline?: boolean
    bad_weather?: boolean
    boring?: boolean
    comfortable?: boolean
    entertainment?: boolean
    interesting?: boolean
    know_everything?: boolean
    know_nothing?: boolean
    not_heard?: boolean
    not_impressed?: boolean
    picturesque?: boolean
    uncomfortable?: boolean
}
export type DtoFeedbackIn = {
    comment?: string
    estimations?: DtoEstimationsIn
    guide_rating?: number
    photos?: DtoFeedbackPhotoIn[]
    tour_rating?: number
}
export type DtoBookingInListOut = {
    email?: string
    name?: string
    participants_count?: number
}
export type DtoBookingListOut = {
    count?: number
    items?: DtoBookingInListOut[]
}
export type DtoBookingCredentials = {
    email?: string
    name?: string
    participants_count?: number
}
export type DtoBookingCountOut = {
    count?: number
}
export const {
    usePostAuthLoginMutation,
    usePostAuthRefreshMutation,
    usePostAuthRegisterMutation,
    useDeleteBookingsByIdMutation,
    usePostFeedbacksPhotosMutation,
    useDeleteFeedbacksPhotosMutation,
    useGetGuideQuery,
    usePutGuideMutation,
    usePostGuideAvatarMutation,
    useDeleteGuideAvatarMutation,
    useGetGuideToursQuery,
    useGetGuidesByIdQuery,
    useGetGuidesByIdEstimationsQuery,
    useGetGuidesByIdToursQuery,
    useGetModerationFeedbacksQuery,
    useGetModerationGuidesQuery,
    usePostModerationGuidesByIdMutation,
    useGetModerationToursQuery,
    usePostModerationToursByIdMutation,
    useGetTagsQuery,
    useGetToursQuery,
    usePostToursMutation,
    usePostToursPhotosMutation,
    useDeleteToursPhotosMutation,
    useGetToursByIdQuery,
    usePutToursByIdMutation,
    useDeleteToursByIdMutation,
    useGetToursByIdEstimationsQuery,
    useGetToursByIdFeedbacksQuery,
    usePostToursByIdFeedbacksMutation,
    useGetToursByTourIdTimesAndTimeIdBookingsQuery,
    usePostToursByTourIdTimesAndTimeIdBookingsMutation,
    useGetToursByTourIdTimesAndTimeIdBookingsCountQuery,
} = injectedRtkApi
