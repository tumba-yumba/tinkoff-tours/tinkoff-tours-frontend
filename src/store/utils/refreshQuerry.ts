import type {
    BaseQueryFn,
    FetchArgs,
    FetchBaseQueryError,
} from "@reduxjs/toolkit/query"
import { fetchBaseQuery } from "@reduxjs/toolkit/query"
import { logOut, selectRefreshToken, tokenReceived } from "../slices/authSlice"
import { Mutex } from "async-mutex"
import { DtoTokensOut } from "../api/exportedApi"
import { RootState } from "../index"
import { buildParams } from "../../utils/querrySerilize"

// create a new mutex
const mutex = new Mutex()
const baseQuery = fetchBaseQuery({
    baseUrl: "/api/",
    prepareHeaders: (headers, { getState }) => {
        // By default, if we have a token in the store, let's use that for authenticated requests
        const token = (getState() as RootState).auth.token
        if (token) {
            headers.set("authorization", `Bearer ${token}`)
        }
        return headers
    },
    paramsSerializer: buildParams,
})
export const baseQueryWithReauth: BaseQueryFn<
    string | FetchArgs,
    unknown,
    FetchBaseQueryError
> = async (args, api, extraOptions) => {
    // wait until the mutex is available without locking it

    await mutex.waitForUnlock()
    let result: any = await baseQuery(args, api, extraOptions)
    if (
        result.error &&
        (result.error.status === 401 || result.error.originalStatus === 401)
    ) {
        // checking whether the mutex is locked
        const token = selectRefreshToken(api.getState() as RootState)
        if (token && !mutex.isLocked()) {
            const release = await mutex.acquire()
            try {
                const refreshResult = await baseQuery(
                    {
                        url: "/auth/refresh",
                        method: "POST",
                        body: JSON.stringify({ refresh_token: token }),
                    },
                    api,
                    extraOptions
                )
                if (refreshResult.data) {
                    api.dispatch(
                        tokenReceived(refreshResult.data as DtoTokensOut)
                    )
                    // retry the initial query
                    result = await baseQuery(args, api, extraOptions)
                } else {
                    api.dispatch(logOut())
                }
            } finally {
                // release must be called once the mutex should be released again.
                release()
            }
        } else {
            // wait until the mutex is available without locking it
            await mutex.waitForUnlock()
            result = await baseQuery(args, api, extraOptions)
        }
    }
    return result
}
