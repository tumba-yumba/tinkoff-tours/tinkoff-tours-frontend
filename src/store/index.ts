import { configureStore } from "@reduxjs/toolkit"
import { emptySplitApi } from "./api/emptyApi"
import authReducer from "./slices/authSlice"

const store = configureStore({
    reducer: {
        [emptySplitApi.reducerPath]: emptySplitApi.reducer,
        auth: authReducer,
    },
    middleware: (getDefaultMiddleware) => {
        return getDefaultMiddleware().concat(emptySplitApi.middleware)
    },
})
export default store
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
