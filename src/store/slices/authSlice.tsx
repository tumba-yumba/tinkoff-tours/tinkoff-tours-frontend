import type { PayloadAction } from "@reduxjs/toolkit"
import { createSlice } from "@reduxjs/toolkit"
import type { RootState } from "../index"
import jwt_decode from "jwt-decode"
import { DtoGuideOut } from "../api/exportedApi"
import { TokenPayload } from "../../types/token"
import { User } from "../../types/user"

type AuthState = {
    token: string | null
    user?: User
    refreshToken: string | null
}

const slice = createSlice({
    name: "auth",
    initialState: () => {
        try {
            let lastAccess = localStorage.getItem("accessToken")
            let lastRefresh = localStorage.getItem("refreshToken")
            let userData = localStorage.getItem("userData")
            if (!lastAccess || !lastRefresh || !userData) {
                localStorage.clear()
                return { token: null, refreshToken: null } as AuthState
            }
            let decodedToken: TokenPayload = jwt_decode(lastAccess)
            let user = {
                userId: decodedToken.id,
                role: decodedToken.role,
                email: decodedToken.email,
                data: JSON.parse(userData) as DtoGuideOut,
            }
            return {
                userId: decodedToken.id,
                token: lastAccess,
                refreshToken: lastRefresh,
                user: user,
            } as AuthState
        } catch (err) {
            console.error(err)
            localStorage.clear()
            return {
                userId: null,
                token: null,
                refreshToken: null,
            } as AuthState
        }
    },
    reducers: {
        tokenReceived: (
            state,
            {
                payload: { access_token, refresh_token },
            }: PayloadAction<{ access_token?: string; refresh_token?: string }>
        ) => {
            state.token = access_token ?? null
            if (access_token !== undefined)
                localStorage.setItem("accessToken", access_token)
            if (refresh_token !== undefined)
                localStorage.setItem("refreshToken", refresh_token)
            state.refreshToken = refresh_token ?? null
            if (access_token !== undefined) {
                let decodedToken: TokenPayload = jwt_decode(access_token)
                state.user = {
                    userId: decodedToken.id,
                    email: decodedToken.email,
                    role: decodedToken.role,
                    data: state.user?.data,
                }
            } else state.user = undefined
        },
        setUserData: (
            state,
            { payload: { userData } }: PayloadAction<{ userData: DtoGuideOut }>
        ) => {
            if (state.user !== undefined) {
                state.user.data = userData
                localStorage.setItem("userData", JSON.stringify(userData))
            }
        },
        setUserPicture: (
            state,
            { payload: { photo } }: PayloadAction<{ photo: string }>
        ) => {
            if (state.user !== undefined) {
                state.user.data = { ...state.user.data, avatar: photo }
                localStorage.setItem(
                    "userData",
                    JSON.stringify(state.user.data)
                )
            }
        },
        logOut: (state) => {
            console.log("logout")
            localStorage.removeItem("accessToken")
            localStorage.removeItem("refreshToken")
            localStorage.removeItem("userData")
            state.token = null
            state.refreshToken = null
            state.user = undefined
        },
    },
})

export const { tokenReceived, logOut, setUserData, setUserPicture } =
    slice.actions

export default slice.reducer

export const selectCurrentUser = (state: RootState) => state.auth.user
export const selectUserId = (state: RootState) => {
    return state.auth.user?.userId
}
export const selectRefreshToken = (state: RootState) => state.auth.refreshToken
