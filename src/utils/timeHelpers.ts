import { DtoTourTimeOut } from "../store/api/exportedApi"

export function getDurationString(
    startDateStr: string,
    endDateStr: string
): string {
    const startDate = new Date(Date.parse(startDateStr))
    const endDate = new Date(Date.parse(endDateStr))
    const diff = endDate.getTime() - startDate.getTime()
    return new Date(diff).toLocaleTimeString("ru-RU", {
        hour: "2-digit",
        minute: "2-digit",
        timeZone: "UTC",
    })
}
export interface TourTimeInternal {
    timeStart: Date | null
}
interface TourTimeDto {
    start_time: string
    end_time: string
}
export function getInternalTimes(times: DtoTourTimeOut[]): TourTimeInternal[] {
    return times.map((item) => {
        return { timeStart: new Date(item.start_time!) }
    })
}
export function formatToursTimes(
    tourTimes: Array<TourTimeInternal>,
    duration: Date
): Array<TourTimeDto> | null {
    const t = toLocaleString(duration).split(" ")[1]?.split(":")
    if (!t) return null
    const hour = parseInt(t[0])
    const minute = parseInt(t[1])
    return tourTimes.map((tourTime) => {
        const startTime = new Date(tourTime.timeStart!)
        const endTime = new Date(
            tourTime.timeStart!.getTime() +
                hour * 60 * 60 * 1000 +
                minute * 60 * 1000
        )
        return {
            start_time: toLocaleString(startTime),
            end_time: toLocaleString(endTime),
        }
    })
}

export function sameDay(d1: Date, d2: Date): boolean {
    return (
        d1.getFullYear() === d2.getFullYear() &&
        d1.getMonth() === d2.getMonth() &&
        d1.getDate() === d2.getDate()
    )
}

export function sameTime(d1: Date, d2: Date): boolean {
    return (
        d1.getHours() === d2.getHours() && d1.getMinutes() === d2.getMinutes()
    )
}

export function getClosetFutureDate(
    times: DtoTourTimeOut[] | undefined
): DtoTourTimeOut | undefined {
    if (!times) return undefined
    const now = new Date()
    const futureTimes = times.filter((time) => {
        const startTime = new Date(time.start_time!)
        return now < startTime
    })
    const sortedTimes = futureTimes.sort((a, b) => {
        const startTimeA = new Date(a.start_time!)
        const startTimeB = new Date(b.start_time!)
        return startTimeA.getTime() - startTimeB.getTime()
    })
    if (sortedTimes.length === 0) return times[0]
    return sortedTimes[0]
}

export function toLocaleString(date: Date) {
    return date.toLocaleString("sv-SE", {
        year: "numeric",
        month: "numeric",
        day: "numeric",
        hour: "2-digit",
        minute: "2-digit",
    })
}
