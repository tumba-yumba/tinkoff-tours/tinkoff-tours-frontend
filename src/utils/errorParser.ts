const ErrorsMessages: any = {
    UnauthorizedError: "Неверный пароль или email",
    NotFoundError: "Пользователя с таким email не существует",
}

export function getErrorMessage(result: any): string {
    if ("error" in result) {
        const error: any = result.error
        if ("data" in error) {
            if (typeof error.data === "string") return error.data
            if (error.data === null && "status" in error)
                return String(error.status)
            if (
                "type" in error.data &&
                error.data["type"] === "" &&
                error.data.error ===
                    "unauthorized, please check your email or password"
            )
                return ErrorsMessages.UnauthorizedError
            if ("type" in error.data && error.data.type in ErrorsMessages)
                return ErrorsMessages[error.data.type]
            return String(error.data.error)
        } else return String(error.error)
    }
    return ""
}
