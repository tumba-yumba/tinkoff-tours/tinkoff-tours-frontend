export const ruDays = {
    locale: "ru",
    long: {
        year: {
            previous: "1 год",
            current: "1 год",
            next: "в следующем году",
            past: {
                one: "{0} год",
                many: "{0} лет",
                other: "{0} года",
            },
            future: {
                one: "{0} год",
                many: "{0} лет",
                other: "{0} года",
            },
        },
        quarter: {
            previous: "1 квартал",
            current: "1 квартал",
            next: "в следующем квартале",
            past: {
                one: "{0} квартал",
                many: "{0} кварталов",
                other: "{0} квартала",
            },
            future: {
                one: "{0} квартал",
                many: "{0} кварталов",
                other: "{0} квартала",
            },
        },
        month: {
            previous: "1 месяц",
            current: "1 месяц",
            next: "в следующем месяце",
            past: {
                one: "{0} месяц",
                many: "{0} месяцев",
                other: "{0} месяца",
            },
            future: {
                one: "{0} месяц",
                many: "{0} месяцев",
                other: "{0} месяца",
            },
        },
        week: {
            previous: "1 неделю",
            current: "1 неделю",
            next: "на следующей неделе",
            past: {
                one: "{0} неделю",
                many: "{0} недель",
                other: "{0} недели",
            },
            future: {
                one: "{0} неделю",
                many: "{0} недель",
                other: "{0} недели",
            },
        },
        day: {
            previous: "1 день",
            "previous-2": "2 дня",
            current: "1 день",
            next: "завтра",
            "next-2": "послезавтра",
            past: {
                one: "{0} день",
                many: "{0} дней",
                other: "{0} дня",
            },
            future: {
                one: "{0} день",
                many: "{0} дней",
                other: "{0} дня",
            },
        },
        hour: {
            current: "меньше суток",
            past: {
                one: "меньше суток",
                many: "меньше суток",
                other: "меньше суток",
            },
            future: {
                one: "меньше суток",
                many: "меньше суток",
                other: "меньше суток",
            },
        },
        minute: {
            current: "меньше суток",
            past: {
                one: "меньше суток",
                many: "меньше суток",
                other: "меньше суток",
            },
            future: {
                one: "меньше суток",
                many: "меньше суток",
                other: "меньше суток",
            },
        },
        second: {
            current: "меньше суток",
            past: {
                one: "меньше суток",
                many: "меньше суток",
                other: "меньше суток",
            },
            future: {
                one: "меньше суток",
                many: "меньше суток",
                other: "меньше суток",
            },
        },
    },
    short: {
        year: {
            previous: "в прошлом г.",
            current: "в этом г.",
            next: "в след. г.",
            past: {
                many: "{0} л.",
                other: "{0} г.",
            },
            future: {
                many: "{0} л.",
                other: "{0} г.",
            },
        },
        quarter: {
            previous: "последний кв.",
            current: "текущий кв.",
            next: "следующий кв.",
            past: "{0} кв.",
            future: "{0} кв.",
        },
        month: {
            previous: "в прошлом мес.",
            current: "в этом мес.",
            next: "в следующем мес.",
            past: "{0} мес.",
            future: "{0} мес.",
        },
        week: {
            previous: "на прошлой нед.",
            current: "на этой нед.",
            next: "на следующей нед.",
            past: "{0} нед.",
            future: "{0} нед.",
        },
        day: {
            previous: "меньше суток",
            "previous-2": "меньше суток",
            current: "меньше суток",
            next: "меньше суток",
            "next-2": "меньше суток",
            past: "меньше суток",
            future: "меньше суток",
        },
        hour: {
            current: "меньше суток",
            past: "меньше суток",
            future: "меньше суток",
        },
        minute: {
            current: "меньше суток",
            past: "меньше суток",
            future: "меньше суток",
        },
        second: {
            current: "меньше суток",
            past: "меньше суток",
            future: "меньше суток",
        },
    },
    narrow: {
        year: {
            previous: "в пр. г.",
            current: "в эт. г.",
            next: "в сл. г.",
            past: {
                many: "-{0} л.",
                other: "-{0} г.",
            },
            future: {
                many: "+{0} л.",
                other: "+{0} г.",
            },
        },
        quarter: {
            previous: "посл. кв.",
            current: "тек. кв.",
            next: "след. кв.",
            past: "-{0} кв.",
            future: "+{0} кв.",
        },
        month: {
            previous: "в пр. мес.",
            current: "в эт. мес.",
            next: "в след. мес.",
            past: "-{0} мес.",
            future: "+{0} мес.",
        },
        week: {
            previous: "на пр. нед.",
            current: "на эт. нед.",
            next: "на след. нед.",
            past: "-{0} нед.",
            future: "+{0} нед.",
        },
        day: {
            previous: "вчера",
            "previous-2": "позавчера",
            current: "сегодня",
            next: "завтра",
            "next-2": "послезавтра",
            past: "-{0} дн.",
            future: "+{0} дн.",
        },
        hour: {
            current: "в этот час",
            past: "-{0} ч",
            future: "+{0} ч",
        },
        minute: {
            current: "в эту минуту",
            past: "-{0} мин",
            future: "+{0} мин",
        },
        second: {
            current: "сейчас",
            past: "-{0} с",
            future: "+{0} с",
        },
    },
    now: {
        now: {
            current: "меньше суток",
            past: "меньше суток",
            future: "меньше суток",
        },
    },
    mini: {
        year: {
            many: "{0} л",
            other: "{0} г",
        },
        month: "{0} мес",
        week: "{0} нед",
        day: "{0} д",
        hour: "{0} ч",
        minute: "{0} мин",
        second: "{0} с",
        now: "сейчас",
    },
    "short-time": {
        year: {
            many: "{0} л.",
            other: "{0} г.",
        },
        month: "{0} мес.",
        week: "{0} нед.",
        day: {
            one: "{0} д.",
            other: "{0} дн.",
        },
        hour: "{0} ч.",
        minute: "{0} мин.",
        second: "{0} сек.",
    },
    "long-time": {
        year: {
            one: "{0} год",
            many: "{0} лет",
            other: "{0} года",
        },
        month: {
            one: "{0} месяц",
            many: "{0} месяцев",
            other: "{0} месяца",
        },
        week: {
            one: "{0} неделю",
            many: "{0} недель",
            other: "{0} недели",
        },
        day: {
            one: "{0} день",
            few: "{0} дня",
            other: "{0} дней",
        },
        hour: {
            one: "меньше суток",
            many: "меньше суток",
            other: "меньше суток",
        },
        minute: {
            one: "меньше суток",
            many: "меньше суток",
            other: "меньше суток",
        },
        second: {
            one: "меньше суток",
            many: "меньше суток",
            other: "меньше суток",
        },
    },
}
