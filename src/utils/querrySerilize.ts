import { SearchParams } from "../components/navigation/ToursSearch"
import { FilterParams } from "../components/navigation/ToursFilter"

export function querySerialize(obj: any): string {
    let str = []
    for (let p in obj)
        if (obj.hasOwnProperty(p)) {
            str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]))
        }
    return str.join("&")
}

export function queryParse(search: string): any {
    let obj: any = {}
    let pairs = search.slice(1).split("&")
    for (let i in pairs) {
        let split = pairs[i].split("=")
        let result = decodeURIComponent(split[1])
        if (result.includes(",")) {
            obj[decodeURIComponent(split[0])] = result.split(",")
        } else {
            obj[decodeURIComponent(split[0])] = result
        }
    }
    return obj
}

export function queryParseSearchParam(search: string): SearchParams {
    const allPrams = queryParse(search)
    return {
        dateStart: allPrams.dateStart ?? "",
        dateEnd: allPrams.dateEnd ?? "",
        participants: parseInt(allPrams.participants ?? 1),
        city: allPrams.city ?? "",
        country: allPrams.country ?? "",
    }
}

export function queryParseFilterParam(search: string): FilterParams {
    const allPrams = queryParse(search)
    return {
        timeStart: allPrams.timeStart ?? "",
        timeEnd: allPrams.timeEnd ?? "",
        price: parseInt(allPrams.price ?? 10000),
        tags: allPrams.tags ?? [],
    }
}

export function buildParams(params: Record<string, any>): string {
    const para = new URLSearchParams()
    console.log(params)
    for (const paramsKey in params) {
        if (params[paramsKey] === undefined) continue
        if (Array.isArray(params[paramsKey])) {
            params[paramsKey].forEach((item: any) =>
                para.append(paramsKey, item)
            )
        } else {
            para.append(paramsKey, params[paramsKey])
        }
    }
    return para.toString()
}

export function queryParseOffset(search: string): number {
    const allPrams = queryParse(search)
    return allPrams.offset ?? 0
}
