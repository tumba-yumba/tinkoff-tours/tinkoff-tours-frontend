import { Route, Routes } from "react-router-dom"
import Layout from "./Layout"
import LoginPage from "./pages/LoginPage"
import RequireAuth from "../components/auth/RequireAuth"
import ModeratePage from "./pages/ModeratePage"
import ToursPage from "./pages/ToursPage"
import GuidePage from "./pages/GuidePage"
import RequireAdmin from "../components/auth/RequireAdmin"
import Error403 from "./errors/error403page"
import Error404 from "./errors/error404page"
import TourPage from "./pages/TourPage"
import RegisterPage from "./pages/RegisterPage"
import WelcomePage from "./pages/WelcomePage"
import CreateTourPage from "./pages/CreateTourPage"
import ToursModerateList from "../components/moderation/ToursModerateList"
import ProfilesModerateTab from "../components/moderation/ProfilesModerateTab"
import { ReviewModerateTab } from "../components/moderation/ReviewModerateTab"
import { UnBookingPage } from "../components/booking/UnBookingPage"
import EditTourPage from "./pages/EditTourPage"

export default function Pages() {
    return (
        <Routes>
            <Route element={<Layout />}>
                <Route path="/" element={<WelcomePage />} />
                <Route path="/login" element={<LoginPage />} />
                <Route path="/register" element={<RegisterPage />} />
                <Route path="/tours" element={<ToursPage />} />
                <Route path="/tours/:id" element={<TourPage />} />
                <Route
                    path="/tour/edit/:id"
                    element={
                        <RequireAuth>
                            <EditTourPage />
                        </RequireAuth>
                    }
                />
                <Route path="/booking/:id" element={<UnBookingPage />} />
                <Route path="/error403" element={<Error403 />} />
                <Route
                    path="/moderate"
                    element={
                        <RequireAdmin>
                            <ModeratePage />
                        </RequireAdmin>
                    }
                >
                    <Route path={"tours"} element={<ToursModerateList />} />
                    <Route
                        path={"profiles"}
                        element={<ProfilesModerateTab />}
                    />
                    <Route path={"reviews"} element={<ReviewModerateTab />} />
                </Route>
                <Route
                    path="/tour/create"
                    element={
                        <RequireAuth>
                            <CreateTourPage />
                        </RequireAuth>
                    }
                />
                <Route path="/guide/:id" element={<GuidePage />} />
                <Route path="*" element={<Error404 />} />
            </Route>
        </Routes>
    )
}
