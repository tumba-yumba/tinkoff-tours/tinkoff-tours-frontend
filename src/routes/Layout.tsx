import { Outlet } from "react-router-dom"
import { Box } from "@chakra-ui/react"
import React from "react"
import Footer from "../components/navigation/Footer"

function Layout() {
    return (
        <Box
            minHeight="calc(100vh - 5px)"
            display="flex"
            flexDirection="column"
            bg={"tinkoff.50"}
        >
            <Box flexGrow="1">
                <Outlet />
            </Box>
            <Footer />
        </Box>
    )
}

export default Layout
