import { Box } from "@chakra-ui/react"
import Navbar from "../../components/navigation/Navbar"
import ModerateTabsNavigation from "../../components/moderation/ModerateTabsNavigation"
import { Outlet } from "react-router-dom"
import React from "react"

export default function ModeratePage() {
    return (
        <>
            <Navbar />
            <ModerateTabsNavigation />
            <Box p="5">
                <Outlet />
            </Box>
        </>
    )
}
