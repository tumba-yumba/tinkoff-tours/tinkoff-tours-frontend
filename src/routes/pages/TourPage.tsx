import { Box, Center, Spinner } from "@chakra-ui/react"
import { useParams, useLocation, useNavigate } from "react-router-dom"
import Navbar from "../../components/navigation/Navbar"
import SearchNavigate from "../../components/navigation/SearchNavigate"
import { useGetToursByIdQuery } from "../../store/api/exportedApi"
import { useAuth } from "../../store/hooks/useAuth"
import Info404 from "../../components/utils/Info404"
import { Role } from "../../types/user"
import NothingFound from "../../components/utils/NothingFound"
import { Tour } from "../../components/tours/Tour"
import { queryParse } from "../../utils/querrySerilize"
import { ReviewModal } from "../../components/modals/ReviewModal"
import { useState } from "react"

function TourPage() {
    let { id } = useParams()
    const {
        data,
        isLoading,
        refetch: refetchTour,
    } = useGetToursByIdQuery({ id: String(id) })

    const location = useLocation()
    const reviewId: string | undefined = queryParse(location.search).review
    const [isOpenModal, setOpenModal] = useState(reviewId !== undefined)
    const auth = useAuth()
    const navigate = useNavigate()
    const isMyTour = !!data?.guide_id && auth?.user?.userId === data?.guide_id

    return (
        <>
            <Navbar />
            <SearchNavigate />
            {reviewId && data && (
                <ReviewModal
                    isOpen={isOpenModal}
                    reviewId={reviewId}
                    tour={data}
                    onClose={() => {
                        setOpenModal(false)
                        navigate({ search: "" }, { replace: true })
                    }}
                />
            )}
            <Box p={5}>
                {isLoading && (
                    <Center>
                        <Spinner />
                    </Center>
                )}
                {!isLoading &&
                    data &&
                    (isMyTour ||
                        auth?.user?.role === Role.admin ||
                        data.moderation_status === "approved") && (
                        <Tour
                            data={data}
                            id={String(id)}
                            onDoneModerate={refetchTour}
                        />
                    )}
                {!isLoading &&
                    data &&
                    !(
                        isMyTour ||
                        auth?.user?.role === Role.admin ||
                        data.moderation_status === "approved"
                    ) && (
                        <Center>
                            <NothingFound
                                image={"construct"}
                                title={"Экскурсия на модерации"}
                                help={
                                    "Вы сможете записаться на неё, как только её проверят наши модераторы."
                                }
                            />
                        </Center>
                    )}
                {!isLoading && !data && (
                    <Center>
                        <Info404 />
                    </Center>
                )}
            </Box>
        </>
    )
}

export default TourPage
