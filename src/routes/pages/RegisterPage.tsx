import { Link as Href, useNavigate } from "react-router-dom"
import React, { useState } from "react"
import {
    Box,
    Button,
    Center,
    Flex,
    FormControl,
    FormErrorIcon,
    FormErrorMessage,
    FormLabel,
    Hide,
    Input,
    Link,
    Text,
    VStack,
} from "@chakra-ui/react"
import { usePostAuthRegisterMutation } from "../../store/api/exportedApi"
import { setUserData, tokenReceived } from "../../store/slices/authSlice"
import { useDispatch } from "react-redux"
import { getErrorMessage } from "../../utils/errorParser"
import { exportedApi, DtoGuideOut } from "../../store/api/exportedApi"
import jwt_decode from "jwt-decode"
import { TokenPayload } from "../../types/token"
import { BeWithUs } from "../../components/parts/BeWithUs"
import { querySerialize } from "../../utils/querrySerilize"

interface RegisterFormErrors {
    email?: string
    name?: string
    password?: string
    password_repeat?: string
}

function RegisterPage() {
    let navigate = useNavigate()
    const dispatch = useDispatch()
    let [validateErrors, setValidateErrors] = useState<RegisterFormErrors>({})
    let [serverErrors, setServerErrors] = useState("")
    const [registerUser, { isLoading }] = usePostAuthRegisterMutation()
    const [trigger] = exportedApi.endpoints.getGuidesById.useLazyQuery()

    function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault()
        let formData = new FormData(event.currentTarget)
        let email = formData.get("email") as string
        let name = formData.get("username") as string
        let password = formData.get("password") as string
        let password_repeat = formData.get("password_repeat") as string
        let errors: RegisterFormErrors = {}
        if (password !== password_repeat) {
            errors.password_repeat = "Пароли не совпадают"
            errors.password = "Пароли не совпадают"
        }
        if (!email.toLowerCase().match(/^[\w-.]+@([\w-]+\.)+[a-z]{2,3}$/)) {
            errors.email = "Неверный формат email"
        }
        let names = name.split(" ")
        if (
            names.length !== 2 ||
            names[0].length === 0 ||
            names[1].length === 0
        ) {
            errors.name = "Имя и Фамилия - два слова через пробел."
        }
        if (
            errors.email ||
            errors.name ||
            errors.password_repeat ||
            errors.password
        ) {
            setValidateErrors(errors)
            return
        }
        const first_name = names[0]
        const last_name = names[1]
        registerUser({
            dtoRegistrationIn: {
                email: email.toLowerCase(),
                first_name,
                last_name,
                password,
            },
        }).then((result) => {
            setServerErrors(getErrorMessage(result))
            if ("data" in result) {
                setServerErrors("")
                dispatch(tokenReceived(result.data))
                const access_token = result.data.access_token
                if (access_token !== undefined) {
                    let decodedToken: TokenPayload = jwt_decode(access_token)
                    trigger({ id: decodedToken.id }).then(({ data }) => {
                        dispatch(setUserData({ userData: data as DtoGuideOut }))
                        const to = {
                            pathname: `/guide/${decodedToken.id}`,
                            search: querySerialize({ first: true }),
                        }
                        navigate(to, { replace: true })
                    })
                }
            }
        })
    }

    return (
        <Flex direction={["column", "column", "row"]}>
            <Hide below="lg">
                <BeWithUs />
            </Hide>
            <Center
                order={["-1", "-1", "0"]}
                minH="100vh"
                w="100%"
                p="5"
                bg={"tinkoff.500"}
            >
                <VStack bg={"white"} borderRadius={15} p={5}>
                    <Text fontWeight={700} fontSize="2.5em">
                        Регистрация
                    </Text>
                    <Center>
                        <FormControl isInvalid={!!serverErrors} maxWidth="40vw">
                            <FormErrorMessage>
                                <FormErrorIcon />
                                {serverErrors}
                            </FormErrorMessage>
                        </FormControl>
                    </Center>
                    <form
                        onSubmit={handleSubmit}
                        style={{ minWidth: "calc(25vw + 50px)" }}
                    >
                        <FormControl
                            isRequired
                            width="100%"
                            isInvalid={!!validateErrors.email}
                        >
                            <FormLabel>Email</FormLabel>
                            <Input
                                borderRadius={30}
                                type="email"
                                width="100%"
                                name="email"
                                placeholder="example@mail.ru"
                            />
                            <FormErrorMessage>
                                {validateErrors.email}
                            </FormErrorMessage>
                        </FormControl>
                        <FormControl
                            isRequired
                            width="100%"
                            isInvalid={!!validateErrors.name}
                        >
                            <FormLabel mt="3">Имя Фамилия</FormLabel>
                            <Input
                                borderRadius={30}
                                width="100%"
                                name="username"
                                placeholder="Вася Пупкин"
                                maxLength={100}
                            />
                            <FormErrorMessage>
                                {validateErrors.name}
                            </FormErrorMessage>
                        </FormControl>
                        <FormControl
                            isRequired
                            width="100%"
                            isInvalid={!!validateErrors.password}
                        >
                            <FormLabel mt="3">Пароль</FormLabel>
                            <Input
                                borderRadius={30}
                                width="100%"
                                name="password"
                                placeholder="**********"
                                type="password"
                            />
                            <FormErrorMessage>
                                {validateErrors.password}
                            </FormErrorMessage>
                        </FormControl>
                        <FormControl
                            isRequired
                            width="100%"
                            isInvalid={!!validateErrors.password_repeat}
                        >
                            <FormLabel mt="3">Подтвердите пароль</FormLabel>
                            <Input
                                borderRadius={30}
                                width="100%"
                                name="password_repeat"
                                placeholder="**********"
                                type="password"
                            />
                            <FormErrorMessage>
                                {validateErrors.password_repeat}
                            </FormErrorMessage>
                        </FormControl>
                        <Center>
                            <Button
                                borderRadius={30}
                                isLoading={isLoading}
                                colorScheme={"purpleT"}
                                type="submit"
                                width="100%"
                                py="6"
                                mt="5"
                                fontWeight={500}
                                fontSize="1.7em"
                            >
                                Зарегистрироваться
                            </Button>
                        </Center>
                    </form>
                    <Center width={"100%"} pt={5}>
                        <Box>
                            <Text color={"gray.400"}>Уже с нами?</Text>
                        </Box>
                        <Box ml={3}>
                            <Link color="purpleT.500" as={"span"}>
                                <Href to={"/login"}>Войти</Href>{" "}
                            </Link>
                        </Box>
                    </Center>
                </VStack>
            </Center>
        </Flex>
    )
}

export default RegisterPage
