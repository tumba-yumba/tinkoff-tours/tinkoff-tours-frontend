import { Box, Text, Center, WrapItem, Wrap } from "@chakra-ui/react"
import ToursSearch, {
    SearchParams,
} from "../../components/navigation/ToursSearch"
import { useNavigate } from "react-router-dom"
import { querySerialize } from "../../utils/querrySerilize"
import Navbar from "../../components/navigation/Navbar"
import React, { useMemo } from "react"
import { DtoTourInListOut, useGetToursQuery } from "../../store/api/exportedApi"
import PopularTourItem from "../../components/cards/PopularTourItem"
import { toLocaleString } from "../../utils/timeHelpers"

interface PopularBlock {
    tour: DtoTourInListOut
    label: string
}

function WelcomePage() {
    const navigate = useNavigate()
    const { data } = useGetToursQuery({
        offset: 0,
        limit: 30,
        startDate: new Date().toISOString().split("T")[0],
    })
    console.log(toLocaleString(new Date()))
    const foundPopular = useMemo(() => {
        const ans: Array<PopularBlock> = []
        if (!data || !data.items) return ans
        let myTours = data.items
        const countries = new Set(data.items.map((tour) => tour.country))
        countries.forEach((county) => {
            const found = myTours.find((tour) => tour.country === county)
            myTours = myTours.filter((tour) => tour !== found)
            if (found) ans.push({ tour: found, label: `Популярное: ${county}` })
        })
        if (myTours.length > 0)
            ans.push({
                tour: myTours[Math.floor(Math.random() * myTours.length)],
                label: `Популярное`,
            })
        return ans
    }, [data])
    function onSearch(params: SearchParams) {
        navigate({ pathname: "/tours", search: querySerialize(params) })
    }
    return (
        <>
            <Navbar />
            <Center
                bg="tinkoff.500"
                p="5"
                pt="0"
                minHeight={"51vh"}
                flexDirection="column"
            >
                <Text fontSize="2.8em" fontWeight="700" align="center">
                    Поиск интересных экскурсий
                </Text>
                <Text mb="5" fontWeight="600" align="center">
                    Разнообразьте свое путешествие прямо сейчас!
                </Text>
                <Box width="100%">
                    <ToursSearch onSearch={onSearch} />
                </Box>
            </Center>
            <Box minH={"40vh"} bg={"white"} pb={5}>
                <Wrap
                    position={"relative"}
                    justify={"center"}
                    top={[5, "-4vh", "-7vh"]}
                >
                    {foundPopular.map((popular) => (
                        <WrapItem h={"300px"} w={"300px"}>
                            {" "}
                            <PopularTourItem
                                key={popular.label}
                                tour={popular.tour}
                                label={popular.label}
                            />
                        </WrapItem>
                    ))}
                </Wrap>
            </Box>
        </>
    )
}
export default WelcomePage
