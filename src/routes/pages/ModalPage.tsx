import React from "react"
import logo from "../../assets/logo.svg"
import "./App.css"
import { Button, Text, useDisclosure } from "@chakra-ui/react"
import { TModal } from "../../components/modals/TModal"
import { useNavigate } from "react-router-dom"
import Navbar from "../../components/navigation/Navbar"

function ModalPage() {
    const { isOpen, onOpen, onClose } = useDisclosure()
    let navigate = useNavigate()
    const onDone = () => {
        console.log("Lets go!")
        navigate("/tours")
    }
    return (
        <>
            <Navbar />
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo" />
                    <p>Авторские экскурсии</p>
                    <Button size="lg" onClick={onOpen}>
                        Вперёд
                    </Button>
                    <TModal
                        isOpen={isOpen}
                        onClose={onClose}
                        onDone={onDone}
                        withPlane={true}
                        title={"Ваше путешествие вот-вот состоится!"}
                    >
                        <Text align="center">
                            Самое время подумать о культурных развлечениях и
                            выбрать себе подходящую экскурсию.
                        </Text>
                    </TModal>
                </header>
            </div>
        </>
    )
}

export default ModalPage
