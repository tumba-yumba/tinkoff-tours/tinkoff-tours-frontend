import React from "react"
import Navbar from "../../components/navigation/Navbar"
import SearchNavigate from "../../components/navigation/SearchNavigate"
import {
    DtoTourCreateIn,
    DtoTourTimeOut,
    useGetToursByIdQuery,
    usePutToursByIdMutation,
} from "../../store/api/exportedApi"
import TourForm from "../../components/tours/TourForm"
import { Center, Spinner } from "@chakra-ui/react"
import { useParams } from "react-router-dom"
import { useAuth } from "../../store/hooks/useAuth"
import NothingFound from "../../components/utils/NothingFound"

function getTimeDiff(times: DtoTourTimeOut[] | undefined) {
    if (!times) return undefined
    const time = times[0]
    if (!time) return undefined
    const startDate = new Date(Date.parse(time.start_time!))
    const endDate = new Date(Date.parse(time.end_time!))
    const diff = endDate.getTime() - startDate.getTime()
    return new Date(diff + new Date().getTimezoneOffset() * 1000 * 60)
}

export default function EditTourPage() {
    const { id } = useParams<{ id: string }>()
    const auth = useAuth()
    const { data, isLoading, refetch } = useGetToursByIdQuery({
        id: String(id),
    })
    const [editTour, { isLoading: loadingEdit }] = usePutToursByIdMutation()

    const isMyTour = !!data?.guide_id && auth?.user?.userId === data?.guide_id
    function editTourForm(data: DtoTourCreateIn) {
        console.log(data)
        return editTour({ id: String(id), dtoTourCreateIn: data }).then(() => {
            refetch()
            return { data: { id: id } }
        })
    }

    return (
        <>
            <Navbar />
            <SearchNavigate />
            {!isLoading && isMyTour && data && (
                <TourForm
                    actionTour={editTourForm}
                    actionTitle={"Изменить"}
                    title={data.title}
                    country={data.country}
                    city={data.region}
                    times={data.times}
                    duration={getTimeDiff(data.times)}
                    price={data.price}
                    participants={data.max_participants}
                    emailDescription={data.email_message}
                    smallDescription={data.short_description}
                    fullDescription={data.description}
                    tags={data.tags}
                    typeForm={"edit"}
                    uploadedPhotos={data.photos}
                />
            )}
            {(isLoading || loadingEdit) && (
                <Center>
                    <Spinner />
                </Center>
            )}
            {!isMyTour && (
                <Center>
                    <NothingFound
                        image={"construct"}
                        title={"Нет доступа на редактироание"}
                        help={""}
                    />
                </Center>
            )}
        </>
    )
}
