import { Link as Href, useLocation, useNavigate } from "react-router-dom"
import React, { useState } from "react"
import {
    Box,
    Button,
    Center,
    Flex,
    FormControl,
    FormErrorIcon,
    FormErrorMessage,
    FormLabel,
    Hide,
    Input,
    Link,
    Text,
    VStack,
} from "@chakra-ui/react"
import { usePostAuthLoginMutation } from "../../store/api/exportedApi"
import { setUserData, tokenReceived } from "../../store/slices/authSlice"
import { useDispatch } from "react-redux"
import { getErrorMessage } from "../../utils/errorParser"
import { exportedApi, DtoGuideOut } from "../../store/api/exportedApi"
import { BeWithUs } from "../../components/parts/BeWithUs"

function LoginPage() {
    let navigate = useNavigate()
    let location = useLocation()
    const dispatch = useDispatch()
    const [
        loginUser, // This is the mutation trigger
        { isLoading },
    ] = usePostAuthLoginMutation()
    const [trigger] = exportedApi.endpoints.getGuide.useLazyQuery()
    let from = location.state?.from?.pathname || "/"
    let [serverErrors, setServerErrors] = useState("")
    let [validateError, setValidateError] = useState("")

    function handleSubmit(event: React.FormEvent<HTMLFormElement>) {
        event.preventDefault()
        let formData = new FormData(event.currentTarget)
        let email = formData.get("email") as string
        let password = formData.get("password") as string
        if (!email.toLowerCase().match(/^[\w-.]+@([\w-]+\.)+[a-z]{2,3}$/)) {
            setValidateError("Неверный формат email")
            return
        }
        setValidateError("")
        setServerErrors("")
        loginUser({ dtoLoginIn: { email, password } }).then((result) => {
            setServerErrors(getErrorMessage(result))
            if ("data" in result) {
                dispatch(tokenReceived(result.data))
                const access_token = result.data.access_token
                if (access_token !== undefined) {
                    trigger(undefined).then(({ data }) => {
                        dispatch(setUserData({ userData: data as DtoGuideOut }))
                        navigate("/", { replace: true })
                    })
                }
            }
        })
    }

    return (
        <Flex direction={["column", "column", "row"]}>
            <Hide below="lg">
                <BeWithUs />
            </Hide>
            <Center
                order={["-1", "-1", "0"]}
                minH="100vh"
                w="100%"
                p="5"
                bg={"tinkoff.500"}
            >
                <VStack bg={"white"} borderRadius={15} p={5}>
                    <Text fontWeight={700} fontSize="2.5em">
                        Вход
                    </Text>
                    {from !== "/" && (
                        <p>
                            Пожалуйста авторизуйтесь, чтобы открыть страницу{" "}
                            {from}
                        </p>
                    )}
                    <Center>
                        <FormControl
                            isInvalid={!!serverErrors}
                            maxWidth={["90vh", "40vw"]}
                        >
                            <FormErrorMessage>
                                <FormErrorIcon />
                                {serverErrors}
                            </FormErrorMessage>
                        </FormControl>
                    </Center>
                    <form
                        onSubmit={handleSubmit}
                        style={{ minWidth: "calc(25vw + 50px)" }}
                    >
                        <FormControl
                            isRequired
                            width="100%"
                            isInvalid={!!validateError}
                        >
                            <FormLabel>Email</FormLabel>
                            <Input
                                borderRadius={30}
                                type="email"
                                width="100%"
                                name="email"
                                placeholder="example@mail.ru"
                            />
                            <FormErrorMessage>{validateError}</FormErrorMessage>
                        </FormControl>
                        <FormControl isRequired width="100%">
                            <FormLabel mt="3">Пароль</FormLabel>
                            <Input
                                borderRadius={30}
                                width="100%"
                                name="password"
                                placeholder="********"
                                type="password"
                            />
                        </FormControl>
                        <Center>
                            <Button
                                borderRadius={30}
                                fontWeight={500}
                                isLoading={isLoading}
                                colorScheme={"purpleT"}
                                type="submit"
                                px="20"
                                py="6"
                                mt="5"
                                fontSize="1.8em"
                            >
                                Войти
                            </Button>
                        </Center>
                    </form>
                    <Center width={"100%"} pt={10}>
                        <Box>
                            <Text color={"gray.400"}>Ещё не с нами?</Text>
                        </Box>
                        <Box ml={3}>
                            <Link color="purpleT.500" as={"span"}>
                                <Href to={"/register"}>Регистрация</Href>{" "}
                            </Link>
                        </Box>
                    </Center>
                </VStack>
            </Center>
        </Flex>
    )
}

export default LoginPage
