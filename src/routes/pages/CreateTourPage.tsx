import React from "react"
import Navbar from "../../components/navigation/Navbar"
import SearchNavigate from "../../components/navigation/SearchNavigate"
import {
    DtoTourCreateIn,
    usePostToursMutation,
} from "../../store/api/exportedApi"
import TourForm from "../../components/tours/TourForm"

export default function CreateTourPage() {
    const [createTourApi] = usePostToursMutation()

    function createTourAction(data: DtoTourCreateIn) {
        return createTourApi({ dtoTourCreateIn: data })
    }

    return (
        <>
            <Navbar />
            <SearchNavigate />
            <TourForm
                actionTour={createTourAction}
                actionTitle={"Создать"}
                typeForm={"create"}
            />
        </>
    )
}
