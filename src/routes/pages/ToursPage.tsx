import { Box, Center, Spinner, Stack } from "@chakra-ui/react"
import ToursSearch, {
    SearchParams,
} from "../../components/navigation/ToursSearch"
import {
    queryParse,
    queryParseFilterParam,
    queryParseOffset,
    queryParseSearchParam,
    querySerialize,
} from "../../utils/querrySerilize"
import { useLocation, useNavigate } from "react-router-dom"
import React, { useState } from "react"
import NothingFound from "../../components/utils/NothingFound"
import ToursFilter, {
    FilterParams,
} from "../../components/navigation/ToursFilter"
import Navbar from "../../components/navigation/Navbar"
import { useGetToursQuery } from "../../store/api/exportedApi"
import TourItemCard from "../../components/cards/TourItemCard"
import { PAGE_SIZE } from "../../configs"
import { PaginateBlock } from "../../components/navigation/PaginateBlock"

export default function ToursPage() {
    const location = useLocation()
    const navigate = useNavigate()
    const [itemOffset, setItemOffset] = useState(
        queryParseOffset(location.search)
    )
    const [searchParams, setParams] = useState<SearchParams>(
        queryParseSearchParam(location.search)
    )
    const [filterParams, setFilter] = useState<FilterParams>(
        queryParseFilterParam(location.search)
    )
    const { data, isLoading } = useGetToursQuery({
        limit: PAGE_SIZE,
        offset: itemOffset,
        country: searchParams.country ? searchParams.country : undefined,
        city: searchParams.city ? searchParams.city : undefined,
        startDate: searchParams.dateStart
            ? searchParams.dateStart
            : new Date().toISOString().split("T")[0],
        endDate: searchParams.dateEnd ? searchParams.dateEnd : undefined,
        participants: searchParams.participants
            ? searchParams.participants
            : undefined,
        tags: filterParams.tags,
        priceLimit: filterParams.price + 1,
        startTime: filterParams.timeStart ? filterParams.timeStart : undefined,
        endTime: filterParams.timeEnd ? filterParams.timeEnd : undefined,
    })

    function onSearch(params: SearchParams) {
        setParams(params)
        const currentQuery = queryParse(location.search)
        const newQuery = { ...currentQuery, ...params, offset: 0 }
        setItemOffset(0)
        const to = {
            pathname: location.pathname,
            search: querySerialize(newQuery),
        }
        navigate(to, { replace: true })
    }

    function onFilter(params: FilterParams) {
        setFilter(params)
        const currentQuery = queryParse(location.search)
        const newQuery = { ...currentQuery, ...params, offset: 0 }
        setItemOffset(0)
        const to = {
            pathname: location.pathname,
            search: querySerialize(newQuery),
        }
        navigate(to, { replace: true })
    }

    function onPageChange(event: { selected: number }) {
        const newOffset = (event.selected * PAGE_SIZE) % (data?.count ?? 0)
        const currentQuery = queryParse(location.search)
        const newQuery = { ...currentQuery, offset: newOffset }
        const to = {
            pathname: location.pathname,
            search: querySerialize(newQuery),
        }
        navigate(to, { replace: true })
        setItemOffset(newOffset)
    }

    const pageCount = Math.ceil((data?.count ?? 0) / PAGE_SIZE)
    return (
        <>
            <Navbar />
            <Box bg="tinkoff.500" p="5" pt="0">
                <ToursSearch
                    onSearch={onSearch}
                    initParams={searchParams}
                    autoApply={true}
                />
            </Box>
            <Stack direction={["column", "column", "row"]} minH={"90vh"}>
                <Box
                    bg="white"
                    px="10"
                    py="5"
                    m="3"
                    maxWidth={["100%", "100%", "50%", "33%"]}
                    height="100%"
                    borderRadius={"md"}
                >
                    <ToursFilter onApply={onFilter} initParams={filterParams} />
                </Box>
                <Box pt={2} px={2} w={"100%"}>
                    {isLoading && (
                        <Center>
                            <Spinner />
                        </Center>
                    )}
                    {!isLoading &&
                        (!data || !data.items || data.items.length === 0) && (
                            <Center>
                                {" "}
                                <NothingFound
                                    image={"direction"}
                                    help={
                                        "Попробуйте убрать некоторые фильтры, чтобы мы смогли найти для вас интересные экскурсии."
                                    }
                                />
                            </Center>
                        )}
                    {data &&
                        data.items &&
                        data.items.map((tour) => (
                            <TourItemCard
                                key={tour.id}
                                tour={tour}
                                squashOnMd={true}
                            />
                        ))}
                </Box>
            </Stack>
            {isLoading && (
                <Center>
                    <Spinner />
                </Center>
            )}
            <PaginateBlock onPageChange={onPageChange} pageCount={pageCount} />
        </>
    )
}
