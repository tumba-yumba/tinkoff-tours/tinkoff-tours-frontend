import {
    Avatar,
    Box,
    Button,
    ButtonGroup,
    Center,
    Flex,
    FormControl,
    FormErrorMessage,
    Heading,
    Icon,
    Input,
    Popover,
    PopoverArrow,
    PopoverBody,
    PopoverCloseButton,
    PopoverContent,
    PopoverFooter,
    PopoverHeader,
    PopoverTrigger,
    Spacer,
    Spinner,
    Text,
    Textarea,
    useDisclosure,
    useToast,
} from "@chakra-ui/react"

import { AiOutlineFieldTime, AiOutlineClose } from "react-icons/ai"
import { TiTick } from "react-icons/ti"
import { useAuth } from "../../store/hooks/useAuth"
import { Link, useLocation, useNavigate, useParams } from "react-router-dom"
import {
    DtoGuideOut,
    DtoSelfGuideOut,
    useGetGuideQuery,
    useGetGuidesByIdEstimationsQuery,
    useGetGuidesByIdQuery,
    useGetGuidesByIdToursQuery,
    useGetGuideToursQuery,
    usePostGuideAvatarMutation,
    usePostModerationGuidesByIdMutation,
    usePutGuideMutation,
} from "../../store/api/exportedApi"
import NothingFound from "../../components/utils/NothingFound"
import { CheckIcon, CloseIcon, EditIcon } from "@chakra-ui/icons"
import React, { ChangeEvent, useEffect, useMemo, useRef, useState } from "react"
import { getErrorMessage } from "../../utils/errorParser"
import { useDispatch } from "react-redux"
import { setUserData, setUserPicture } from "../../store/slices/authSlice"
import ReactTimeAgo from "react-time-ago"
import Navbar from "../../components/navigation/Navbar"
import SearchNavigate from "../../components/navigation/SearchNavigate"
import { MdAddAPhoto } from "react-icons/md"
import { VALID_PHOTO_TYPES } from "../../data/fileTypes"
import TourItemCard from "../../components/cards/TourItemCard"
import { MAX_FILE_SIZE_MB, PAGE_SIZE } from "../../configs"
import { PaginateBlock } from "../../components/navigation/PaginateBlock"
import Info404 from "../../components/utils/Info404"
import { queryParse } from "../../utils/querrySerilize"
import { Role } from "../../types/user"
import { PointsCountBlock } from "../../components/raiting/PointsCountBlock"
import { SubscribeButton } from "../../components/parts/SubscribeButton"
import { ComplaintButton } from "../../components/parts/ComplaintButton"
import ReactStars from "react-stars"

export default function GuidePage() {
    let { id } = useParams()
    const auth = useAuth()
    const isMyProfile = id !== undefined && auth.user?.userId === id
    const location = useLocation()
    let {
        data: dataAll,
        isLoading: isLoadingAll,
        refetch: refetchAll,
    } = useGetGuidesByIdQuery({ id: id ?? "" }, { skip: isMyProfile })
    let {
        data: dataMy,
        isLoading: isLoadingMy,
        refetch: refetchMy,
    } = useGetGuideQuery(undefined, { skip: !isMyProfile })
    const [itemOffset, setItemOffset] = useState(0)
    const { isOpen, onToggle, onClose } = useDisclosure()
    const [editUser, { isLoading: isUserChanging }] = usePutGuideMutation()

    const getTours:
        | typeof useGetGuidesByIdToursQuery
        | typeof useGetGuideToursQuery = useMemo(() => {
        if (id === auth.user?.userId) return useGetGuideToursQuery
        return useGetGuidesByIdToursQuery
    }, [id, auth.user])
    const { data: guideTours, isLoading: isLoadingTours } = getTours({
        id: id ?? "",
        offset: itemOffset,
        limit: PAGE_SIZE,
    })

    const dispatch = useDispatch()
    const navigate = useNavigate()
    const toast = useToast()
    const [uploadFile] = usePostGuideAvatarMutation()
    const { data: est } = useGetGuidesByIdEstimationsQuery({ id: id ?? "" })
    const [isEditing, setIsEditing] = useState(false)
    const [isFirst, setIsFirst] = useState(false)
    const [newName, setNewName] = useState(``)
    const [nameError, setNameError] = useState(``)
    const inputFileRef = useRef<HTMLInputElement>(null)
    const [newAbout, setNewAbout] = useState(``)
    const [comment, setComment] = useState("")

    const [moderate, { isLoading: isLoadingModerate }] =
        usePostModerationGuidesByIdMutation()
    let guide: DtoGuideOut | DtoSelfGuideOut | undefined = !isMyProfile
        ? dataAll
        : dataMy
    let isLoading = !isMyProfile ? isLoadingAll : isLoadingMy
    let refetch = !isMyProfile ? refetchAll : refetchMy

    function startEdit() {
        if (!isMyProfile) return
        setNewName(`${guide?.first_name} ${guide?.last_name}`)
        setNewAbout(`${guide?.bio}`)
        setNameError("")
        setIsEditing(true)
    }

    useEffect(() => {
        if (!guide) return
        const currentQuery = queryParse(location.search)
        console.log(currentQuery)
        if (currentQuery.first) {
            const to = { pathname: location.pathname, search: "" }
            navigate(to, { replace: true })
            setIsFirst(true)
            startEdit()
        }
    }, [guide])

    function editProfile() {
        setNameError("")
        const words = newName.split(" ")
        if (
            words.length !== 2 ||
            words[0].length === 0 ||
            words[1].length === 0
        ) {
            setNameError("Имя должно состоять из двух слов через пробел.")
            return
        }
        if (
            guide?.first_name === words[0] &&
            guide?.last_name === words[1] &&
            guide?.bio === newAbout
        ) {
            setIsEditing(false)
            return
        }

        editUser({
            dtoGuideIn: {
                bio: newAbout,
                first_name: words[0],
                last_name: words[1],
            },
        })
            .then((result: any) => {
                const error = getErrorMessage(result)
                if (error) throw Error(error)
                if ("data" in result) {
                    dispatch(setUserData({ userData: result.data }))
                    setIsEditing(false)
                    refetch()
                }
            })
            .catch((err) => {
                console.error(err)
                toast({
                    description: String(err),
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "error",
                    isClosable: true,
                })
            })
    }

    function ModerateProfile() {
        const onClick: (status: "approved" | "rejected") => void = (
            status: "approved" | "rejected"
        ) => {
            moderate({ id: id ?? "", dtoModerateIn: { comment, status } })
                .then((resp) => {
                    const error = getErrorMessage(resp)
                    if (error) throw Error(error)
                    toast({
                        title: `Вы изменили статус модерации`,
                        description:
                            status === "approved"
                                ? "Теперь профиль виден всем пользователям"
                                : "Профиль отвергнут",
                        position: "bottom-right",
                        variant: "left-accent",
                        status: "info",
                        isClosable: true,
                    })
                    setComment("")
                    refetch()
                })
                .catch((err) => {
                    console.error(err)
                    toast({
                        title: `Ошибка при смене статуса`,
                        description: String(err),
                        position: "bottom-right",
                        variant: "left-accent",
                        status: "error",
                        isClosable: true,
                    })
                })
        }
        return (
            <Center>
                <Flex
                    direction={"column"}
                    p={2}
                    m={[0, 2, 5]}
                    w={["90%", "95%", "100%"]}
                >
                    <Textarea
                        value={comment}
                        onChange={(t) => setComment(t.target.value)}
                        placeholder={"Коментарий"}
                    />
                    <Flex gap={3} w={"100%"} mb={2} mt={2}>
                        <Button
                            colorScheme={"gray"}
                            w={"50%"}
                            color={"#695B12"}
                            isLoading={isLoadingModerate}
                            onClick={() => onClick("rejected")}
                        >
                            Отклонить
                        </Button>
                        <Button
                            colorScheme={"tinkoff"}
                            w={"50%"}
                            isLoading={isLoadingModerate}
                            onClick={() => onClick("approved")}
                        >
                            Принять
                        </Button>
                    </Flex>
                </Flex>
            </Center>
        )
    }

    function onPageChange(event: { selected: number }) {
        const newOffset =
            (event.selected * PAGE_SIZE) % (guideTours?.count ?? 0)
        setItemOffset(newOffset)
    }
    const pageCount = Math.ceil((guideTours?.count ?? 0) / PAGE_SIZE)

    function profileInfo() {
        function uploadImage(e: ChangeEvent<HTMLInputElement>) {
            const photos = e.target.files
            console.log(photos)
            if (!photos) return
            const photo = photos[0]
            if (!photo) return
            if (photo.size / (1024 * 1024) >= MAX_FILE_SIZE_MB) {
                toast({
                    title: "Файл слишком большой",
                    description: `Размер ${photo.name} больше ${MAX_FILE_SIZE_MB}МБ. Выберите файл поменьше.`,
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "warning",
                    isClosable: true,
                })
                return
            }
            const extension = photo.name.split(".").pop()
            if (
                !extension ||
                VALID_PHOTO_TYPES.indexOf(extension.toLowerCase()) === -1
            ) {
                toast({
                    title: "Не валидный файл",
                    description: `Расширение файла ${photo.name} не валидно. Выберите изображение.`,
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "warning",
                    isClosable: true,
                })
                return
            }
            const uploadData = new FormData()
            uploadData.append("avatar", photo)
            uploadFile({
                body: uploadData as unknown as { avatar: Blob },
            })
                .then((resp: any) => {
                    const error = getErrorMessage(resp)
                    if (error) throw Error(error)
                    toast({
                        title: `Успешная смена аватарки`,
                        position: "bottom-right",
                        variant: "left-accent",
                        status: "info",
                        isClosable: true,
                    })
                    refetch()
                    dispatch(setUserPicture({ photo: resp.data.path }))
                })
                .catch((err) => {
                    console.error(err)
                    toast({
                        title: `Ошибка при загрузке ${photo.name}.`,
                        description: String(err),
                        position: "bottom-right",
                        variant: "left-accent",
                        status: "warning",
                        isClosable: true,
                    })
                })
        }
        const selectFile = () => {
            if (inputFileRef && inputFileRef.current !== null) {
                inputFileRef.current.click()
            }
        }
        return (
            <Flex
                w={"100%"}
                bg={"white"}
                direction={["column", "column", "row"]}
            >
                <Box flexGrow={1}>
                    <input
                        style={{ display: "none" }}
                        type="file"
                        ref={inputFileRef}
                        onChange={uploadImage}
                    />
                    <Flex direction={["column", "column", "row"]} p={2}>
                        <Center>
                            {isMyProfile && isEditing && (
                                <Avatar
                                    bg={"gray.200"}
                                    size="2xl"
                                    src={guide?.avatar}
                                    cursor={"pointer"}
                                    icon={<></>}
                                    onClick={selectFile}
                                >
                                    <Box
                                        bg={"blackAlpha.500"}
                                        position={"absolute"}
                                        p={5}
                                        borderRadius={"50%"}
                                        h={"110px"}
                                        w={"110px"}
                                    >
                                        <Icon as={MdAddAPhoto} />
                                    </Box>
                                </Avatar>
                            )}
                            {!isEditing && (
                                <Avatar
                                    bg={"tinkoff.200"}
                                    color={"#695B12"}
                                    size="2xl"
                                    name={
                                        guide?.first_name +
                                        " " +
                                        guide?.last_name
                                    }
                                    src={guide?.avatar}
                                />
                            )}
                        </Center>
                        <Flex direction={"column"} ml={5} mt={5}>
                            {!isEditing && est && (
                                <ReactStars
                                    count={5}
                                    edit={false}
                                    value={est.guide_rating}
                                    size={40}
                                    half={true}
                                />
                            )}
                            {isMyProfile &&
                                !isEditing &&
                                guide?.moderate_status !== "approved" && (
                                    <Button
                                        size="xs"
                                        w={"120px"}
                                        leftIcon={<EditIcon />}
                                        aria-label={"Редактировать"}
                                        onClick={startEdit}
                                    >
                                        {" "}
                                        Редактировать{" "}
                                    </Button>
                                )}
                            {isMyProfile &&
                                !isEditing &&
                                guide?.moderate_status === "approved" && (
                                    <Popover
                                        returnFocusOnClose={false}
                                        isOpen={isOpen}
                                        onClose={onClose}
                                    >
                                        <PopoverTrigger>
                                            <Button
                                                size="xs"
                                                w={"120px"}
                                                leftIcon={<EditIcon />}
                                                aria-label={"Редактировать"}
                                                onClick={onToggle}
                                            >
                                                {" "}
                                                Редактировать{" "}
                                            </Button>
                                        </PopoverTrigger>
                                        <PopoverContent>
                                            <PopoverHeader fontWeight="semibold">
                                                ВНИМАНИЕ
                                            </PopoverHeader>
                                            <PopoverArrow />
                                            <PopoverCloseButton />
                                            <PopoverBody>
                                                После изменения профиля, он
                                                снова отправится на модерацию.{" "}
                                                <br /> Ваши экскурсии и профиль
                                                не будут доступны для просмотра
                                                другим пользователям до
                                                окончания модерации
                                            </PopoverBody>
                                            <PopoverFooter
                                                display="flex"
                                                justifyContent="flex-end"
                                            >
                                                <ButtonGroup size="sm">
                                                    <Button
                                                        variant="outline"
                                                        colorScheme="gray"
                                                        onClick={onToggle}
                                                    >
                                                        Отмена
                                                    </Button>
                                                    <Button
                                                        variant="outline"
                                                        colorScheme="purple"
                                                        onClick={startEdit}
                                                    >
                                                        Редактировать
                                                    </Button>
                                                </ButtonGroup>
                                            </PopoverFooter>
                                        </PopoverContent>
                                    </Popover>
                                )}
                            {isEditing && !isFirst && (
                                <Flex mb={1}>
                                    <Button
                                        mx={2}
                                        size="xs"
                                        w={"120px"}
                                        leftIcon={<CheckIcon />}
                                        isLoading={isUserChanging}
                                        aria-label={"Сохранить"}
                                        onClick={editProfile}
                                    >
                                        {" "}
                                        Сохранить{" "}
                                    </Button>
                                    <Button
                                        mx={2}
                                        colorScheme={"orange"}
                                        size="xs"
                                        w={"120px"}
                                        leftIcon={<CloseIcon />}
                                        aria-label={"Отменить"}
                                        onClick={() => setIsEditing(false)}
                                    >
                                        {" "}
                                        Отменить{" "}
                                    </Button>
                                </Flex>
                            )}
                            {isEditing && (
                                <FormControl
                                    mb={1}
                                    isRequired
                                    isInvalid={!!nameError}
                                >
                                    <FormErrorMessage mt={0}>
                                        {nameError}
                                    </FormErrorMessage>
                                    <Input
                                        fontWeight={700}
                                        size={"lg"}
                                        value={newName}
                                        maxLength={100}
                                        onChange={(t) =>
                                            setNewName(t.target.value)
                                        }
                                    />
                                </FormControl>
                            )}
                            {!isEditing && (
                                <Text
                                    align={"left"}
                                    fontSize={"3xl"}
                                    fontWeight={700}
                                >
                                    {guide?.first_name} {guide?.last_name}
                                    {(isMyProfile ||
                                        auth.user?.role === Role.admin) &&
                                        guide?.moderate_status ===
                                            "approved" && (
                                            <Icon
                                                ml={1}
                                                boxSize={8}
                                                top={1}
                                                position={"relative"}
                                                color={"green.300"}
                                                as={TiTick}
                                            />
                                        )}
                                    {!isMyProfile && (
                                        <SubscribeButton size={"xl"} />
                                    )}
                                    {!isMyProfile && (
                                        <ComplaintButton size={"xl"} />
                                    )}
                                    {(isMyProfile ||
                                        auth.user?.role === Role.admin) &&
                                        guide?.moderate_status ===
                                            "pending" && (
                                            <Icon
                                                ml={1}
                                                boxSize={8}
                                                top={1}
                                                position={"relative"}
                                                color={"tinkoff.500"}
                                                as={AiOutlineFieldTime}
                                            />
                                        )}
                                    {(isMyProfile ||
                                        auth.user?.role === Role.admin) &&
                                        guide?.moderate_status ===
                                            "rejected" && (
                                            <Icon
                                                ml={1}
                                                boxSize={8}
                                                top={2}
                                                position={"relative"}
                                                color={"red.500"}
                                                as={AiOutlineClose}
                                            />
                                        )}
                                </Text>
                            )}
                            {isMyProfile && (
                                <Text align={"left"}>
                                    Email: {(guide as DtoSelfGuideOut).email}
                                </Text>
                            )}
                            <Text align={"left"}>
                                <strong>На платформе:</strong>{" "}
                                <ReactTimeAgo
                                    date={new Date(guide!.created_at!)}
                                    tooltip={false}
                                    timeStyle={"long-time"}
                                    future={false}
                                />
                            </Text>
                        </Flex>
                        <Spacer />
                        {isMyProfile && !(isEditing && isFirst) && (
                            <Center
                                mr="5"
                                mb={5}
                                order={["-1", "-1", "0"]}
                                alignSelf={["self-end", "self-end", "auto"]}
                            >
                                <Link to={"/tour/create"}>
                                    {" "}
                                    <Button
                                        size={"xl"}
                                        h="50px"
                                        p="5"
                                        my={2}
                                        fontSize={"xl"}
                                    >
                                        Создать экскурсию
                                    </Button>
                                </Link>
                            </Center>
                        )}
                        {isEditing && isFirst && (
                            <Center
                                mr="5"
                                mb={5}
                                order={["-1", "-1", "0"]}
                                alignSelf={["self-end", "self-end", "auto"]}
                            >
                                <Button
                                    size={"xl"}
                                    h="50px"
                                    p="5"
                                    my={2}
                                    fontSize={"xl"}
                                    leftIcon={<CheckIcon />}
                                    isLoading={isUserChanging}
                                    aria-label={"Сохранить"}
                                    onClick={editProfile}
                                >
                                    {" "}
                                    Сохранить{" "}
                                </Button>
                            </Center>
                        )}
                    </Flex>
                    <Text align={"left"} m={"5"} pb={"5"}>
                        <strong>O себе: </strong>
                        {isEditing && (
                            <Textarea
                                value={newAbout}
                                onChange={(e) => setNewAbout(e.target.value)}
                                rows={18}
                                placeholder="Напишите что-нибудь о себе"
                            />
                        )}
                        {!isEditing &&
                            (guide?.bio || (
                                <span
                                    style={{
                                        color: "gray",
                                        fontStyle: "italic",
                                    }}
                                >
                                    Не заполнено
                                </span>
                            ))}
                    </Text>
                </Box>
                {!isMyProfile &&
                    auth.user?.role === Role.admin &&
                    ModerateProfile()}
            </Flex>
        )
    }

    return (
        <>
            <Navbar />
            <SearchNavigate />
            <Box p="5">
                {isLoading && (
                    <Center>
                        <Spinner />
                    </Center>
                )}
                {!isLoading &&
                    guide &&
                    !(
                        isMyProfile ||
                        guide.moderate_status === "approved" ||
                        auth.user?.role === Role.admin
                    ) && (
                        <Center>
                            <NothingFound
                                image={"construct"}
                                title={"Экскурсовод на модерации"}
                                help={
                                    "Подождите пока наши модераторы проверят его."
                                }
                            />
                        </Center>
                    )}

                {!isLoading &&
                    guide &&
                    (isMyProfile ||
                        guide.moderate_status === "approved" ||
                        auth.user?.role === Role.admin) && (
                        <>
                            {profileInfo()}
                            <PointsCountBlock estimationGuide={est} />
                            <Flex
                                alignContent={"center"}
                                direction={"column"}
                                mx={[1, 0, 50, 100]}
                            >
                                {!isLoadingTours &&
                                    (!guideTours ||
                                        guideTours.items!.length === 0) && (
                                        <NothingFound
                                            title={
                                                isEditing
                                                    ? "Заполните профиль"
                                                    : "Нет экскурсий :("
                                            }
                                            image={
                                                isEditing
                                                    ? "question"
                                                    : "adventure"
                                            }
                                            help={
                                                isMyProfile
                                                    ? isEditing
                                                        ? "Самое время добавить вашу фотографию и небольшое описание о себе"
                                                        : "Самое время создать свою первую экскурсию"
                                                    : ""
                                            }
                                        />
                                    )}
                                {isLoadingTours && (
                                    <Center>
                                        <Spinner />
                                    </Center>
                                )}

                                {!isLoadingTours &&
                                    guideTours &&
                                    guideTours.items &&
                                    guideTours.items.length > 0 && (
                                        <Center mt={4} mb={5}>
                                            <Heading size={"2xl"}>
                                                Экскурсии
                                            </Heading>
                                        </Center>
                                    )}

                                {!isLoadingTours &&
                                    guideTours &&
                                    guideTours.items &&
                                    guideTours.items.map((tour) => (
                                        <TourItemCard
                                            key={tour.id}
                                            tour={tour}
                                        />
                                    ))}
                            </Flex>
                            <PaginateBlock
                                onPageChange={onPageChange}
                                pageCount={pageCount}
                            />
                        </>
                    )}
                {!isLoading && !guide && <Info404 />}
            </Box>
        </>
    )
}
