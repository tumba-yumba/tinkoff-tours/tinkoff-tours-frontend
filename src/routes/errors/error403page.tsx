import { Container, Text } from "@chakra-ui/react"
import React from "react"
import Navbar from "../../components/navigation/Navbar"

function Error403() {
    return (
        <>
            <Navbar />
            <Container p={5}>
                <Text size={"lg"} fontWeight={500}>
                    403. Доступ запрещён. Вы не admin.
                </Text>
            </Container>
        </>
    )
}
export default Error403
