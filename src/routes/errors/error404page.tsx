import { Box } from "@chakra-ui/react"
import React from "react"
import Navbar from "../../components/navigation/Navbar"
import Info404 from "../../components/utils/Info404"

function Error404() {
    return (
        <>
            <Navbar />
            <Box p={5} bg={"tinkoff.50"} borderRadius={10} mt={5}>
                <Info404 />
            </Box>
        </>
    )
}

export default Error404
