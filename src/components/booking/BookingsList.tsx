import { DtoTourTimeOut } from "../../store/api/exportedApi"
import { BookingListTable } from "./BookingListTable"
import { useState } from "react"
import { getClosetFutureDate } from "../../utils/timeHelpers"
import { Select } from "@chakra-ui/react"

interface BookingsListProps {
    tourId: string
    times: DtoTourTimeOut[]
}
export default function BookingsList({ tourId, times }: BookingsListProps) {
    const [selectedTime, setSelectedTime] = useState<
        DtoTourTimeOut | undefined
    >(getClosetFutureDate(times))

    return (
        <>
            <Select
                minW={"25vw"}
                value={selectedTime?.id}
                onChange={(e) => {
                    const time = times.find((t) => t.id === e.target.value)
                    if (time) setSelectedTime(time)
                }}
            >
                {times.map((t) => (
                    <option value={t.id}>{t.start_time}</option>
                ))}
            </Select>
            <BookingListTable tourId={tourId} timeId={selectedTime?.id ?? ""} />
        </>
    )
}
