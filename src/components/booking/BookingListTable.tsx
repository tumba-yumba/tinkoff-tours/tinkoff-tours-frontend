import {
    Center,
    Heading,
    Spinner,
    Table,
    TableContainer,
    Tbody,
    Tfoot,
    Th,
    Thead,
    Tr,
} from "@chakra-ui/react"
import React, { useState } from "react"
import { useGetToursByTourIdTimesAndTimeIdBookingsQuery } from "../../store/api/exportedApi"
import { PaginateBlock } from "../navigation/PaginateBlock"

interface BookingListTableProps {
    tourId: string
    timeId: string
}

const TABLE_PAGE_SIZE = 10

export function BookingListTable({ tourId, timeId }: BookingListTableProps) {
    const [itemOffset, setItemOffset] = useState(0)
    const { data: bookings, isLoading: isLoadingBookings } =
        useGetToursByTourIdTimesAndTimeIdBookingsQuery({
            tourId: tourId,
            timeId: timeId,
            offset: itemOffset,
            limit: TABLE_PAGE_SIZE,
        })

    function onPageChange(event: { selected: number }) {
        const newOffset =
            (event.selected * TABLE_PAGE_SIZE) % (bookings?.count ?? 0)
        setItemOffset(newOffset)
    }

    const pageCount = Math.ceil((bookings?.count ?? 0) / TABLE_PAGE_SIZE)
    return (
        <TableContainer>
            <Heading size={"md"}>Записей: {bookings?.count} </Heading>
            {isLoadingBookings && (
                <Center>
                    <Spinner />
                </Center>
            )}
            {!isLoadingBookings && (
                <Table size="md">
                    <Thead>
                        <Tr>
                            <Th>Email</Th>
                            <Th>Имя</Th>
                            <Th isNumeric>Кол-во</Th>
                        </Tr>
                    </Thead>
                    <Tbody>
                        {bookings?.items &&
                            bookings.items.map((booking, ind) => (
                                <Tr key={ind}>
                                    <Th>{booking.email}</Th>
                                    <Th>{booking.name}</Th>
                                    <Th isNumeric>
                                        {booking.participants_count}
                                    </Th>
                                </Tr>
                            ))}
                    </Tbody>
                    <Tfoot
                        textAlign={"center"}
                        w={"100%"}
                        justifyContent={"center"}
                        alignContent={"center"}
                    ></Tfoot>
                </Table>
            )}

            <Center w={"100%"}>
                <PaginateBlock
                    onPageChange={onPageChange}
                    pageCount={pageCount}
                />
            </Center>
        </TableContainer>
    )
}
