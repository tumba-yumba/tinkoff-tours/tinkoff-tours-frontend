import { Link, useNavigate, useParams } from "react-router-dom"
import { TModal } from "../modals/TModal"
import { Center, Spinner, Box, Text, Link as Href } from "@chakra-ui/react"
import Navbar from "../navigation/Navbar"
import { useDeleteBookingsByIdMutation } from "../../store/api/exportedApi"
import { useEffect } from "react"

export function UnBookingPage() {
    const { id } = useParams()
    const [trigger, { isLoading, error }] = useDeleteBookingsByIdMutation()
    const navigate = useNavigate()
    useEffect(() => {
        trigger({ id: String(id) ?? "" })
    }, [])
    return (
        <>
            <Navbar />
            {isLoading && (
                <Center m={5}>
                    <Spinner mr={2} /> Отписываемся
                </Center>
            )}
            {!isLoading && error && (
                <Center flexDir={"column"}>
                    <Box mt={3}>
                        <Text>
                            Что-то пошло не так, при попытке отменить
                            бронирование
                        </Text>
                    </Box>
                    <Link to={"/"}>
                        <Href color={"purpleT.500"}> На главную </Href>
                    </Link>
                </Center>
            )}
            {!isLoading && !error && (
                <TModal
                    isOpen={true}
                    onDone={() => navigate("/", { replace: true })}
                    title={"Бронирование отменено "}
                    withPlane={true}
                >
                    Вы отменили бронирование на экскурсию <br /> Время выбирать
                    новую
                </TModal>
            )}
        </>
    )
}
