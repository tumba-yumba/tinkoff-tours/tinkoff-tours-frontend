import NothingFound from "../utils/NothingFound"
import {
    DtoFeedbackInList,
    exportedApi,
    useGetModerationFeedbacksQuery,
} from "../../store/api/exportedApi"
import { Box, Center, Flex, Spinner } from "@chakra-ui/react"
import React from "react"
import { ReviewCard } from "../raiting/ReviewCard"
import { useDispatch } from "react-redux"

export function ReviewModerateTab() {
    const { data, isLoading } = useGetModerationFeedbacksQuery()
    const dispatch = useDispatch()
    const onModerate = (review: DtoFeedbackInList) => {
        const t = exportedApi.util.updateQueryData(
            "getModerationFeedbacks",
            undefined,
            (oldData) => {
                oldData.items = oldData!.items!.filter(
                    (item: DtoFeedbackInList) => item.id !== review.id
                )
                return oldData
            }
        )
        // @ts-ignore
        dispatch(t)
    }
    return (
        <>
            {isLoading && (
                <Center>
                    <Spinner />
                </Center>
            )}
            {!isLoading && !data?.items?.length && (
                <Center>
                    {" "}
                    <NothingFound
                        image={"adventure"}
                        title={"Все проверено"}
                        help={"Ожидаем когда будут новые"}
                    />
                </Center>
            )}
            {!isLoading && (
                <Center>
                    <Box w={["100%", "100%", "90%", "75%", "60%"]}>
                        <Flex direction={"column"} justify={"center"} gap={2}>
                            {data?.items &&
                                data.items.map((item) => (
                                    <ReviewCard
                                        key={item.id}
                                        review={item}
                                        onModerate={onModerate}
                                    />
                                ))}
                        </Flex>
                    </Box>
                </Center>
            )}
        </>
    )
}
