import { Box, Center, Flex, Spinner } from "@chakra-ui/react"
import TourItemCard from "../cards/TourItemCard"
import React, { useState } from "react"
import { useGetModerationToursQuery } from "../../store/api/exportedApi"
import { PaginateBlock } from "../navigation/PaginateBlock"
import { PAGE_SIZE } from "../../configs"
import NothingFound from "../utils/NothingFound"

function ToursModerateList() {
    const [itemOffset, setItemOffset] = useState(0)
    const { data, isLoading, refetch } = useGetModerationToursQuery({
        offset: itemOffset,
        limit: PAGE_SIZE,
    })
    const pageCount = Math.ceil((data?.count ?? 0) / PAGE_SIZE)

    function onPageChange(event: { selected: number }) {
        const newOffset = (event.selected * PAGE_SIZE) % (data?.count ?? 0)
        setItemOffset(newOffset)
        refetch()
    }
    return (
        <>
            {isLoading && (
                <Center>
                    <Spinner />
                </Center>
            )}
            {!isLoading && !data?.items?.length && (
                <Center>
                    {" "}
                    <NothingFound
                        image={"adventure"}
                        title={"Все проверено"}
                        help={"Ожидаем когда создадут новое"}
                    />
                </Center>
            )}
            {!isLoading && (
                <Center>
                    <Box w={["100%", "100%", "90%", "75%", "60%"]}>
                        <Flex direction={"column"} justify={"center"}>
                            {data?.items &&
                                data.items.map((tour) => (
                                    <TourItemCard
                                        key={tour.id}
                                        tour={tour}
                                        goToText={"Проверить"}
                                    />
                                ))}
                        </Flex>
                        <Center>
                            <PaginateBlock
                                onPageChange={onPageChange}
                                pageCount={pageCount}
                            />
                        </Center>
                    </Box>
                </Center>
            )}
        </>
    )
}

export default ToursModerateList
