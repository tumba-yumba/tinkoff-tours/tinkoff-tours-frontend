import { Button, Flex } from "@chakra-ui/react"
import { NavLink } from "react-router-dom"
import "./links.css"

function ModerateTabsNavigation() {
    return (
        <Flex
            bg={"tinkoff.500"}
            justifyContent={"center"}
            alignItems={"center"}
            direction={["column", "column", "row"]}
            gap={7}
            pb={4}
        >
            <NavLink
                to={"/moderate/tours"}
                className={({ isActive }) => (isActive ? "real-active" : "")}
            >
                <Button
                    colorScheme={"purpleT"}
                    size={"lg"}
                    className={"active"}
                    px={"60px"}
                    h={"60px"}
                >
                    Экскурсии
                </Button>
                <Button
                    colorScheme={"gray"}
                    size={"lg"}
                    className={"not-active"}
                    color={"black"}
                    px={"60px"}
                    h={"60px"}
                >
                    Экскурсии
                </Button>
            </NavLink>
            <NavLink
                to={"/moderate/profiles"}
                className={({ isActive }) => (isActive ? "real-active" : "")}
            >
                <Button
                    colorScheme={"purpleT"}
                    size={"lg"}
                    className={"active"}
                    px={"60px"}
                    h={"60px"}
                >
                    Профили
                </Button>
                <Button
                    colorScheme={"gray"}
                    size={"lg"}
                    className={"not-active"}
                    color={"black"}
                    px={"60px"}
                    h={"60px"}
                >
                    Профили
                </Button>
            </NavLink>
            <NavLink
                to={"/moderate/reviews"}
                className={({ isActive }) => (isActive ? "real-active" : "")}
            >
                <Button
                    colorScheme={"purpleT"}
                    size={"lg"}
                    className={"active"}
                    px={"60px"}
                    h={"60px"}
                >
                    Отзывы
                </Button>
                <Button
                    colorScheme={"gray"}
                    size={"lg"}
                    className={"not-active"}
                    color={"black"}
                    px={"60px"}
                    h={"60px"}
                >
                    Отзывы
                </Button>
            </NavLink>
        </Flex>
    )
}

export default ModerateTabsNavigation
