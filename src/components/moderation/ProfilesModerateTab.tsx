import { useGetModerationGuidesQuery } from "../../store/api/exportedApi"
import { Box, Center, Flex, Spinner } from "@chakra-ui/react"
import React, { useState } from "react"
import { GuideCard } from "../cards/GuideCard"
import { PAGE_SIZE } from "../../configs"
import { PaginateBlock } from "../navigation/PaginateBlock"
import NothingFound from "../utils/NothingFound"

function ProfilesModerateTab() {
    const [itemOffset, setItemOffset] = useState(0)
    const { data, isLoading, refetch } = useGetModerationGuidesQuery({
        offset: itemOffset,
        limit: PAGE_SIZE,
    })
    const pageCount = Math.ceil((data?.count ?? 0) / PAGE_SIZE)

    function onPageChange(event: { selected: number }) {
        const newOffset = (event.selected * PAGE_SIZE) % (data?.count ?? 0)
        setItemOffset(newOffset)
        refetch()
    }

    return (
        <>
            {isLoading && (
                <Center>
                    <Spinner />
                </Center>
            )}
            {!isLoading && !data?.items?.length && (
                <Center>
                    {" "}
                    <NothingFound
                        image={"adventure"}
                        title={"Все проверено"}
                        help={"Ожидаем когда создадут новое"}
                    />
                </Center>
            )}
            {!isLoading && (
                <Center>
                    <Box w={["100%", "100%", "90%", "75%", "60%"]}>
                        <Flex direction={"column"} justify={"center"}>
                            {data?.items &&
                                data.items.map((tour) => (
                                    <GuideCard key={tour.id} guide={tour} />
                                ))}
                        </Flex>
                        <Center>
                            <PaginateBlock
                                onPageChange={onPageChange}
                                pageCount={pageCount}
                            />
                        </Center>
                    </Box>
                </Center>
            )}
        </>
    )
}

export default ProfilesModerateTab
