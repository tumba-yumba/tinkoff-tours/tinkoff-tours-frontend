import { Box } from "@chakra-ui/react"
import ToursSearch, {
    SearchParams,
} from "../../components/navigation/ToursSearch"
import { querySerialize } from "../../utils/querrySerilize"
import { useNavigate } from "react-router-dom"

export default function SearchNavigate() {
    const navigate = useNavigate()
    function onSearch(params: SearchParams) {
        navigate({ pathname: "/tours", search: querySerialize(params) })
    }
    return (
        <>
            <Box bg="tinkoff.500" p="5" pt="0">
                <ToursSearch onSearch={onSearch} />
            </Box>
        </>
    )
}
