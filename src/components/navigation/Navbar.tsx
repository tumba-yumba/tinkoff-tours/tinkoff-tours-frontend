import { Link as Href, useNavigate } from "react-router-dom"
import {
    Avatar,
    Box,
    Button,
    Center,
    Flex,
    Hide,
    Img,
    Link,
    Menu,
    MenuButton,
    MenuItem,
    MenuList,
    Show,
    Spacer,
    Text,
    Wrap,
    WrapItem,
} from "@chakra-ui/react"
import logo from "../../assets/logo.svg"
import { ArrowForwardIcon } from "@chakra-ui/icons"
import { useAuth } from "../../store/hooks/useAuth"
import { useDispatch } from "react-redux"
import { logOut } from "../../store/slices/authSlice"
import { BsPersonFill } from "react-icons/bs"
import { FiLogOut } from "react-icons/fi"
import { FaUserShield } from "react-icons/fa"
import { Role } from "../../types/user"
import { exportedApi } from "../../store/api/exportedApi"

function Navbar() {
    const auth = useAuth()
    const dispatch = useDispatch()
    const navigate = useNavigate()
    return (
        <Box bg="tinkoff.500" p="2" px="5">
            <Wrap alignItems={"center"}>
                <WrapItem>
                    <Flex alignItems={"center"} gap={2}>
                        <Href to="/">
                            <Img src={logo} maxWidth={"50px"} p={1} />
                        </Href>
                        <Hide below={"sm"}>
                            <Href to="/">
                                <Text fontWeight={700}>
                                    ТумбаЮмба.Экскурсии
                                </Text>
                            </Href>
                        </Hide>
                        <Show below={"sm"}>
                            <Href to="/">
                                <Text fontWeight={700} ml={-2}>
                                    ТумбаЮмба
                                </Text>
                            </Href>
                        </Show>
                    </Flex>
                </WrapItem>
                <Spacer />
                {!auth.user && (
                    <WrapItem>
                        <Flex
                            flexDirection={"column"}
                            h={"100%"}
                            justifyContent={"center"}
                            alignItems={"center"}
                        >
                            <Link color="black" as={"span"}>
                                <Href to="/login">
                                    <Hide below={"sm"}>Для экскурсоводов</Hide>
                                    <Show below={"sm"}>Экскурсоводам</Show>
                                    <ArrowForwardIcon
                                        color={"purpleT.500"}
                                        boxSize={"1.5em"}
                                    />
                                </Href>
                            </Link>
                        </Flex>
                    </WrapItem>
                )}
                <WrapItem>
                    {auth.user?.data !== undefined && (
                        <Menu>
                            <MenuButton as={Button} h={"100%"} minH={"30px"}>
                                <Flex>
                                    <Center>
                                        {auth.user.data.first_name}{" "}
                                        {auth.user.data.last_name}
                                    </Center>{" "}
                                    <Avatar
                                        src={auth.user.data.avatar}
                                        bg={"tinkoff.200"}
                                        color={"#695B12"}
                                        ml={2}
                                        size={"sm"}
                                        name={
                                            auth.user.data.first_name +
                                            " " +
                                            auth.user.data.last_name
                                        }
                                    />
                                </Flex>
                            </MenuButton>
                            <MenuList zIndex={3}>
                                {auth.user.role !== Role.admin && (
                                    <Href to={`/guide/${auth.user.userId}`}>
                                        <MenuItem
                                            icon={
                                                <BsPersonFill
                                                    color={"purple"}
                                                    size={"1.5em"}
                                                />
                                            }
                                        >
                                            Профиль{" "}
                                        </MenuItem>
                                    </Href>
                                )}
                                {auth.user.role === Role.admin && (
                                    <Href to="/moderate/tours">
                                        <MenuItem
                                            icon={
                                                <FaUserShield
                                                    color={"purple"}
                                                    size={"1.5em"}
                                                />
                                            }
                                        >
                                            {" "}
                                            Модерация{" "}
                                        </MenuItem>{" "}
                                    </Href>
                                )}
                                <MenuItem
                                    icon={
                                        <FiLogOut
                                            color={"purple"}
                                            size={"1.5em"}
                                        />
                                    }
                                    onClick={() => {
                                        dispatch(logOut())
                                        dispatch(
                                            exportedApi.util.resetApiState()
                                        )
                                        navigate("/")
                                    }}
                                >
                                    {" "}
                                    Выйти{" "}
                                </MenuItem>
                            </MenuList>
                        </Menu>
                    )}
                </WrapItem>
            </Wrap>
        </Box>
    )
}

export default Navbar
