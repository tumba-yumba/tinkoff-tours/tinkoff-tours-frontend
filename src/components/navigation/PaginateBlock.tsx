import { Center } from "@chakra-ui/react"
import ReactPaginate from "react-paginate"
import React from "react"
import "./pagination.css"

interface PaginateProps {
    onPageChange: (selectedItem: { selected: number }) => void
    pageCount: number
}

export function PaginateBlock({ onPageChange, pageCount }: PaginateProps) {
    return (
        <Center mt={3}>
            <ReactPaginate
                breakLabel="..."
                containerClassName="pagination"
                activeClassName="active"
                previousLabel="<"
                nextLabel=">"
                pageClassName="page-item"
                pageLinkClassName="page-link"
                previousClassName="page-item"
                previousLinkClassName="page-link"
                nextClassName="page-item"
                nextLinkClassName="page-link"
                breakClassName="page-item"
                breakLinkClassName="page-link"
                onPageChange={onPageChange}
                pageRangeDisplayed={5}
                renderOnZeroPageCount={() => null}
                pageCount={pageCount}
            />
        </Center>
    )
}
