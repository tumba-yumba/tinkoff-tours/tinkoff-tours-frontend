import { Box, Center, Text, Flex, Link, Spacer, Icon } from "@chakra-ui/react"
import { TbBrandGitlab } from "react-icons/tb"
import { Link as Href } from "react-router-dom"

function Footer() {
    return (
        <Center
            width="100%"
            flexShrink="0"
            mt="3"
            mb="2"
            flexDirection="column"
        >
            <Box width="90%" bg="tinkoff.300" height="5px"></Box>
            <Flex width="90%">
                <Text color={"gray.400"}>
                    © 2022, ООО <Href to={"/"}>«тумба-юмба»</Href>, студенческий
                    проект ФИИТ
                </Text>
                <Spacer />
                <Box color={"gray.400"} pt="1">
                    <Link
                        href="https://gitlab.com/tumba-yumba/tinkoff-tours"
                        mt="2"
                        target="_blank"
                        rel="noreferrer"
                    >
                        <Icon as={TbBrandGitlab} />
                    </Link>
                </Box>
            </Flex>
        </Center>
    )
}

export default Footer
