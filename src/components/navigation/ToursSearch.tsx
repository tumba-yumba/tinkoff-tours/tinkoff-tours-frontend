import { Button, Center, Select, Spacer, Stack } from "@chakra-ui/react"
import React, { useEffect, useState } from "react"
import {
    AutoComplete,
    AutoCompleteInput,
    AutoCompleteItem,
    AutoCompleteList,
} from "@choc-ui/chakra-autocomplete"
import { places } from "../../data/places"
import { DatePicker } from "../parts/DatePicker"
import { toLocaleString } from "../../utils/timeHelpers"

interface SearchToursProps {
    onSearch: (params: SearchParams) => void
    initParams?: SearchParams
    autoApply?: boolean
}

export interface SearchParams {
    city: string
    dateStart: string
    dateEnd: string
    country: string
    participants: number
}

function ToursSearch(props: SearchToursProps) {
    const { onSearch, initParams, autoApply } = props
    const [city, setCity] = useState(initParams?.city ?? "")
    const [country, setCountry] = useState(initParams?.country ?? "")
    const [dateStart, setDateStart] = useState<Date | null>(
        initParams?.dateStart !== undefined &&
            !isNaN(Date.parse(initParams.dateStart))
            ? new Date(initParams.dateStart)
            : null
    )
    const [dateEnd, setDateEnd] = useState<Date | null>(
        initParams?.dateEnd !== undefined &&
            !isNaN(Date.parse(initParams.dateEnd))
            ? new Date(initParams.dateEnd)
            : null
    )
    const [participants, setParticipants] = useState(
        initParams?.participants ?? 1
    )

    function onClick() {
        onSearch({
            city,
            country,
            dateEnd: dateEnd !== null ? toLocaleString(dateEnd) : "",
            dateStart: dateStart !== null ? toLocaleString(dateStart) : "",
            participants,
        })
    }

    useEffect(() => {
        if (autoApply) {
            onClick()
        }
    }, [autoApply, city, country, dateEnd, participants])

    const onChangePlace = (e: string) => {
        if (Object.keys(places).includes(e)) {
            setCountry(e)
            setCity("")
        } else {
            setCountry("")
            setCity(e)
        }
    }

    return (
        <Stack direction={["column", "column", "row"]} mx={["0", "3", "5"]}>
            <Stack
                direction={["column", "row"]}
                spacing="0px"
                gap={0.5}
                width="100%"
                minHeight="48px"
            >
                <AutoComplete
                    listAllValuesOnFocus
                    suggestWhenEmpty
                    selectOnFocus
                    defaultValue={city ? city : country}
                    openOnFocus
                    closeOnSelect
                    emptyState={<Center>Ничего не найдено</Center>}
                    value={city ? city : country}
                    onChange={onChangePlace}
                >
                    <AutoCompleteInput
                        borderRightRadius={["md", 0]}
                        bg={"white"}
                        size={"lg"}
                        placeholder={"Страна/Город"}
                    />
                    <AutoCompleteList>
                        <AutoCompleteItem
                            textTransform="capitalize"
                            value=""
                            key=""
                        >
                            -не важно-
                        </AutoCompleteItem>
                        {Object.values(places)
                            .reduce(
                                (res: Array<string>, cur: Array<string>) =>
                                    res.concat(cur),
                                Object.keys(places)
                            )
                            .map((key) => (
                                <AutoCompleteItem
                                    textTransform="capitalize"
                                    value={key}
                                    key={key}
                                >
                                    {key}
                                </AutoCompleteItem>
                            ))}
                    </AutoCompleteList>
                </AutoComplete>
                <DatePicker
                    dtype={"date"}
                    size={"lg"}
                    placeholder={"C даты"}
                    borderRadius={["md", 0]}
                    bg={"white"}
                    selected={dateStart}
                    selectsStart
                    startDate={dateStart}
                    endDate={dateEnd}
                    onDateChanged={(e) => setDateStart(e)}
                />
                <DatePicker
                    dtype={"date"}
                    size={"lg"}
                    placeholder={"По дату"}
                    borderRadius={["md", 0]}
                    bg={"white"}
                    selectsEnd
                    startDate={dateStart}
                    endDate={dateEnd}
                    minDate={dateStart}
                    selected={dateEnd}
                    onDateChanged={(e) => setDateEnd(e)}
                />
                <Select
                    size={"lg"}
                    borderLeftRadius={["md", 0]}
                    bg={"white"}
                    value={participants}
                    onChange={(e) => setParticipants(parseInt(e.target.value))}
                >
                    <option value={1}>1 Участник</option>
                    <option value={2}>2 Участника</option>
                    <option value={3}>3 Участника</option>
                    <option value={4}>4 Участника</option>
                    <option value={5}>5 Участников</option>
                </Select>
            </Stack>

            <Spacer />
            <Button colorScheme="purpleT" size="lg" onClick={onClick}>
                Показать
            </Button>
        </Stack>
    )
}

export default ToursSearch
