import {
    Box,
    Button,
    Divider,
    Flex,
    FormControl,
    FormLabel,
    Icon,
    Input,
    InputGroup,
    InputLeftAddon,
    NumberInput,
    NumberInputField,
    NumberInputStepper,
    Slider,
    SliderFilledTrack,
    SliderMark,
    SliderThumb,
    SliderTrack,
    Text,
    Tooltip,
} from "@chakra-ui/react"
import React, { useEffect, useState } from "react"
import { BiRuble } from "react-icons/bi"
import { TagSelect } from "../parts/TagsSelect"
import { DtoTagOut, useGetTagsQuery } from "../../store/api/exportedApi"
import { QuestionOutlineIcon } from "@chakra-ui/icons"

interface SearchToursProps {
    initParams?: FilterParams
    onApply: (params: FilterParams) => void
    autoApply?: boolean
}

export interface FilterParams {
    price: number
    timeStart: string
    timeEnd: string
    tags: Array<string>
}

function ToursSearch(props: SearchToursProps) {
    const { initParams, onApply, autoApply } = props
    const [sliderValue, setSliderValue] = useState(initParams?.price ?? 10000)
    const [priceValue, setPriceValue] = useState(initParams?.price ?? 10000)
    const [showTooltip, setShowTooltip] = useState(false)
    const [timeStart, setTimeStart] = useState(initParams?.timeStart ?? "")
    const [timeEnd, setTimeEnd] = useState(initParams?.timeEnd ?? "")
    const { data: allTags = { tags: [] } } = useGetTagsQuery()
    const [tags, setTags] = useState<Array<DtoTagOut>>([])
    useEffect(() => {
        if (initParams?.tags) {
            setTags(
                allTags.tags!.filter((tag) => initParams.tags.includes(tag.id!))
            )
        }
    }, [allTags])
    useEffect(() => {
        if (autoApply) {
            onApply({
                price: sliderValue,
                timeStart,
                timeEnd,
                tags: tags.map((t) => t.id!),
            })
        }
    }, [autoApply, tags, sliderValue, timeStart, timeEnd])
    return (
        <Box width="100%">
            <FormControl mb="5">
                <FormLabel fontWeight={800} fontSize={"lg"}>
                    Цена
                </FormLabel>
                <Divider colorScheme="black" mt="-2" />
                <Flex>
                    <FormLabel mt={3}>До:</FormLabel>
                    <NumberInput
                        mt={1}
                        value={priceValue}
                        maxW="100px"
                        minW="95px"
                        onChange={(e) =>
                            setPriceValue(parseInt(e.length > 0 ? e : "0"))
                        }
                        onBlur={(e) =>
                            setSliderValue(
                                parseInt(
                                    e.target.value.length > 0
                                        ? e.target.value
                                        : "0"
                                )
                            )
                        }
                    >
                        <NumberInputField />
                        <NumberInputStepper bg={"gray.50"}>
                            <Icon mt={2} ml={1} as={BiRuble} />
                        </NumberInputStepper>
                    </NumberInput>
                    <Text mt={3} whiteSpace={"nowrap"}>
                        /чел.
                    </Text>
                </Flex>

                <Slider
                    id="slider"
                    value={sliderValue}
                    min={0}
                    max={10000}
                    colorScheme="tinkoff"
                    onChange={(v) => {
                        setSliderValue(v)
                        setPriceValue(v)
                    }}
                    onMouseEnter={() => setShowTooltip(true)}
                    onMouseLeave={() => setShowTooltip(false)}
                >
                    <SliderMark value={0} mt="1" ml="-2.5" fontSize="sm">
                        0
                    </SliderMark>
                    <SliderMark value={10000} mt="1" ml="-2.5" fontSize="sm">
                        10000
                    </SliderMark>
                    <SliderTrack>
                        <SliderFilledTrack />
                    </SliderTrack>
                    <Tooltip
                        hasArrow
                        bg="tinkoff.500"
                        color="white"
                        placement="top"
                        isOpen={showTooltip}
                        label={`${sliderValue}`}
                    >
                        <SliderThumb />
                    </Tooltip>
                </Slider>
            </FormControl>

            <FormControl mt="1">
                <FormLabel fontWeight={800} fontSize={"lg"}>
                    Время{" "}
                    <Tooltip
                        label="Время дня, в пределах которого должна начаться/закончится экскурсия."
                        closeDelay={100}
                    >
                        <QuestionOutlineIcon />
                    </Tooltip>
                </FormLabel>
                <Divider colorScheme="black" mt="-2" mb="3" />
                <InputGroup flexDirection={["column", "row", "column", "row"]}>
                    <InputLeftAddon children="C:" />
                    <Input
                        value={timeStart}
                        type="time"
                        width="auto"
                        borderRightRadius="0"
                        onChange={(e) => setTimeStart(e.target.value)}
                    />
                    <InputLeftAddon children="До:" borderLeftRadius="0" />
                    <Input
                        value={timeEnd}
                        type="time"
                        width="auto"
                        onChange={(e) => setTimeEnd(e.target.value)}
                    />
                </InputGroup>
            </FormControl>
            <TagSelect value={tags} onChange={setTags} labelWithDivider />
            <Button
                mt="10"
                width="100%"
                onClick={() =>
                    onApply({
                        price: sliderValue,
                        timeStart,
                        timeEnd,
                        tags: tags.map((t) => t.id!),
                    })
                }
            >
                Применить
            </Button>
        </Box>
    )
}

export default ToursSearch
