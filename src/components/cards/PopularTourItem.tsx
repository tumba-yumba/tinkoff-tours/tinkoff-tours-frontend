import { Box, Button, Heading, Image, Text } from "@chakra-ui/react"
import { Link } from "react-router-dom"
import { DtoTourInListOut } from "../../store/api/exportedApi"

interface CardItemProps {
    tour: DtoTourInListOut
    label: string
}

function PopularTourItem(props: CardItemProps) {
    const { tour: item, label } = props
    return (
        <Box borderRadius="lg" position="relative" w={"300px"} h={"300px"}>
            <Image
                borderRadius="lg"
                src={item.photo}
                alt={item.title}
                objectFit={"cover"}
                w={"300px"}
                h={"300px"}
                position={"absolute"}
            />
            <Box
                borderRadius="lg"
                position={"absolute"}
                w={"300px"}
                h={"300px"}
                bgGradient="linear(to-b, blackAlpha.700, blackAlpha.50)"
            ></Box>
            <Box p="3" h={"100%"} w={"100%"} zIndex={5} position={"absolute"}>
                <Text
                    w={"max-content"}
                    color={"gray.200"}
                    bg={"blackAlpha.500"}
                    px={3}
                    py={1}
                    borderRadius={13}
                    fontWeight={900}
                >
                    {label.toUpperCase()}
                </Text>
                <Link to={`/tours/${item.id}`}>
                    <Heading
                        mt={2}
                        px={1}
                        py={1}
                        cursor={"pointer"}
                        size={"lg"}
                        noOfLines={4}
                        borderRadius={3}
                        color={"white"}
                    >
                        {item.title}
                    </Heading>
                </Link>
                <Box position={"absolute"} bottom={3} left={3}>
                    <Link to={`/tours/${item.id}`}>
                        <Button colorScheme={"gray"} color={"black"}>
                            Билеты {item.price}₽
                        </Button>
                    </Link>
                </Box>
            </Box>
        </Box>
    )
}

export default PopularTourItem
