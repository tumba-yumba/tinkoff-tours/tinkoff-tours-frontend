import { DtoGuideOut } from "../../store/api/exportedApi"
import {
    Avatar,
    Box,
    Button,
    Center,
    Flex,
    Icon,
    Spacer,
    Text,
} from "@chakra-ui/react"
import { TiTick } from "react-icons/ti"
import { AiOutlineClose, AiOutlineFieldTime } from "react-icons/ai"
import ReactTimeAgo from "react-time-ago"
import { Link } from "react-router-dom"
import React from "react"

interface CardItemProps {
    guide: DtoGuideOut
}
export function GuideCard({ guide }: CardItemProps) {
    return (
        <Box flexGrow={1} bg={"white"} p={1} borderRadius={3}>
            <Flex direction={["column", "column", "row"]} p={2}>
                <Center>
                    <Avatar
                        bg={"tinkoff.200"}
                        color={"#695B12"}
                        size="2xl"
                        name={guide?.first_name + " " + guide?.last_name}
                        src={guide?.avatar}
                    />
                </Center>
                <Flex direction={"column"} ml={5} mt={5}>
                    <Text align={"left"} fontSize={"3xl"} fontWeight={700}>
                        {guide?.first_name} {guide?.last_name}
                        {guide?.moderate_status === "approved" && (
                            <Icon
                                ml={1}
                                boxSize={8}
                                top={1}
                                position={"relative"}
                                color={"green.300"}
                                as={TiTick}
                            />
                        )}
                        {guide?.moderate_status === "pending" && (
                            <Icon
                                ml={1}
                                boxSize={8}
                                top={1}
                                position={"relative"}
                                color={"tinkoff.500"}
                                as={AiOutlineFieldTime}
                            />
                        )}
                        {guide?.moderate_status === "rejected" && (
                            <Icon
                                ml={1}
                                boxSize={8}
                                top={1}
                                position={"relative"}
                                color={"red.500"}
                                as={AiOutlineClose}
                            />
                        )}
                    </Text>
                    <Text align={"left"}>
                        <strong>На платформе:</strong>{" "}
                        <ReactTimeAgo
                            date={new Date(guide!.created_at!)}
                            tooltip={false}
                            timeStyle={"long-time"}
                            future={false}
                        />
                    </Text>
                    <Text align={"left"} pb={"5"}>
                        <strong>O себе: </strong>
                        {guide?.bio || (
                            <span
                                style={{ color: "gray", fontStyle: "italic" }}
                            >
                                Не заполнено
                            </span>
                        )}
                    </Text>
                </Flex>
                <Spacer />
                <Center
                    mr="5"
                    mb={5}
                    order={["0", "0", "0"]}
                    alignSelf={["self-end", "self-end", "auto"]}
                >
                    <Link to={`/guide/${guide.id}`}>
                        {" "}
                        <Button
                            size={"xl"}
                            h="50px"
                            p="5"
                            my={2}
                            fontSize={"xl"}
                        >
                            Проверить
                        </Button>
                    </Link>
                </Center>
            </Flex>
        </Box>
    )
}
