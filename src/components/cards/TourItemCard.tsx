import {
    Box,
    Button,
    Flex,
    Heading,
    Image,
    Text,
    Wrap,
    WrapItem,
} from "@chakra-ui/react"
import { Link } from "react-router-dom"
import { DtoTourInListOut } from "../../store/api/exportedApi"
import { getDurationString } from "../../utils/timeHelpers"

interface CardItemProps {
    tour: DtoTourInListOut
    squashOnMd?: boolean
    goToText?: string
}

export default function TourItemCard(props: CardItemProps) {
    const { tour: item, squashOnMd, goToText } = props
    return (
        <Flex
            borderRadius="lg"
            bg={"white"}
            mt={3}
            direction={["column", "row", squashOnMd ? "column" : "row", "row"]}
        >
            <Box
                h={["200px", "200px", "250px"]}
                w={[
                    "100%",
                    "250px",
                    squashOnMd ? "100%" : "300px",
                    "350px",
                    "400px",
                ]}
                p={3}
                mt={2}
                mb={2}
            >
                <Link to={`/tours/${item.id}`}>
                    <Image
                        objectFit={"cover"}
                        cursor={"pointer"}
                        src={item.photo}
                        alt={item.title}
                        h={"100%"}
                        w={"100%"}
                        borderRadius={10}
                    />
                </Link>
            </Box>

            <Flex
                p="6"
                gap={1}
                direction={"column"}
                w={[
                    "100%",
                    "calc(100% - 250px)",
                    squashOnMd ? "100%" : "calc(100% - 300px)",
                    "calc(100% - 300px)",
                ]}
                h={"100%"}
            >
                <Link to={`/tours/${item.id}`}>
                    <Heading
                        size={"lg"}
                        cursor={"pointer"}
                        wordBreak={"break-word"}
                    >
                        {item.title}
                    </Heading>
                </Link>
                <Box noOfLines={5} wordBreak={"break-word"}>
                    {item.short_description}
                </Box>

                <Wrap justify={"space-between"} h={"100%"}>
                    <WrapItem flexDirection={"column"}>
                        <Flex>
                            <Text fontWeight={800}>Город: </Text>
                            <Text ml={2}>{item.city}</Text>
                        </Flex>
                        <Flex>
                            <Text fontWeight={800}>Дата: </Text>
                            <Text ml={2}>
                                {new Date(
                                    Date.parse(item.start_time!)
                                ).toLocaleDateString([], {
                                    dateStyle: "short",
                                })}
                            </Text>
                        </Flex>
                        <Flex>
                            <Text fontWeight={800}>Время: </Text>
                            <Text ml={2}>
                                {new Date(
                                    Date.parse(item.start_time!)
                                ).toLocaleTimeString([], {
                                    timeStyle: "short",
                                })}
                            </Text>
                        </Flex>
                        <Flex>
                            <Text fontWeight={800}>Длительность: </Text>
                            <Text ml={2}>
                                {getDurationString(
                                    item.start_time!,
                                    item.end_time!
                                )}
                            </Text>
                        </Flex>

                        <Flex>
                            <Text fontWeight={800}>Цена: </Text>
                            <Text ml={2}>
                                {item.price} ₽
                                <Box
                                    as="span"
                                    color="gray.600"
                                    fontSize="sm"
                                    ml={1}
                                >
                                    с человека
                                </Box>
                            </Text>
                        </Flex>
                    </WrapItem>
                    <WrapItem alignSelf={"end"} alignContent={"end"}>
                        <Link to={`/tours/${item.id}`}>
                            <Button fontWeight={"semibold"}>
                                {goToText ?? "Подробнее"}
                            </Button>
                        </Link>
                    </WrapItem>
                </Wrap>
            </Flex>
        </Flex>
    )
}
