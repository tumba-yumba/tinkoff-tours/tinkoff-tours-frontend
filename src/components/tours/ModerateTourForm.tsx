import { getErrorMessage } from "../../utils/errorParser"
import { Button, Flex, Text, Textarea, useToast } from "@chakra-ui/react"
import React, { useState } from "react"
import { usePostModerationToursByIdMutation } from "../../store/api/exportedApi"

interface ModerateTourFormProps {
    onDone: () => void
    id: string | undefined
}
export function ModerateTourForm({ onDone, id }: ModerateTourFormProps) {
    const [moderate, { isLoading: isLoadingModerate }] =
        usePostModerationToursByIdMutation()
    const [comment, setComment] = useState("")
    const toast = useToast()
    const onClick: (status: "approved" | "rejected") => void = (
        status: "approved" | "rejected"
    ) => {
        moderate({ id: id ?? "", dtoModerateIn: { comment, status } })
            .then((resp) => {
                const error = getErrorMessage(resp)
                if (error) throw Error(error)
                toast({
                    title: `Вы изменили статус модерации`,
                    description:
                        status === "approved"
                            ? "Теперь экскурсия видна всем пользователям"
                            : "Экскурсия отвергнута",
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "info",
                    isClosable: true,
                })
                onDone()
                setComment("")
            })
            .catch((err) => {
                console.error(err)
                toast({
                    title: `Ошибка при смене статуса`,
                    description: String(err),
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "error",
                    isClosable: true,
                })
            })
    }
    return (
        <Flex direction={"column"} p={3}>
            <Text fontSize={"lg"} mb={"2"}>
                Модерация
            </Text>
            <Textarea
                value={comment}
                onChange={(t) => setComment(t.target.value)}
                placeholder={"Комментарий"}
            />
            <Flex gap={3} w={"100%"} mb={2} mt={2}>
                <Button
                    colorScheme={"gray"}
                    w={"50%"}
                    isLoading={isLoadingModerate}
                    onClick={() => onClick("rejected")}
                >
                    Отклонить
                </Button>
                <Button
                    colorScheme={"tinkoff"}
                    w={"50%"}
                    isLoading={isLoadingModerate}
                    onClick={() => onClick("approved")}
                >
                    Принять
                </Button>
            </Flex>
        </Flex>
    )
}
