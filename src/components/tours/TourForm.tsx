import React, { ChangeEvent, useMemo, useState } from "react"
import {
    DtoPhotoIn,
    DtoPhotoOut,
    DtoTagIn,
    DtoTagOut,
    DtoTourCreateIn,
    DtoTourOut,
    DtoTourTimeOut,
} from "../../store/api/exportedApi"
import {
    Box,
    Button,
    ButtonGroup,
    Editable,
    EditableInput,
    EditablePreview,
    Flex,
    FormControl,
    Input,
    Popover,
    PopoverArrow,
    PopoverBody,
    PopoverCloseButton,
    PopoverContent,
    PopoverFooter,
    PopoverHeader,
    PopoverTrigger,
    Textarea,
    useDisclosure,
    useToast,
} from "@chakra-ui/react"
import { useNavigate } from "react-router-dom"
import { getErrorMessage } from "../../utils/errorParser"
import { DatePicker } from "../parts/DatePicker"
import { TagSelect } from "../parts/TagsSelect"
import {
    formatToursTimes,
    getInternalTimes,
    TourTimeInternal,
} from "../../utils/timeHelpers"
import { GoBackPopover } from "./TourFormParts/GoBackPopover"
import { PhotoTourUploader } from "./TourFormParts/PhotoTourUploader"
import { TimeSelect } from "./TourFormParts/TimeSelect"
import { PlaceSelect } from "./TourFormParts/PlaceSelect"

interface ITourFormProps {
    title?: string
    country?: string
    city?: string
    times?: DtoTourTimeOut[]
    duration?: Date
    price?: number
    participants?: number
    smallDescription?: string
    emailDescription?: string
    fullDescription?: string
    tags?: Array<DtoTagOut>
    uploadedPhotos?: Array<DtoPhotoOut>
    actionTour: (tour: DtoTourCreateIn) => Promise<any>
    actionTitle: string
    typeForm: "edit" | "create"
}

export default function TourForm(props: ITourFormProps) {
    const [title, setTitle] = useState(props.title || "Введите название")
    const [country, setCounty] = useState(props.country || "")
    const [city, setCity] = useState(props.city || "")
    const [startValidate, setStartValidate] = useState(false)
    const [duration, setDuration] = useState<Date | null>(
        props.duration || null
    )
    const [tourTimes, setTourTimes] = useState<Array<TourTimeInternal>>(
        props.times && props.times.length > 0
            ? getInternalTimes(props.times)
            : [{ timeStart: null }]
    )

    const [price, setPrice] = useState<number>(props.price || NaN)
    const [participants, setParticipants] = useState<number>(
        props.participants || NaN
    )
    const [smallDescription, setSmallDescription] = useState<string>(
        props.smallDescription || ""
    )
    const [emailDescription, setEmailDescription] = useState<string>(
        props.emailDescription || ""
    )
    const [fullDescription, setFullDescription] = useState<string>(
        props.fullDescription || ""
    )
    const [tags, setTags] = useState<Array<DtoTagOut>>(props.tags || [])
    const [uploadedPhotos, setUploadedPhotos] = useState<Array<DtoPhotoOut>>(
        props.uploadedPhotos || []
    )
    const [loadings, setLoadings] = useState<Array<string>>([])
    const { isOpen, onToggle, onClose } = useDisclosure()
    const navigate = useNavigate()
    const toast = useToast()
    const isCountryInvalid = !country
    const isCityInvalid = !city
    const isTimeStartInvalid =
        tourTimes.length === 0 ||
        !tourTimes.some((item) => item.timeStart !== null)
    const isSmallDescriptionInvalid = !smallDescription
    const isEmailDescriptionInvalid = !emailDescription
    const isFullDescriptionInvalid = !fullDescription
    const isDurationInvalid = !duration
    const isPriceInvalid =
        price === undefined || price === null || isNaN(price) || price <= 0
    const isParticipantsInvalid =
        participants === undefined ||
        participants === null ||
        isNaN(participants) ||
        participants <= 0
    const isAnyError =
        isCountryInvalid ||
        isCityInvalid ||
        isTimeStartInvalid ||
        isDurationInvalid ||
        isPriceInvalid ||
        isParticipantsInvalid ||
        isSmallDescriptionInvalid ||
        isEmailDescriptionInvalid ||
        isFullDescriptionInvalid

    function TitleChange() {
        return (
            <Editable
                mx={5}
                fontSize={"4xl"}
                color={"gray"}
                textAlign={"center"}
                value={title}
                wordBreak={"break-word"}
                overflowX={"hidden"}
                onChange={(t) => setTitle(t)}
                onSubmit={(t) =>
                    setTitle(
                        t.length > 0 ? t.slice(0, 100) : "Введите название"
                    )
                }
            >
                <EditablePreview overflowX={"hidden"} />
                <EditableInput />
            </Editable>
        )
    }

    function PriceCountInput() {
        const parsePrice = (e: ChangeEvent<HTMLInputElement>) => {
            const number = Math.round(parseFloat(e.target.value) * 100) / 100
            setPrice(number < 0 ? 0 : number)
        }
        const parseCount = (e: ChangeEvent<HTMLInputElement>) => {
            const number = parseInt(e.target.value)
            setParticipants(number < 0 ? 0 : number)
        }
        return (
            <Flex px={2} gap={2} direction={["column", "row"]}>
                <FormControl isInvalid={startValidate && isDurationInvalid}>
                    <DatePicker
                        dtype={"time"}
                        placeholder={"Длительность"}
                        timeCaption={"Длительность"}
                        selected={duration}
                        maxTime={"12:00"}
                        onDateChanged={(e) => setDuration(e)}
                    />
                </FormControl>
                <FormControl isInvalid={startValidate && isPriceInvalid}>
                    <Input
                        type="number"
                        value={price}
                        onChange={parsePrice}
                        placeholder={"Цена (₽)"}
                        step="1"
                        min="0"
                    />
                </FormControl>
                <FormControl isInvalid={startValidate && isParticipantsInvalid}>
                    <Input
                        type="number"
                        value={participants}
                        onChange={parseCount}
                        placeholder={"Количество человек"}
                        step="1"
                        min="0"
                    />
                </FormControl>
            </Flex>
        )
    }

    const doWithTour = (validated = false) => {
        setStartValidate(true)
        const isTitleInvalid = title === "Введите название"
        const isPhotosInvalid = uploadedPhotos.length === 0
        const isStillUploading = loadings.length !== 0
        if (isTitleInvalid) {
            toast({
                title: "Заполните название экскурсии",
                description: "Кликните по заголовку для изменения",
                position: "bottom-right",
                variant: "left-accent",
                status: "warning",
                isClosable: true,
            })
        }
        if (isStillUploading) {
            toast({
                title: "Фото ещё загружаются.",
                description: "Дождитесь загрузки, прежде чем продолжить.",
                position: "bottom-right",
                variant: "left-accent",
                status: "warning",
                isClosable: true,
            })
        } else if (isPhotosInvalid) {
            toast({
                title: "Требуется фото.",
                description: "Добавьте хотя бы одно фото к экскурсии",
                position: "bottom-right",
                variant: "left-accent",
                status: "warning",
                isClosable: true,
            })
        }
        if (
            isAnyError ||
            isPhotosInvalid ||
            isTitleInvalid ||
            isStillUploading
        ) {
            return
        }
        const times = formatToursTimes(tourTimes, duration)
        if (!times) return
        if (!validated && props.typeForm === "edit") {
            const changeTimes = times.some(
                (t) =>
                    !props.times?.find(
                        (oldT) =>
                            oldT.start_time === t.start_time &&
                            oldT.end_time === t.end_time
                    )
            )
            if (changeTimes) {
                onToggle()
                return
            }
        }
        props
            .actionTour({
                city: city,
                country: country,
                description: fullDescription,
                max_participants: participants!,
                short_description: smallDescription,
                email_message: emailDescription,
                photos: uploadedPhotos.map((el) => el as DtoPhotoIn),
                price: price!,
                tags: tags.map((el) => el as DtoTagIn),
                title: title,
                times: times,
            })
            .then((resp: any) => {
                const error = getErrorMessage(resp)
                if (error) throw Error(error)
                const tour = resp.data as DtoTourOut
                navigate(`/tours/${tour.id}`)
            })
            .catch((err: any) => {
                console.error(err)
                toast({
                    title: `Ошибка.`,
                    description: String(err),
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "warning",
                    isClosable: true,
                })
            })
    }
    const anyEdit = useMemo(() => {
        return (
            title !== "Введите название" ||
            country !== "" ||
            city !== "" ||
            (tourTimes.length > 0 && tourTimes[0] !== null) ||
            duration ||
            price ||
            participants ||
            smallDescription !== "" ||
            emailDescription !== "" ||
            fullDescription !== "" ||
            tags.length > 0 ||
            uploadedPhotos.length > 0 ||
            loadings.length > 0
        )
    }, [
        title,
        country,
        city,
        tourTimes,
        duration,
        price,
        participants,
        smallDescription,
        emailDescription,
        fullDescription,
        tags,
        uploadedPhotos,
        loadings,
    ])

    return (
        <>
            <Box p="5">
                {TitleChange()}
                <PhotoTourUploader
                    uploadedPhotos={uploadedPhotos}
                    setUploadedPhotos={setUploadedPhotos}
                    setLoadings={setLoadings}
                    loadings={loadings}
                />
                <Box p="5" bg={"white"}>
                    <Flex direction={["column", "column", "row"]}>
                        <Box w={["100%", "100%", "50%"]} m={2} h={"100%"}>
                            <Textarea
                                isInvalid={
                                    startValidate && isSmallDescriptionInvalid
                                }
                                placeholder="Короткое описание экскурсии..."
                                m={2}
                                value={smallDescription}
                                onChange={(e) =>
                                    setSmallDescription(
                                        e.target.value.slice(0, 254)
                                    )
                                }
                                w={"calc(100% - 1em)"}
                                resize={"vertical"}
                                rows={5}
                                maxLength={254}
                            />
                            <Textarea
                                isInvalid={
                                    startValidate && isFullDescriptionInvalid
                                }
                                placeholder="Полное описание экскурсии..."
                                m={2}
                                w={"calc(100% - 1em)"}
                                value={fullDescription}
                                onChange={(e) =>
                                    setFullDescription(e.target.value)
                                }
                                resize={"vertical"}
                                rows={10}
                            />
                            <Textarea
                                isInvalid={
                                    startValidate && isEmailDescriptionInvalid
                                }
                                placeholder="Сообщение на email участников, напишите место встречи, контакты для связи..."
                                m={2}
                                value={emailDescription}
                                onChange={(e) =>
                                    setEmailDescription(
                                        e.target.value.slice(0, 254)
                                    )
                                }
                                w={"calc(100% - 1em)"}
                                resize={"vertical"}
                                rows={5}
                                maxLength={254}
                            />
                        </Box>
                        <Flex
                            w={["100%", "100%", "50%"]}
                            direction={"column"}
                            ml={["0", "0", "5"]}
                            gap={3}
                            maxW={["100%", "100%", "50%"]}
                        >
                            <TimeSelect
                                tourTimes={tourTimes}
                                setTourTimes={setTourTimes}
                                startValidate={startValidate}
                                isTimeStartInvalid={isTimeStartInvalid}
                            />
                            <PlaceSelect
                                defaultCity={props.city || ""}
                                defaultCountry={props.country || ""}
                                country={country}
                                city={city}
                                setCounty={setCounty}
                                setCity={setCity}
                                startValidate={startValidate}
                                isCityInvalid={isCityInvalid}
                                isCountryInvalid={isCountryInvalid}
                            />
                            {PriceCountInput()}
                            <TagSelect value={tags} onChange={setTags} />
                            <Flex w={"100%"} mt={5}>
                                <GoBackPopover anyEdit={!!anyEdit} />
                                <Popover
                                    returnFocusOnClose={false}
                                    isOpen={isOpen}
                                    onClose={onClose}
                                >
                                    <PopoverTrigger>
                                        <Button
                                            w={"50%"}
                                            size={"lg"}
                                            fontSize={"2xl"}
                                            onClick={() => doWithTour()}
                                            ml={5}
                                        >
                                            {props.actionTitle}
                                        </Button>
                                    </PopoverTrigger>
                                    <PopoverContent>
                                        <PopoverHeader fontWeight="semibold">
                                            Внимание
                                        </PopoverHeader>
                                        <PopoverArrow />
                                        <PopoverCloseButton />
                                        <PopoverBody>
                                            Вы изменили время, поэтому все
                                            текущие брони на эту экскурсию будут
                                            ОТМЕНЕНЫ.
                                            <br />
                                            Участникам придётся занавно
                                            бронировать её.
                                            <br />
                                            После изменения, экскурсия снова
                                            отправится на модерацию.{" "}
                                        </PopoverBody>
                                        <PopoverFooter
                                            display="flex"
                                            justifyContent="flex-end"
                                        >
                                            <ButtonGroup size="sm">
                                                <Button
                                                    variant="outline"
                                                    colorScheme="gray"
                                                    onClick={onToggle}
                                                >
                                                    Отмена
                                                </Button>
                                                <Button
                                                    variant="outline"
                                                    colorScheme="purple"
                                                    onClick={() =>
                                                        doWithTour(true)
                                                    }
                                                >
                                                    Редактировать
                                                </Button>
                                            </ButtonGroup>
                                        </PopoverFooter>
                                    </PopoverContent>
                                </Popover>
                            </Flex>
                        </Flex>
                    </Flex>
                </Box>
            </Box>
        </>
    )
}
