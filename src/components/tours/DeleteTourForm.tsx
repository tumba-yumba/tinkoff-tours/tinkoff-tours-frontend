import {
    Button,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalFooter,
    ModalHeader,
    ModalOverlay,
    useDisclosure,
    useToast,
} from "@chakra-ui/react"
import {
    exportedApi,
    useDeleteToursByIdMutation,
} from "../../store/api/exportedApi"
import { useDispatch } from "react-redux"
import { getErrorMessage } from "../../utils/errorParser"
import React from "react"
import { useNavigate } from "react-router-dom"

interface DeleteTourBlockProps {
    id: string | undefined
}
export function DeleteTourForm({ id }: DeleteTourBlockProps) {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const [deleteExcursion, { isLoading }] = useDeleteToursByIdMutation()
    const dispatch = useDispatch()
    const toast = useToast()
    const navigate = useNavigate()
    const onDeleteClick = () => {
        deleteExcursion({ id: id! })
            .then((resp) => {
                const error = getErrorMessage(resp)
                if (error) throw Error(error)
                toast({
                    title: `Экскурсия удалена`,
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "info",
                    isClosable: true,
                })
                dispatch(exportedApi.util.resetApiState())
                navigate(-1)
            })
            .catch((err) => {
                console.error(err)
                toast({
                    title: `Ошибка при удалении`,
                    description: String(err),
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "error",
                    isClosable: true,
                })
            })
    }
    return (
        <>
            <Button
                w={"50%"}
                onClick={onOpen}
                colorScheme={"red"}
                variant="ghost"
                my={2}
            >
                Удалить экскурсию
            </Button>
            <Modal isOpen={isOpen} isCentered onClose={onClose}>
                <ModalOverlay />
                <ModalContent>
                    <ModalHeader>Удаление экскурсии</ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        Вы действительно хотите удалить экскурсию? Данне будут
                        удалены безвозвратно.
                    </ModalBody>

                    <ModalFooter>
                        <Button
                            colorScheme="gray"
                            variant="ghost"
                            mr={3}
                            onClick={onClose}
                        >
                            Отмена
                        </Button>
                        <Button
                            colorScheme="red"
                            variant="ghost"
                            onClick={onDeleteClick}
                            isLoading={isLoading}
                        >
                            Удалить
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    )
}
