import React from "react"
import { Center, Image, Spinner } from "@chakra-ui/react"
import { Slider } from "../utils/SLider"
import { DtoPhotoOut } from "../../store/api/exportedApi"

interface PhotosBlockProps {
    photos: DtoPhotoOut[] | undefined
}
export function PhotosBlock({ photos }: PhotosBlockProps) {
    const photosList: Function = (): React.ReactElement[] =>
        photos!.map((el) => (
            <Image
                loading={"lazy"}
                fallbackSrc="https://via.placeholder.com/150"
                fallback={
                    <Center>
                        <Spinner />
                    </Center>
                }
                key={el.path}
                objectFit={"cover"}
                h={"300px"}
                maxW={"500px"}
                src={el.path}
                borderRadius={5}
            />
        ))
    return (
        <>
            {!!photos?.length && (
                <Slider shouldSwiperUpdate={false} loop={true}>
                    {photosList()}
                </Slider>
            )}
        </>
    )
}
