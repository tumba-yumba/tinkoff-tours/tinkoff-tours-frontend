import React, { ChangeEvent, useMemo, useState } from "react"
import { getErrorMessage } from "../../utils/errorParser"
import {
    Button,
    Flex,
    FormControl,
    FormErrorMessage,
    FormLabel,
    Heading,
    Input,
    NumberDecrementStepper,
    NumberIncrementStepper,
    NumberInput,
    NumberInputField,
    NumberInputStepper,
    Spacer,
    Text,
    useToast,
} from "@chakra-ui/react"
import {
    DtoTourOut,
    DtoTourTimeOut,
    useGetToursByTourIdTimesAndTimeIdBookingsCountQuery,
    usePostToursByTourIdTimesAndTimeIdBookingsMutation,
} from "../../store/api/exportedApi"
import { useAuth } from "../../store/hooks/useAuth"
import { DatePicker } from "../parts/DatePicker"
import {
    getClosetFutureDate,
    sameDay,
    sameTime,
    toLocaleString,
} from "../../utils/timeHelpers"

interface RegisterTourFormProps {
    id: string | undefined
    tour: DtoTourOut
}
export function RegisterTourForm({ id, tour: data }: RegisterTourFormProps) {
    const auth = useAuth()
    const toast = useToast()
    const [selectedTime, setSelectedTime] = useState<
        DtoTourTimeOut | undefined
    >(getClosetFutureDate(data.times))
    const { data: counter, refetch } =
        useGetToursByTourIdTimesAndTimeIdBookingsCountQuery({
            tourId: String(id),
            timeId: selectedTime?.id ?? "",
        })
    const [bookTour, { isLoading: isLoadingBooking }] =
        usePostToursByTourIdTimesAndTimeIdBookingsMutation()
    const [participants, setParticipants] = useState(1)
    const [email, setEmail] = useState<string>(auth.user?.email || "")
    const [name, setName] = useState<string>(auth.user?.data?.first_name || "")
    const [validateError, setValidateError] = useState<string>("")
    const [validateCountError, setValidateCountError] = useState<string>("")
    const [selectedDate, setSelectedDate] = useState<Date | null>(
        selectedTime ? new Date(selectedTime.start_time!) : null
    )
    const leftPlace = (data?.max_participants || 0) - (counter?.count || 0)
    const onTimeChanged = (date: Date | null) => {
        if (!date) return
        date.setDate(selectedDate?.getDate() || 0)
        date.setMilliseconds(0)
        const time = data?.times?.find(
            (t) =>
                toLocaleString(new Date(t.start_time!)) === toLocaleString(date)
        )
        if (time) {
            setSelectedTime(time)
        }
    }
    const validDates = useMemo<Date[]>(() => {
        if (!data.times) return []
        return data.times.map((t) => new Date(t.start_time!))
    }, [data.times])

    const validTimes = useMemo<Date[]>(() => {
        if (!selectedDate) {
            setSelectedTime(undefined)
            return []
        }
        const result = validDates.filter((d) => sameDay(d, selectedDate))
        if (result.length > 0) {
            onTimeChanged(result[0])
        } else {
            setSelectedTime(undefined)
        }
        return result
    }, [validDates, selectedDate])

    const onChange = (valStr: string, valNum: number) => {
        let number = Math.ceil(valNum)
        if (number > leftPlace) number = leftPlace
        setParticipants(number > 0 ? number : 1)
    }
    const onChangeEmail = (e: ChangeEvent<HTMLInputElement>) => {
        setEmail(e.target.value)
    }
    const onChangeName = (e: ChangeEvent<HTMLInputElement>) => {
        setName(e.target.value)
    }
    const onSubmit = () => {
        if (!email.toLowerCase().match(/^[\w-.]+@([\w-]+\.)+[a-z]{2,3}$/)) {
            setValidateError("Неверный формат email")
            return
        }
        if (!participants) {
            setValidateCountError("Введите количество участников")
            return
        }
        setValidateError("")
        setValidateCountError("")
        bookTour({
            tourId: id!,
            timeId: selectedTime?.id!,
            dtoBookingCredentials: {
                email,
                name,
                participants_count: participants,
            },
        })
            .then((resp) => {
                const error = getErrorMessage(resp)
                if (error) throw Error(error)
                toast({
                    title: `Вы забронировали место`,
                    description:
                        "Проверьте почту. Подробности экскурсии высланы на указанный email.",
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "info",
                    isClosable: true,
                })
                setName("")
                refetch()
                setParticipants(1)
                setEmail("")
            })
            .catch((err) => {
                console.error(err)
                toast({
                    title: `Ошибка при бронировании`,
                    description: String(err),
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "error",
                    isClosable: true,
                })
            })
    }

    const onDateChanged = (date: Date | null) => {
        setSelectedDate(date)
    }

    const filterTime = (date: Date) => {
        return !!validTimes?.some((t) => sameTime(date, t))
    }
    const filterDate = (date: Date) => {
        return !!validDates?.some((t) => sameDay(date, t))
    }
    return (
        <Flex direction={"column"} gap={5}>
            <Heading>Бронирование</Heading>
            <Flex direction={["column", "row"]} gap={2}>
                <DatePicker
                    dtype={"date"}
                    onDateChanged={onDateChanged}
                    selected={selectedDate}
                    filterDate={filterDate}
                />
                <DatePicker
                    dtype={"time"}
                    onDateChanged={onTimeChanged}
                    selected={
                        selectedTime
                            ? new Date(selectedTime?.start_time!)
                            : null
                    }
                    filterTime={filterTime}
                />
            </Flex>
            <Text fontSize={"lg"}>
                Осталось мест: <strong>{leftPlace}</strong>
            </Text>
            <Flex direction={["column", "column", "row"]} gap={5}>
                <FormControl isInvalid={!!validateCountError}>
                    <FormLabel>Количество участников</FormLabel>
                    <NumberInput
                        maxW={"100px"}
                        value={participants}
                        onChange={onChange}
                        min={1}
                        max={leftPlace}
                    >
                        <NumberInputField disabled={leftPlace <= 0} />
                        <NumberInputStepper>
                            <NumberIncrementStepper />
                            <NumberDecrementStepper />
                        </NumberInputStepper>
                    </NumberInput>

                    <FormErrorMessage>{validateCountError}</FormErrorMessage>
                </FormControl>
                <Spacer />
                <Heading mt={5} size={"lg"}>
                    {(isNaN(participants) ? 0 : participants) *
                        (data?.price ?? 0)}
                    ₽
                </Heading>
            </Flex>
            <FormControl>
                <FormLabel>Ваше имя</FormLabel>
                <Input
                    type={"text"}
                    placeholder="Петр"
                    value={name}
                    onChange={onChangeName}
                />
            </FormControl>
            <FormControl isInvalid={!!validateError}>
                <FormLabel>Ваша почта</FormLabel>
                <Input
                    type={"email"}
                    placeholder="myemail@mail.ru"
                    value={email}
                    onChange={onChangeEmail}
                />
                <FormErrorMessage>{validateError}</FormErrorMessage>
            </FormControl>
            <Button
                isLoading={isLoadingBooking}
                onClick={onSubmit}
                disabled={leftPlace <= 0}
            >
                Забронировать
            </Button>
        </Flex>
    )
}
