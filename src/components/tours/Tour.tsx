import { DtoTourOut, useGetGuidesByIdQuery } from "../../store/api/exportedApi"
import {
    Avatar,
    Button,
    Center,
    Flex,
    Heading,
    Icon,
    Text,
} from "@chakra-ui/react"
import Info404 from "../utils/Info404"
import { Role } from "../../types/user"
import { TiTick } from "react-icons/ti"
import { AiOutlineClose, AiOutlineFieldTime } from "react-icons/ai"
import { PhotosBlock } from "./PhotosBlock"
import { Link, useNavigate } from "react-router-dom"
import { getDurationString } from "../../utils/timeHelpers"
import BookingsList from "../booking/BookingsList"
import { ModerateTourForm } from "./ModerateTourForm"
import { RegisterTourForm } from "./RegisterTourForm"
import { DeleteTourForm } from "./DeleteTourForm"
import { useAuth } from "../../store/hooks/useAuth"
import { ReviewsTable } from "./ReviewsTable"

interface TourProps {
    id: string
    data: DtoTourOut | undefined
    onDoneModerate: () => void
}

export function Tour({ id, data, onDoneModerate }: TourProps) {
    const { data: guide, isLoading: isLoadingGuide } = useGetGuidesByIdQuery({
        id: data?.guide_id ?? "",
    })
    const auth = useAuth()
    const navigate = useNavigate()
    const isMyTour = !!data?.guide_id && auth?.user?.userId === data?.guide_id
    if (!data) {
        return (
            <Center>
                <Info404 />
            </Center>
        )
    }
    return (
        <>
            <Center>
                <Heading size={"2xl"} wordBreak={"break-word"}>
                    {data.title}, {data.country}
                    {(isMyTour || auth?.user?.role === Role.admin) &&
                        data?.moderation_status === "approved" && (
                            <Icon
                                ml={1}
                                boxSize={8}
                                top={1}
                                position={"relative"}
                                color={"green.300"}
                                as={TiTick}
                            />
                        )}
                    {(isMyTour || auth?.user?.role === Role.admin) &&
                        data?.moderation_status === "pending" && (
                            <Icon
                                ml={1}
                                boxSize={8}
                                top={1}
                                position={"relative"}
                                color={"tinkoff.500"}
                                as={AiOutlineFieldTime}
                            />
                        )}
                    {(isMyTour || auth?.user?.role === Role.admin) &&
                        data?.moderation_status === "rejected" && (
                            <Icon
                                ml={1}
                                boxSize={8}
                                top={1}
                                position={"relative"}
                                color={"red.500"}
                                as={AiOutlineClose}
                            />
                        )}
                </Heading>
            </Center>
            <PhotosBlock photos={data.photos} />
            <Flex direction={["column", "column", "row"]}>
                <Flex
                    flexGrow={2}
                    direction={"column"}
                    bg={"white"}
                    p={5}
                    borderRadius={5}
                >
                    {!isLoadingGuide && guide && (
                        <Link to={`/guide/${guide.id}`}>
                            <Flex>
                                <Avatar
                                    src={guide.avatar}
                                    bg={"tinkoff.200"}
                                    color={"#695B12"}
                                    size={"md"}
                                    name={
                                        guide.first_name + " " + guide.last_name
                                    }
                                />
                                <Text ml={2} mt={3}>
                                    {guide.first_name} {guide.last_name} -
                                    экскурсовод
                                </Text>
                            </Flex>
                        </Link>
                    )}
                    {data?.times && data.times[0] && (
                        <Flex mt={1}>
                            <Text fontWeight={600}>Длительность:</Text>
                            <Text ml={2}>
                                {getDurationString(
                                    data.times[0].start_time!,
                                    data.times[0].end_time!
                                )}
                            </Text>
                        </Flex>
                    )}
                    <Flex mt={1}>
                        <Text fontWeight={600}>Город:</Text>
                        <Text ml={2}>{data.region}</Text>
                    </Flex>
                    <Flex mt={1}>
                        <Text fontWeight={600}>Вид экскурсии:</Text>
                        <Text ml={2}>
                            {data.tags?.map((el) => el.title).join(", ")}
                        </Text>
                    </Flex>
                    <Flex mt={1}>
                        <Text fontWeight={600}>Цена:</Text>
                        <Text ml={2}>{data.price} ₽ с человека</Text>
                    </Flex>
                    <Text
                        whiteSpace={"pre-line"}
                        wordBreak={"break-word"}
                        maxW={"100%"}
                        mt={1}
                    >
                        {data.short_description}
                    </Text>
                    <Text
                        whiteSpace={"pre-line"}
                        wordBreak={"break-word"}
                        maxW={"100%"}
                        mt={1}
                    >
                        {data.description}
                    </Text>
                </Flex>

                <Flex
                    direction={"column"}
                    flexGrow={1}
                    mr={5}
                    ml={5}
                    mb={5}
                    mt={[5, 5, 0, 0]}
                    minW={"33vw"}
                    h={"100%"}
                >
                    <Flex
                        direction={"column"}
                        gap={5}
                        bg={"white"}
                        borderRadius={5}
                        mb={5}
                        p={3}
                    >
                        {isMyTour && data.times && (
                            <BookingsList
                                tourId={String(id)}
                                times={data.times}
                            />
                        )}
                        {auth?.user?.role === Role.admin && (
                            <ModerateTourForm onDone={onDoneModerate} id={id} />
                        )}
                        {!isMyTour && <RegisterTourForm id={id} tour={data} />}
                    </Flex>
                    {isMyTour && (
                        <Flex
                            direction={"row"}
                            gap={5}
                            bg={"white"}
                            borderRadius={5}
                            mb={5}
                            p={3}
                        >
                            <DeleteTourForm id={id} />
                            <Button
                                w={"50%"}
                                colorScheme={"tinkoff"}
                                onClick={() => navigate(`/tour/edit/${id}`)}
                                my={2}
                            >
                                Изменить
                            </Button>
                        </Flex>
                    )}
                </Flex>
            </Flex>
            <ReviewsTable tourId={id} />
        </>
    )
}
