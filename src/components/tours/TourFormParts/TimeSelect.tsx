import { Flex, FormControl, IconButton, Text } from "@chakra-ui/react"
import { AddIcon, CloseIcon } from "@chakra-ui/icons"
import { DatePicker } from "../../parts/DatePicker"
import React from "react"
import { TourTimeInternal } from "../../../utils/timeHelpers"

interface TimeSelectProps {
    tourTimes: TourTimeInternal[]
    setTourTimes: React.Dispatch<React.SetStateAction<TourTimeInternal[]>>
    startValidate: boolean
    isTimeStartInvalid: boolean
}
export function TimeSelect({
    tourTimes,
    setTourTimes,
    startValidate,
    isTimeStartInvalid,
}: TimeSelectProps) {
    const today = new Date(new Date().getTime())
    const addNewTime = () => {
        setTourTimes((last) => last.concat([{ timeStart: null }]))
    }
    return (
        <Flex direction={"column"} mt={4} mx={2} gap={2}>
            <Flex gap={2}>
                <Text mt={1}>Дата и время начала:</Text>
                <IconButton
                    size={"sm"}
                    icon={<AddIcon />}
                    aria-label={"Добавить дату и время"}
                    colorScheme={"gray"}
                    onClick={addNewTime}
                />
            </Flex>
            {tourTimes.map((time, i) => {
                return (
                    <Flex gap={2} mb={3} direction={["column", "row"]}>
                        <FormControl
                            isInvalid={startValidate && isTimeStartInvalid}
                        >
                            <DatePicker
                                autoFocus={i !== 0}
                                selected={time.timeStart}
                                onDateChanged={(e) =>
                                    setTourTimes((last) =>
                                        last.map((t, ind) => {
                                            if (ind === i)
                                                return { timeStart: e }
                                            return t
                                        })
                                    )
                                }
                                dtype={"datetime-local"}
                                minW={"215px"}
                                placeholder={"Время начала"}
                                minDate={today}
                            />
                        </FormControl>
                        {tourTimes.length > 1 && (
                            <IconButton
                                icon={<CloseIcon />}
                                aria-label={"Удалить"}
                                onClick={() =>
                                    setTourTimes((last) =>
                                        last.filter((t) => t !== time)
                                    )
                                }
                            />
                        )}
                    </Flex>
                )
            })}
        </Flex>
    )
}
