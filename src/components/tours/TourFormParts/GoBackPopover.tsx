import {
    Button,
    ButtonGroup,
    Popover,
    PopoverArrow,
    PopoverBody,
    PopoverCloseButton,
    PopoverContent,
    PopoverFooter,
    PopoverHeader,
    PopoverTrigger,
    useDisclosure,
} from "@chakra-ui/react"
import React from "react"
import { useNavigate } from "react-router-dom"

interface GoBackPopoverProps {
    anyEdit: boolean
}
export function GoBackPopover({ anyEdit }: GoBackPopoverProps) {
    const { isOpen, onToggle, onClose } = useDisclosure()

    const navigate = useNavigate()
    function goBack() {
        if (!anyEdit) navigate(-1)
        else onToggle()
    }

    return (
        <Popover returnFocusOnClose={false} isOpen={isOpen} onClose={onClose}>
            <PopoverTrigger>
                <Button
                    w={"50%"}
                    colorScheme={"gray"}
                    fontSize={"2xl"}
                    size={"lg"}
                    onClick={goBack}
                >
                    Отменить
                </Button>
            </PopoverTrigger>
            <PopoverContent>
                <PopoverHeader fontWeight="semibold">Выход</PopoverHeader>
                <PopoverArrow />
                <PopoverCloseButton />
                <PopoverBody>
                    Вы действительно хотите выйти? <br /> Данные не будут
                    сохранены.
                </PopoverBody>
                <PopoverFooter display="flex" justifyContent="flex-end">
                    <ButtonGroup size="sm">
                        <Button
                            variant="outline"
                            colorScheme="gray"
                            onClick={onToggle}
                        >
                            Отмена
                        </Button>
                        <Button
                            variant="outline"
                            colorScheme="red"
                            onClick={() => navigate(-1)}
                        >
                            Выйти
                        </Button>
                    </ButtonGroup>
                </PopoverFooter>
            </PopoverContent>
        </Popover>
    )
}
