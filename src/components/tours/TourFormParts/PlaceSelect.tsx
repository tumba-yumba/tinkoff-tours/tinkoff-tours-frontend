import { places } from "../../../data/places"
import { Center, Flex, FormControl } from "@chakra-ui/react"
import {
    AutoComplete,
    AutoCompleteInput,
    AutoCompleteItem,
    AutoCompleteList,
} from "@choc-ui/chakra-autocomplete"
import React from "react"

interface PlaceSelectProps {
    country: string
    defaultCountry: string
    startValidate: boolean
    isCountryInvalid: boolean
    city: string

    isCityInvalid: boolean
    defaultCity: string
    setCounty: (e: string) => void
    setCity: (e: string) => void
}

export function PlaceSelect({
    country,
    defaultCountry,
    city,
    setCity,
    setCounty,
    startValidate,
    isCountryInvalid,
    isCityInvalid,
    defaultCity,
}: PlaceSelectProps) {
    let cities: Array<string> = places[country] || []
    return (
        <Flex gap={5} px={2} direction={["column", "row"]}>
            <FormControl>
                <AutoComplete
                    openOnFocus
                    emptyState={<Center>Ничего не найдено</Center>}
                    value={country}
                    defaultValue={defaultCountry || ""}
                    onChange={(e) => setCounty(e)}
                    placeholder={"Страна"}
                >
                    <AutoCompleteInput
                        placeholder={"Страна"}
                        isInvalid={startValidate && isCountryInvalid}
                    />
                    <AutoCompleteList maxH={"250px"}>
                        {Object.keys(places).map((key) => (
                            <AutoCompleteItem
                                textTransform="capitalize"
                                value={key}
                                key={key}
                            >
                                {key}
                            </AutoCompleteItem>
                        ))}
                    </AutoCompleteList>
                </AutoComplete>
            </FormControl>
            <FormControl>
                <AutoComplete
                    openOnFocus
                    emptyState={<Center>Ничего не найдено</Center>}
                    value={city}
                    defaultValue={defaultCity || ""}
                    onChange={(e) => setCity(e)}
                    placeholder={"Город"}
                >
                    <AutoCompleteInput
                        placeholder={"Город"}
                        isInvalid={startValidate && isCityInvalid}
                    />
                    <AutoCompleteList maxH={"250px"}>
                        {cities.map((key) => (
                            <AutoCompleteItem
                                textTransform="capitalize"
                                value={key}
                                key={key}
                            >
                                {key}
                            </AutoCompleteItem>
                        ))}
                    </AutoCompleteList>
                </AutoComplete>
            </FormControl>
        </Flex>
    )
}
