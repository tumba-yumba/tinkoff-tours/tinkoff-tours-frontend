import React, { ChangeEvent, useRef } from "react"
import { MAX_FILE_SIZE_MB, MAX_FILES_PER_TOUR } from "../../../configs"
import { VALID_PHOTO_TYPES } from "../../../data/fileTypes"
import { getErrorMessage } from "../../../utils/errorParser"
import {
    DtoPhotoIn,
    DtoPhotoOut,
    useDeleteToursPhotosMutation,
    usePostToursPhotosMutation,
} from "../../../store/api/exportedApi"
import {
    Box,
    Center,
    Icon,
    IconButton,
    Image,
    Spinner,
    useToast,
} from "@chakra-ui/react"
import { CloseIcon } from "@chakra-ui/icons"
import { Slider } from "../../utils/SLider"
import { MdAddAPhoto } from "react-icons/md"

interface PhotoTourUploaderProps {
    uploadedPhotos: DtoPhotoOut[]
    setUploadedPhotos: React.Dispatch<React.SetStateAction<DtoPhotoOut[]>>
    loadings: Array<string>
    setLoadings: React.Dispatch<React.SetStateAction<string[]>>
}

export function PhotoTourUploader({
    uploadedPhotos,
    setUploadedPhotos,
    loadings,
    setLoadings,
}: PhotoTourUploaderProps) {
    const inputFileRef = useRef<HTMLInputElement>(null)
    const sliderRef = useRef(null)
    const [uploadFile] = usePostToursPhotosMutation()
    const [deleteFile] = useDeleteToursPhotosMutation()

    const toast = useToast()
    const selectFile = () => {
        if (inputFileRef && inputFileRef.current !== null) {
            inputFileRef.current.click()
        }
    }
    const uploadPhoto = (e: ChangeEvent<HTMLInputElement>) => {
        const photos = e.target.files
        if (!photos) return
        console.log(photos)
        if (
            photos.length >
            MAX_FILES_PER_TOUR - loadings.length - uploadedPhotos.length
        ) {
            toast({
                title: "Слишком много файлов",
                description: `Максимум ${MAX_FILES_PER_TOUR} файлов для одной экскурсии.`,
                position: "bottom-right",
                variant: "left-accent",
                status: "error",
                isClosable: true,
            })
            return
        }
        let anyUploading = false
        for (const photo of photos) {
            if (photo.size / (1024 * 1024) >= MAX_FILE_SIZE_MB) {
                toast({
                    title: "Файл слишком большой",
                    description: `Размер ${photo.name} больше ${MAX_FILE_SIZE_MB}МБ. Выберите файл поменьше.`,
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "warning",
                    isClosable: true,
                })
                continue
            }
            const extension = photo.name.split(".").pop()
            if (
                !extension ||
                VALID_PHOTO_TYPES.indexOf(extension.toLowerCase()) === -1
            ) {
                toast({
                    title: "Не валидный файл",
                    description: `Расширение файла ${photo.name} не валидно. Выберите изображение.`,
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "warning",
                    isClosable: true,
                })
                continue
            }
            anyUploading = true
            const uploadData = new FormData()
            uploadData.append("photo", photo)
            uploadFile({ body: uploadData as unknown as { photo: Blob } })
                .then((resp: any) => {
                    const error = getErrorMessage(resp)
                    if (error) {
                        toast({
                            title: `Ошибка при загрузке ${photo.name}.`,
                            description: error,
                            position: "bottom-right",
                            variant: "left-accent",
                            status: "warning",
                            isClosable: true,
                        })
                    } else {
                        const gotPhoto = resp.data as DtoPhotoOut
                        setUploadedPhotos((prev) => [gotPhoto].concat(prev))
                    }
                    setLoadings((prev) =>
                        prev.filter((load) => load !== photo.name)
                    )
                })
                .catch((err) => {
                    console.error(err)
                    toast({
                        title: `Ошибка при загрузке ${photo.name}.`,
                        description: String(err),
                        position: "bottom-right",
                        variant: "left-accent",
                        status: "warning",
                        isClosable: true,
                    })
                    setLoadings((prev) =>
                        prev.filter((load) => load !== photo.name)
                    )
                })

            setLoadings((prev) => prev.concat([photo.name]))
        }
        if (anyUploading) {
            // @ts-ignore
            sliderRef.current.goNext()
        }
    }
    const getLoadingsList: Function = (): React.ReactElement[] =>
        loadings.map((el) => (
            <Center
                key={el}
                p={5}
                bg={"gray.300"}
                borderRadius={"lg"}
                h="298px"
                maxW={"500px"}
            >
                <Spinner
                    thickness="4px"
                    speed="0.65s"
                    emptyColor="gray.200"
                    color="purpleT.500"
                    size="xl"
                />
            </Center>
        ))

    function deletePhoto(photo: DtoPhotoOut) {
        deleteFile({ dtoPhotoIn: photo as DtoPhotoIn })
            .then((resp: any) => {
                const error = getErrorMessage(resp)
                if (error) throw Error(error)
                setUploadedPhotos((prev) => prev.filter((el) => el !== photo))
            })
            .catch((err) => {
                console.error(err)
                toast({
                    title: `Ошибка при удалении.`,
                    description: String(err),
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "warning",
                    isClosable: true,
                })
            })
    }

    const getPhotosList: Function = (): React.ReactElement[] =>
        uploadedPhotos.map((el) => (
            <Box h={"298px"} maxW={"500px"} key={el.id}>
                <IconButton
                    onClick={() => deletePhoto(el)}
                    aria-label="Удалить картинку"
                    icon={<CloseIcon />}
                    position={"absolute"}
                    top={5}
                    right={5}
                    size="sm"
                    opacity={0.8}
                    colorScheme={"tinkoff"}
                />
                <Image
                    h={"100%"}
                    objectFit={"cover"}
                    w={"100%"}
                    src={el.path}
                    borderRadius={5}
                />
            </Box>
        ))
    return (
        <>
            <Slider shouldSwiperUpdate loop={false} ref={sliderRef}>
                <Center
                    p={6}
                    bg={"gray.300"}
                    borderRadius={"lg"}
                    maxW={"500px"}
                    onClick={selectFile}
                >
                    <Icon as={MdAddAPhoto} color={"white"} boxSize={"250px"} />
                </Center>
                {getLoadingsList()}
                {getPhotosList()}
            </Slider>
            <input
                type="file"
                multiple={true}
                onChange={uploadPhoto}
                ref={inputFileRef}
                style={{ display: "none" }}
            />
        </>
    )
}
