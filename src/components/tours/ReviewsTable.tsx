import { Center, Flex, Heading, Spinner } from "@chakra-ui/react"
import React from "react"
import ReactStars from "react-stars"
import { PointsCountBlock } from "../raiting/PointsCountBlock"
import { ReviewCard } from "../raiting/ReviewCard"
import {
    useGetToursByIdEstimationsQuery,
    useGetToursByIdFeedbacksQuery,
} from "../../store/api/exportedApi"

interface ReviewsTableProps {
    tourId: string
}

export function ReviewsTable({ tourId }: ReviewsTableProps) {
    const { data, isLoading } = useGetToursByIdFeedbacksQuery({ id: tourId })
    const { data: est } = useGetToursByIdEstimationsQuery({ id: tourId })

    return (
        <Flex direction={"column"} m={5} gap={4}>
            <Center>
                <Flex
                    direction={"column"}
                    alignItems={"center"}
                    textAlign={"center"}
                >
                    {data && data.items && data.items.length > 0 && (
                        <Heading ml={2} fontSize={"43px"}>
                            Отзывы ({data.items.length})
                        </Heading>
                    )}
                    {est && (
                        <ReactStars
                            count={5}
                            edit={false}
                            value={est.tour_rating}
                            size={50}
                            half={true}
                        />
                    )}
                </Flex>
            </Center>
            {est && <PointsCountBlock estimationTour={est} />}
            {data &&
                data.items &&
                data.items.map((item) => <ReviewCard review={item} />)}
            {isLoading && (
                <Center m={5}>
                    <Spinner />
                </Center>
            )}
        </Flex>
    )
}
