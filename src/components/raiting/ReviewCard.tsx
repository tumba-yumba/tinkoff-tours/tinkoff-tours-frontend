import { Button, Flex, Icon, Image, Text } from "@chakra-ui/react"
import ReactStars from "react-stars"
import React from "react"
import { DtoFeedbackInList } from "../../store/api/exportedApi"
import { ComplaintButton } from "../parts/ComplaintButton"
import { useAuth } from "../../store/hooks/useAuth"
import { Role } from "../../types/user"
import { GiMountaintop } from "react-icons/gi"
import { HiThumbDown, HiThumbUp } from "react-icons/hi"
import { FaFireAlt } from "react-icons/fa"
import { AiFillEyeInvisible } from "react-icons/ai"
import { BsFillLightningFill } from "react-icons/bs"

interface ReviewCardProps {
    review: DtoFeedbackInList

    onModerate?: (review: DtoFeedbackInList) => void
}

export function ReviewCard({ review, onModerate }: ReviewCardProps) {
    const auth = useAuth()
    const onClick = () => {
        if (onModerate) onModerate(review)
    }
    return (
        <Flex direction={"column"} p={3} bg={"white"}>
            <Flex justifyContent={"space-between"} ml={1} mr={1}>
                <Flex direction={"column"}>
                    <Flex>
                        <Text decoration={"underline"} fontSize={"25px"}>
                            {review.name}
                        </Text>
                        <ComplaintButton size={"md"} />
                    </Flex>
                </Flex>
                <Flex direction={"column"}>
                    <Text fontSize={"13px"} mb={-3} color={"gray"}>
                        {new Date(review.created_at + "Z").toLocaleDateString(
                            [],
                            {
                                dateStyle: "short",
                            }
                        )}
                    </Text>
                </Flex>
            </Flex>
            <Flex
                direction={["column", "row"]}
                mt={-4}
                justifyContent={["flex-start", "space-between"]}
            >
                <ReactStars
                    count={5}
                    edit={false}
                    value={review.tour_rating}
                    size={40}
                    half={false}
                />
                <Flex gap={1} mt={2}>
                    {review.picturesque && (
                        <Icon
                            as={GiMountaintop}
                            color={"purpleT.400"}
                            boxSize={9}
                            mr={1}
                        />
                    )}
                    {review.comfortable && (
                        <Icon
                            as={HiThumbUp}
                            color={"purpleT.400"}
                            boxSize={9}
                            mr={1}
                        />
                    )}
                    {review.adrenaline && (
                        <Icon
                            as={FaFireAlt}
                            color={"purpleT.400"}
                            boxSize={9}
                            mr={1}
                        />
                    )}
                    {review.not_impressed && (
                        <Icon
                            as={AiFillEyeInvisible}
                            color={"purpleT.400"}
                            boxSize={9}
                            mr={1}
                        />
                    )}
                    {review.bad_weather && (
                        <Icon
                            as={BsFillLightningFill}
                            color={"purpleT.400"}
                            boxSize={9}
                            mr={1}
                        />
                    )}
                    {review.uncomfortable && (
                        <Icon
                            as={HiThumbDown}
                            color={"purpleT.400"}
                            boxSize={9}
                            mr={1}
                        />
                    )}
                </Flex>
            </Flex>

            <Flex mb={3} ml={1} mr={1}>
                <Text
                    fontSize={"20px"}
                    fontWeight={"light"}
                    wordBreak={"break-all"}
                >
                    {review.comment}
                </Text>
            </Flex>
            <Flex
                justifyContent={"space-between"}
                direction={["column", "row"]}
            >
                <Flex gap={5} direction={["column", "row"]} flexWrap={"wrap"}>
                    {review.photos?.map((photo) => (
                        <Image src={photo} w={180} h={180} borderRadius={5} />
                    ))}
                </Flex>
                {auth.user?.role === Role.admin && onModerate && (
                    <Flex alignSelf={"flex-end"} gap={2}>
                        <Button
                            colorScheme={"gray"}
                            color={"black"}
                            onClick={onClick}
                        >
                            Удалить
                        </Button>
                        <Button onClick={onClick}>Принять</Button>
                    </Flex>
                )}
            </Flex>
        </Flex>
    )
}
