import { Flex } from "@chakra-ui/react"
import { AiFillEyeInvisible } from "react-icons/ai"
import {
    BsFillLightningFill,
    BsFillQuestionOctagonFill,
    BsThreeDots,
} from "react-icons/bs"
import { HiThumbDown, HiThumbUp } from "react-icons/hi"
import { GiMountaintop } from "react-icons/gi"
import { FaFireAlt, FaGraduationCap, FaGrin } from "react-icons/fa"
import { PointBlock } from "./PointBlock"
import { MdHearingDisabled, MdMarkChatRead } from "react-icons/md"
import {
    DtoGuideEstimationsOut,
    DtoTourEstimationsOut,
} from "../../store/api/exportedApi"

interface PointsCountBlockProps {
    estimationGuide?: DtoGuideEstimationsOut
    estimationTour?: DtoTourEstimationsOut
}

export function PointsCountBlock({
    estimationGuide,
    estimationTour,
}: PointsCountBlockProps) {
    return (
        <Flex
            direction={["column", "column", "column", "row"]}
            bg={"white"}
            p={5}
            gap={50}
            justifyContent={"center"}
            alignItems={"center"}
        >
            {estimationTour && (
                <>
                    <Flex direction={["column", "row"]} gap={5}>
                        <PointBlock
                            count={estimationTour.picturesque!.count!}
                            icon={GiMountaintop}
                            text={"Живописно"}
                        />
                        <PointBlock
                            count={estimationTour.comfortable!.count!}
                            icon={HiThumbUp}
                            text={"Комфортно"}
                        />
                        <PointBlock
                            count={estimationTour.adrenaline!.count!}
                            icon={FaFireAlt}
                            text={"Адреналин"}
                        />
                    </Flex>
                    <Flex direction={["column", "row"]} gap={5}>
                        <PointBlock
                            count={estimationTour.not_impressed!.count!}
                            icon={AiFillEyeInvisible}
                            text={"Не впечатлило"}
                        />
                        <PointBlock
                            count={estimationTour.bad_weather!.count!}
                            icon={BsFillLightningFill}
                            text={"Непогода"}
                        />
                        <PointBlock
                            count={estimationTour.uncomfortable!.count!}
                            icon={HiThumbDown}
                            text={"Некомфортно"}
                        />
                    </Flex>
                </>
            )}
            {estimationGuide && (
                <>
                    <Flex direction={["column", "row"]} gap={5}>
                        <PointBlock
                            count={estimationGuide.interesting!.count!}
                            icon={MdMarkChatRead}
                            text={"Интересно"}
                        />
                        <PointBlock
                            count={estimationGuide.know_everything!.count!}
                            icon={FaGraduationCap}
                            text={"Всё знает"}
                        />
                        <PointBlock
                            count={estimationGuide.entertainment!.count!}
                            icon={FaGrin}
                            text={"Развлёк"}
                        />
                    </Flex>
                    <Flex direction={["column", "row"]} gap={5}>
                        <PointBlock
                            count={estimationGuide.boring!.count!}
                            icon={BsThreeDots}
                            text={"Скучно"}
                        />
                        <PointBlock
                            count={estimationGuide.know_nothing!.count!}
                            icon={BsFillQuestionOctagonFill}
                            text={"Ничего не знает"}
                        />
                        <PointBlock
                            count={estimationGuide.not_heard!.count!}
                            icon={MdHearingDisabled}
                            text={"Не слышно"}
                        />
                    </Flex>
                </>
            )}
        </Flex>
    )
}
