import ReactStars from "react-stars"
import { useEffect, useState } from "react"
import { Flex, Text } from "@chakra-ui/react"
import { IconCheckbox } from "../parts/IconCheckbox"
import { AiFillEyeInvisible } from "react-icons/ai"
import {
    BsFillLightningFill,
    BsFillQuestionOctagonFill,
    BsThreeDots,
} from "react-icons/bs"
import { GiMountaintop } from "react-icons/gi"
import { HiThumbUp, HiThumbDown } from "react-icons/hi"
import { FaFireAlt } from "react-icons/fa"
import { MdHearingDisabled, MdMarkChatRead } from "react-icons/md"
import { FaGrin, FaGraduationCap } from "react-icons/fa"
import { DtoEstimationsIn } from "../../store/api/exportedApi"
type RatingType = "tour" | "guide" | "fixed"
interface RatingInputProps {
    type: RatingType
    onChange?: (raitng: number, estimations: DtoEstimationsIn) => void
    fixed?: boolean
}
const DEFAULT_CHECK: DtoEstimationsIn = {
    adrenaline: false,
    boring: false,
    comfortable: false,
    know_nothing: false,
    not_heard: false,
    picturesque: false,
    bad_weather: false,
    entertainment: false,
    interesting: false,
    know_everything: false,
    not_impressed: false,
    uncomfortable: false,
}

const GUIDE_FIELDS = [
    "know_nothing",
    "know_everything",
    "not_heard",
    "entertainment",
    "boring",
    "interesting",
]
export function RatingInput({ type, fixed, onChange }: RatingInputProps) {
    const [raiting, setRaiting] = useState(0)
    const [estimations, setEstimations] =
        useState<DtoEstimationsIn>(DEFAULT_CHECK)

    const ratingChanged = (nextValue: number) => {
        if (fixed) return
        setEstimations(DEFAULT_CHECK)
        setRaiting(nextValue)
    }
    useEffect(() => {
        if (onChange) {
            let est = { ...estimations }
            for (const field in est) {
                if (
                    (type !== "guide" && GUIDE_FIELDS.includes(field)) ||
                    (type !== "tour" && !GUIDE_FIELDS.includes(field))
                )
                    delete est[field as keyof DtoEstimationsIn]
            }
            onChange(raiting, est)
        }
    }, [raiting, estimations])

    return (
        <Flex
            direction={["column", "column", "row"]}
            justifyContent={["center", "center", "space-between"]}
            alignItems={"center"}
            w={"100%"}
        >
            <Flex direction={"column"} w={"220px"}>
                {!fixed && (
                    <Text>
                        Оцените {type === "tour" ? "экскурсию" : "гида"}
                    </Text>
                )}
                <ReactStars
                    count={5}
                    edit={!fixed}
                    value={raiting}
                    onChange={ratingChanged}
                    size={50}
                    half={false}
                />
            </Flex>
            {!fixed && raiting > 0 && raiting < 4 && (
                <Flex
                    direction={"column"}
                    justifyContent={"center"}
                    alignContent={"center"}
                    w={"390px"}
                >
                    <Text textAlign={"center"}>Что не понравилось?</Text>
                    <Flex direction={"row"} justifyContent={"space-between"}>
                        {type === "tour" ? (
                            <>
                                <IconCheckbox
                                    w={"130px"}
                                    iconBlock={AiFillEyeInvisible}
                                    name={"impress"}
                                    isChecked={estimations.not_impressed}
                                    onChange={(e) =>
                                        setEstimations((prev) => {
                                            return {
                                                ...prev,
                                                not_impressed: e.target.checked,
                                            }
                                        })
                                    }
                                >
                                    {" "}
                                    Не впечатлило
                                </IconCheckbox>
                                <IconCheckbox
                                    w={"130px"}
                                    iconBlock={BsFillLightningFill}
                                    name={"weather"}
                                    isChecked={estimations.bad_weather}
                                    onChange={(e) =>
                                        setEstimations((prev) => {
                                            return {
                                                ...prev,
                                                bad_weather: e.target.checked,
                                            }
                                        })
                                    }
                                >
                                    {" "}
                                    Непогода
                                </IconCheckbox>
                                <IconCheckbox
                                    w={"130px"}
                                    iconBlock={HiThumbDown}
                                    name={"uncomfortable"}
                                    isChecked={estimations.uncomfortable}
                                    onChange={(e) =>
                                        setEstimations((prev) => {
                                            return {
                                                ...prev,
                                                uncomfortable: e.target.checked,
                                            }
                                        })
                                    }
                                >
                                    {" "}
                                    Некомфортно
                                </IconCheckbox>
                            </>
                        ) : (
                            <>
                                <IconCheckbox
                                    w={"130px"}
                                    iconBlock={BsThreeDots}
                                    name={"boring"}
                                    isChecked={estimations["boring"]}
                                    onChange={(e) =>
                                        setEstimations((prev) => {
                                            return {
                                                ...prev,
                                                boring: e.target.checked,
                                            }
                                        })
                                    }
                                >
                                    Скучно
                                </IconCheckbox>
                                <IconCheckbox
                                    w={"130px"}
                                    iconBlock={BsFillQuestionOctagonFill}
                                    name={"unknown"}
                                    isChecked={estimations["know_nothing"]}
                                    onChange={(e) =>
                                        setEstimations((prev) => {
                                            return {
                                                ...prev,
                                                know_nothing: e.target.checked,
                                            }
                                        })
                                    }
                                >
                                    Ничего не знает
                                </IconCheckbox>
                                <IconCheckbox
                                    w={"130px"}
                                    iconBlock={MdHearingDisabled}
                                    name={"sound"}
                                    isChecked={estimations["not_heard"]}
                                    onChange={(e) =>
                                        setEstimations((prev) => {
                                            return {
                                                ...prev,
                                                not_heard: e.target.checked,
                                            }
                                        })
                                    }
                                >
                                    Не слышно
                                </IconCheckbox>
                            </>
                        )}
                    </Flex>
                </Flex>
            )}
            {raiting >= 4 && (
                <Flex
                    direction={"column"}
                    justifyContent={"center"}
                    alignContent={"center"}
                    w={"390px"}
                >
                    <Text textAlign={"center"}>Что понравилось?</Text>
                    <Flex direction={"row"} justifyContent={"space-between"}>
                        {type === "tour" ? (
                            <>
                                <IconCheckbox
                                    w={"130px"}
                                    iconBlock={GiMountaintop}
                                    name={"scenic"}
                                    isChecked={estimations.picturesque}
                                    onChange={(e) =>
                                        setEstimations((prev) => {
                                            return {
                                                ...prev,
                                                picturesque: e.target.checked,
                                            }
                                        })
                                    }
                                >
                                    Живописно
                                </IconCheckbox>
                                <IconCheckbox
                                    w={"130px"}
                                    iconBlock={HiThumbUp}
                                    name={"comfortable"}
                                    isChecked={estimations.comfortable}
                                    onChange={(e) =>
                                        setEstimations((prev) => {
                                            return {
                                                ...prev,
                                                comfortable: e.target.checked,
                                            }
                                        })
                                    }
                                >
                                    {" "}
                                    Комфортно
                                </IconCheckbox>
                                <IconCheckbox
                                    w={"130px"}
                                    iconBlock={FaFireAlt}
                                    name={"action"}
                                    isChecked={estimations.adrenaline}
                                    onChange={(e) =>
                                        setEstimations((prev) => {
                                            return {
                                                ...prev,
                                                adrenaline: e.target.checked,
                                            }
                                        })
                                    }
                                >
                                    {" "}
                                    Адреналин
                                </IconCheckbox>
                            </>
                        ) : (
                            <>
                                <IconCheckbox
                                    w={"130px"}
                                    iconBlock={MdMarkChatRead}
                                    name={"interesting"}
                                    isChecked={estimations.interesting}
                                    onChange={(e) =>
                                        setEstimations((prev) => {
                                            return {
                                                ...prev,
                                                interesting: e.target.checked,
                                            }
                                        })
                                    }
                                >
                                    Интересно
                                </IconCheckbox>
                                <IconCheckbox
                                    w={"130px"}
                                    iconBlock={FaGraduationCap}
                                    name={"known"}
                                    isChecked={estimations.know_everything}
                                    onChange={(e) =>
                                        setEstimations((prev) => {
                                            return {
                                                ...prev,
                                                know_everything:
                                                    e.target.checked,
                                            }
                                        })
                                    }
                                >
                                    Всё знает
                                </IconCheckbox>
                                <IconCheckbox
                                    w={"130px"}
                                    iconBlock={FaGrin}
                                    name={"funny"}
                                    isChecked={estimations.entertainment}
                                    onChange={(e) =>
                                        setEstimations((prev) => {
                                            return {
                                                ...prev,
                                                entertainment: e.target.checked,
                                            }
                                        })
                                    }
                                >
                                    Развлёк
                                </IconCheckbox>
                            </>
                        )}
                    </Flex>
                </Flex>
            )}
        </Flex>
    )
}
