import { Flex, Icon, Text } from "@chakra-ui/react"

import { IconType } from "react-icons/lib"

interface PointsCountBlockProps {
    count: number
    icon: IconType
    text: string
}
export function PointBlock({ count, icon, text }: PointsCountBlockProps) {
    return (
        <Flex direction={"column"} alignItems={"center"}>
            <Text color="gray.700" fontSize={16}>
                {text}
            </Text>
            <Flex justifyContent={"center"} alignItems={"center"} w={"130px"}>
                <Icon as={icon} color={"purpleT.400"} boxSize={9} mr={1} />
                {count}
            </Flex>
        </Flex>
    )
}
