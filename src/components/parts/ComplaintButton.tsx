import {
    Button,
    ButtonGroup,
    Heading,
    IconButton,
    Popover,
    PopoverArrow,
    PopoverCloseButton,
    PopoverContent,
    PopoverTrigger,
    Stack,
    Textarea,
    useDisclosure,
    useToast,
} from "@chakra-ui/react"
import { TiWarningOutline } from "react-icons/ti"
import React, { useState } from "react"

interface ComplaintButtonProps {
    size: "xl" | "md" | "lg"
}
export function ComplaintButton({ size }: ComplaintButtonProps) {
    const { onOpen, onClose, isOpen } = useDisclosure()
    const [message, setMessage] = useState("")
    const toast = useToast()
    const onSend = () => {
        onClose()
        setTimeout(() => {
            setMessage("")
            toast({
                title: `Спасибо. Мы рассмотрим вашу жалобу.`,
                position: "bottom-right",
                variant: "left-accent",
                status: "info",
                isClosable: true,
            })
        }, 200)
    }
    return (
        <Popover
            isOpen={isOpen}
            onOpen={onOpen}
            onClose={onClose}
            placement="right"
            closeOnBlur={false}
        >
            <PopoverTrigger>
                <IconButton
                    icon={<TiWarningOutline />}
                    colorScheme={"outline"}
                    top={-1}
                    ml={1}
                    size={size}
                    _hover={{ color: "red" }}
                    aria-label={"Пожаловаться"}
                    position={"relative"}
                    color={"gray"}
                />
            </PopoverTrigger>
            <PopoverContent p={5}>
                <PopoverArrow />
                <PopoverCloseButton />
                <Stack spacing={4}>
                    <Heading size={"md"}>Пожаловаться</Heading>
                    <Textarea
                        value={message}
                        placeholder={"Расскажите, что случилось"}
                        onChange={(e) => setMessage(e.target.value)}
                    />
                    <ButtonGroup display="flex" justifyContent="flex-end">
                        <Button variant="outline" onClick={onClose}>
                            Отмена
                        </Button>
                        <Button onClick={onSend} colorScheme="tinkoff">
                            Отрпавить
                        </Button>
                    </ButtonGroup>
                </Stack>
            </PopoverContent>
        </Popover>
    )
}
