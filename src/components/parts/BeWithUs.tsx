import { Box, Flex, Img, Text } from "@chakra-ui/react"
import { Link as Href } from "react-router-dom"
import logo from "../../assets/logo.svg"
import guide from "../../assets/image/tour_guide.svg"
import React from "react"

export function BeWithUs() {
    return (
        <Box minWidth="50vw" p={7} pb={0} bg={"white"}>
            <Flex ml={3} alignItems={"center"} gap={2} mb={5}>
                <Href to="/">
                    <Img src={logo} maxWidth={"50px"} p={1} />
                </Href>
                <Href to="/">
                    <Text fontWeight={700}>ТумбаЮмба.Экскурсии</Text>
                </Href>
            </Flex>
            <Flex mt={10}>
                <Text ml={3} mt={10} fontSize="2.1em" fontWeight={600}>
                    Станьте экскурсоводом <br />
                    вместе с ТумбаЮмба.Экскурсии!
                </Text>
            </Flex>
            <Img mt={-5} src={guide} w={"calc(100% - 100px)"} />
        </Box>
    )
}
