import { Center, Divider, FormControl, FormLabel } from "@chakra-ui/react"
import {
    AutoComplete,
    AutoCompleteInput,
    AutoCompleteItem,
    AutoCompleteList,
    AutoCompleteTag,
} from "@choc-ui/chakra-autocomplete"
import React, { useMemo } from "react"
import { DtoTagOut, useGetTagsQuery } from "../../store/api/exportedApi"
interface TagsSelectProps {
    value: DtoTagOut[]
    onChange: (tags: DtoTagOut[]) => void
    labelWithDivider?: boolean
}

interface LabelToIdDict {
    [key: string]: DtoTagOut
}

export function TagSelect(props: TagsSelectProps) {
    const { value, onChange, labelWithDivider } = props
    const { data = { tags: [] } } = useGetTagsQuery()

    const labelToTag = useMemo<LabelToIdDict>(() => {
        if (!data.tags) return {}
        const ans: any = {}
        data.tags.forEach((tag) => {
            ans[tag.title!] = tag
        })
        return ans
    }, [data.tags])

    return (
        <FormControl px={2}>
            {labelWithDivider && (
                <>
                    <FormLabel fontWeight={800} fontSize={"lg"} mt={3}>
                        Теги
                    </FormLabel>
                    <Divider colorScheme="black" mt="-2" mb="3" />
                </>
            )}
            {!labelWithDivider && <FormLabel>Теги</FormLabel>}
            <AutoComplete
                openOnFocus
                multiple
                restoreOnBlurIfEmpty={false}
                listAllValuesOnFocus
                value={value.map((tag) => tag.title!)}
                emptyState={<Center>Ничего не найдено</Center>}
                onChange={(e) =>
                    onChange(e.map((tag: string) => labelToTag[tag]))
                }
            >
                <AutoCompleteInput placeholder="Поиск по тегам">
                    {({ tags }) =>
                        tags.map((tag, tid) => (
                            <AutoCompleteTag
                                key={tid}
                                label={tag.label}
                                onRemove={tag.onRemove}
                            />
                        ))
                    }
                </AutoCompleteInput>
                <AutoCompleteList maxH={"250px"}>
                    {data.tags?.map((tag) => (
                        <AutoCompleteItem
                            textTransform="capitalize"
                            key={tag.id}
                            value={tag.title}
                            _selected={{ bg: "tinkoff.100" }}
                            _focus={{ bg: "gray.50" }}
                        >
                            {" "}
                            {tag.title}
                        </AutoCompleteItem>
                    ))}
                </AutoCompleteList>
            </AutoComplete>
        </FormControl>
    )
}
