import {
    chakra,
    useCheckbox,
    Text,
    CheckboxProps,
    Icon,
} from "@chakra-ui/react"
import { IconType } from "react-icons/lib"

interface IconCheckboxProps extends CheckboxProps {
    iconBlock: IconType
}
export function IconCheckbox(props: IconCheckboxProps) {
    const { state, getInputProps, getLabelProps, htmlProps } =
        useCheckbox(props)

    return (
        <chakra.label
            display="flex"
            flexDirection="column"
            alignItems="center"
            gridColumnGap={2}
            maxW="36"
            px={3}
            py={1}
            whiteSpace={"nowrap"}
            cursor="pointer"
            {...htmlProps}
        >
            <input {...getInputProps()} hidden />

            <Icon
                as={props.iconBlock}
                color={state.isChecked ? "purpleT.400" : "gray.400"}
                boxSize={10}
            />
            <Text color="gray.700" fontSize={16} {...getLabelProps()}>
                {props.children}
            </Text>
        </chakra.label>
    )
}
