import { InputProps } from "@chakra-ui/input"
import { Input, InputGroup, InputRightElement } from "@chakra-ui/react"
import { CalendarIcon, TimeIcon } from "@chakra-ui/icons"
import React from "react"
import { default as DatePick } from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css"
import "../../components/parts/date-picker.css"
import { ru } from "date-fns/locale"

interface DatePickerProps extends InputProps {
    dtype: DateTypes
    size?: string
    onDateChanged: (date: Date | null) => void
    selected: Date | null
    placeholder?: string
    selectsStart?: boolean
    selectsEnd?: boolean
    startDate?: Date | null
    endDate?: Date | null
    minDate?: Date | null
    maxTime?: string
    timeCaption?: string
    filterDate?: (date: Date) => boolean
    filterTime?: (date: Date) => boolean
}

type DateTypes = "date" | "datetime-local" | "time"

export function DatePicker(props: DatePickerProps) {
    let dateFormat: string = "dd.MM.yyyy HH:mm"
    if (props.dtype === "date") dateFormat = "dd.MM.yyyy"
    if (props.dtype === "time") dateFormat = "HH:mm"
    let maxTime: Date | undefined = undefined
    let minTime: Date | undefined = undefined
    if (props.maxTime) {
        const t = props.maxTime.split(":")
        maxTime = new Date(new Date().setHours(parseInt(t[0])))
        maxTime = new Date(maxTime.setMinutes(parseInt(t[1])))
        minTime = new Date(new Date().setHours(0))
        minTime = new Date(minTime.setMinutes(15))
    }
    return (
        <InputGroup className={props.size} minW={props.minW}>
            <InputRightElement
                pointerEvents="none"
                children={
                    props.dtype === "time" ? (
                        <TimeIcon
                            color="black"
                            mt={props.size === "lg" ? 2 : 0}
                            zIndex={0}
                            mr={props.size === "lg" ? 3 : 2}
                        />
                    ) : (
                        <CalendarIcon
                            color="black"
                            mt={props.size === "lg" ? 2 : 0}
                            zIndex={0}
                            mr={props.size === "lg" ? 3 : 2}
                        />
                    )
                }
            />

            <DatePick
                selected={props.selected}
                onChange={props.onDateChanged}
                startDate={props.startDate}
                endDate={props.endDate}
                selectsStart={props.selectsStart}
                selectsEnd={props.selectsEnd}
                minDate={props.minDate}
                locale={ru}
                autoFocus={props.autoFocus}
                dateFormat={dateFormat}
                timeIntervals={15}
                maxTime={maxTime}
                minTime={minTime}
                openToDate={minTime}
                filterDate={props.filterDate}
                filterTime={props.filterTime}
                timeCaption={props.timeCaption ?? "Время"}
                showTimeSelectOnly={props.dtype === "time"}
                showTimeSelect={props.dtype !== "date"}
                placeholderText={props.placeholder}
                customInput={
                    <Input
                        autoComplete={"off"}
                        aria-autocomplete={"inline"}
                        name={"date"}
                        {...(props as InputProps)}
                    />
                }
            />
        </InputGroup>
    )
}
