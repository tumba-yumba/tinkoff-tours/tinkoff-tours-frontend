import {
    Button,
    ButtonGroup,
    FormControl,
    FormErrorMessage,
    FormLabel,
    Heading,
    IconButton,
    Input,
    Popover,
    PopoverArrow,
    PopoverCloseButton,
    PopoverContent,
    PopoverTrigger,
    Stack,
    Text,
    useDisclosure,
    useToast,
} from "@chakra-ui/react"
import { TiHeartOutline } from "react-icons/ti"
import React, { useState } from "react"

interface SubscribeButtonProps {
    size: "xl" | "md" | "lg"
}
export function SubscribeButton({ size }: SubscribeButtonProps) {
    const { onOpen, onClose, isOpen } = useDisclosure()
    const [email, setEmail] = useState("")
    const [error, setError] = useState("")
    const toast = useToast()
    const onSend = () => {
        if (!email.toLowerCase().match(/^[\w-.]+@([\w-]+\.)+[a-z]{2,3}$/)) {
            setError("Неверный формат email")
            return
        }
        setError("")
        onClose()
        setTimeout(() => {
            toast({
                title: `Вы подписались на обновления`,
                description: `Теперь вы получите письмо на почту ${email}, как только экскусровод откроет новый тур.`,
                position: "bottom-right",
                variant: "left-accent",
                status: "info",
                isClosable: true,
            })
            setEmail("")
        }, 200)
    }
    return (
        <Popover
            isOpen={isOpen}
            onOpen={onOpen}
            onClose={onClose}
            placement="right"
            closeOnBlur={false}
        >
            <PopoverTrigger>
                <IconButton
                    icon={<TiHeartOutline />}
                    colorScheme={"outline"}
                    top={-1}
                    ml={2}
                    size={size}
                    _hover={{ color: "red" }}
                    _selected={{ color: "red" }}
                    aria-label={"Пожаловаться"}
                    position={"relative"}
                    color={"gray"}
                />
            </PopoverTrigger>
            <PopoverContent p={5}>
                <PopoverArrow />
                <PopoverCloseButton />
                <Stack spacing={4}>
                    <Heading size={"md"}>Подписаться</Heading>
                    <Text fontSize={"sm"} fontWeight={"normal"}>
                        Если вы хотите получать уведомления, когда у
                        экскурсовода появится новая экскурсия, введите ваш email
                    </Text>
                    <FormControl isRequired width="100%" isInvalid={!!error}>
                        <FormLabel mt="3">Email</FormLabel>
                        <Input
                            value={email}
                            onChange={(e) => setEmail(e.target.value)}
                            isInvalid={!!error}
                            type={"email"}
                            placeholder={"myemail@example.com"}
                        />
                        <FormErrorMessage>{error}</FormErrorMessage>
                    </FormControl>

                    <ButtonGroup display="flex" justifyContent="flex-end">
                        <Button variant="outline" onClick={onClose}>
                            Отмена
                        </Button>
                        <Button onClick={onSend} colorScheme="tinkoff">
                            Подписаться
                        </Button>
                    </ButtonGroup>
                </Stack>
            </PopoverContent>
        </Popover>
    )
}
