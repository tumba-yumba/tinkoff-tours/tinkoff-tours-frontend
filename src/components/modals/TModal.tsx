import {
    Button,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalFooter,
    ModalHeader,
    ModalOverlay,
} from "@chakra-ui/react"
import React from "react"
import paperPlane from "../../assets/image/paper_plane.svg"
import vectorFlight from "../../assets/image/vector_flight.svg"
import { css, StyleSheet } from "aphrodite"
interface ModalProps {
    isOpen: boolean
    onClose?: () => void
    onDone: () => void
    children: React.ReactNode
    withPlane?: boolean
    title: React.ReactNode
}
export const styles = StyleSheet.create({
    plane: {
        position: "absolute",
        left: "-55px",
        top: "-25px",
        maxWidth: "100px",
    },
    vector: {
        position: "absolute",
        left: "-50px",
        maxWidth: "280px",
    },
})

export function TModal(props: ModalProps) {
    const { isOpen, onClose, children, withPlane, title, onDone } = props
    const closeWithDone = () => {
        if (onClose) onClose()
        onDone()
    }
    const onCloseInternal = () => {
        if (onClose) onClose()
    }
    return (
        <>
            <Modal
                isCentered
                onClose={onCloseInternal}
                isOpen={isOpen}
                motionPreset="slideInBottom"
            >
                <ModalOverlay />
                <ModalContent>
                    {withPlane && (
                        <img
                            className={css(styles.vector)}
                            src={vectorFlight}
                            alt="vector"
                        />
                    )}
                    <ModalHeader mx={2}>
                        {" "}
                        {withPlane && (
                            <img
                                className={css(styles.plane)}
                                src={paperPlane}
                                alt="plane"
                            />
                        )}
                        {title}
                    </ModalHeader>
                    {onClose && <ModalCloseButton />}
                    <ModalBody>{children}</ModalBody>
                    <ModalFooter>
                        <Button
                            mr={3}
                            size="md"
                            width="100%"
                            onClick={closeWithDone}
                        >
                            Посмотреть экскурсии
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    )
}
