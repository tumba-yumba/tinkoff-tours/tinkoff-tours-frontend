import {
    Box,
    Button,
    Center,
    Flex,
    FormControl,
    FormLabel,
    Heading,
    Icon,
    IconButton,
    Image,
    Modal,
    ModalBody,
    ModalContent,
    ModalFooter,
    ModalOverlay,
    Spinner,
    Textarea,
    useToast,
} from "@chakra-ui/react"
import React, { ChangeEvent, useRef, useState } from "react"
import { RatingInput } from "../raiting/RatingInput"
import {
    DtoEstimationsIn,
    DtoFeedbackPhotoIn,
    DtoFeedbackPhotoOut,
    DtoPhotoOut,
    DtoTourOut,
    useDeleteFeedbacksPhotosMutation,
    usePostFeedbacksPhotosMutation,
    usePostToursByIdFeedbacksMutation,
} from "../../store/api/exportedApi"
import { MdAddAPhoto } from "react-icons/md"
import { MAX_FILE_SIZE_MB, MAX_FILES_PER_REVIEW } from "../../configs"
import { VALID_PHOTO_TYPES } from "../../data/fileTypes"
import { getErrorMessage } from "../../utils/errorParser"
import { CloseIcon } from "@chakra-ui/icons"
interface ReviewModalProps {
    isOpen: boolean
    reviewId: string
    tour: DtoTourOut
    onClose: () => void
}
export function ReviewModal({
    isOpen,
    onClose,
    tour,
    reviewId,
}: ReviewModalProps) {
    const toast = useToast()
    const [loadings, setLoadings] = useState<Array<string>>([])
    const [uploadedPhotos, setUploadedPhotos] = useState<
        Array<DtoFeedbackPhotoOut>
    >([])
    const [uploadFile] = usePostFeedbacksPhotosMutation()
    const [deleteFile] = useDeleteFeedbacksPhotosMutation()
    const [addFeedback] = usePostToursByIdFeedbacksMutation()
    const [message, setMessage] = useState<string>("")
    const [estimations, setEstimations] = useState<DtoEstimationsIn>({})
    const [guideRating, setGuideRating] = useState<number>(0)
    const [tourRating, setTourRating] = useState<number>(0)

    const updateRaiting = (
        type: "guide" | "tour",
        raitng: number,
        estimations: DtoEstimationsIn
    ) => {
        console.log(estimations)
        if (type === "guide") setGuideRating(raitng)
        else setTourRating(raitng)
        setEstimations((prev) => {
            return { ...prev, ...estimations }
        })
    }
    const closeWithDone = () => {
        if (loadings.length > 0) {
            toast({
                title: "Фото ещё загружаются.",
                description: "Дождитесь загрузки, прежде чем продолжить.",
                position: "bottom-right",
                variant: "left-accent",
                status: "warning",
                isClosable: true,
            })
            return
        }
        if (!tourRating || !guideRating) {
            toast({
                title: "Пожалуйста поставьте оцнку, кликнув по звёздачкам.",
                position: "bottom-right",
                variant: "left-accent",
                status: "warning",
                isClosable: true,
            })
            return
        }
        addFeedback({
            id: tour.id!,
            bookingId: reviewId,
            dtoFeedbackIn: {
                comment: message,
                estimations: estimations,
                guide_rating: guideRating,
                photos: uploadedPhotos,
                tour_rating: tourRating,
            },
        })
            .then((resp) => {
                const error = getErrorMessage(resp)
                if (error) throw Error(error)
                toast({
                    title: "Спасибо за ваш отзыв!",
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "info",
                    isClosable: true,
                })
                onClose()
            })
            .catch((err: any) => {
                console.error(err)
                toast({
                    title: `Ошибка.`,
                    description: String(err),
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "warning",
                    isClosable: true,
                })
            })
    }
    const inputFileRef = useRef<HTMLInputElement>(null)

    const selectFile = () => {
        if (inputFileRef && inputFileRef.current !== null) {
            inputFileRef.current.click()
        }
    }

    function deletePhoto(photo: DtoFeedbackPhotoOut) {
        deleteFile({ dtoFeedbackPhotoIn: photo as DtoFeedbackPhotoIn })
            .then((resp: any) => {
                const error = getErrorMessage(resp)
                if (error) throw Error(error)
                setUploadedPhotos((prev) => prev.filter((el) => el !== photo))
            })
            .catch((err) => {
                console.error(err)
                toast({
                    title: `Ошибка при удалении.`,
                    description: String(err),
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "warning",
                    isClosable: true,
                })
            })
    }

    const uploadPhoto = (e: ChangeEvent<HTMLInputElement>) => {
        const photos = e.target.files
        if (!photos) return
        console.log(photos)
        if (
            photos.length >
            MAX_FILES_PER_REVIEW - loadings.length - uploadedPhotos.length
        ) {
            toast({
                title: "Слишком много файлов",
                description: `Максимум ${MAX_FILES_PER_REVIEW} файлов для одного отзыва.`,
                position: "bottom-right",
                variant: "left-accent",
                status: "error",
                isClosable: true,
            })
            return
        }
        for (const photo of photos) {
            if (photo.size / (1024 * 1024) >= MAX_FILE_SIZE_MB) {
                toast({
                    title: "Файл слишком большой",
                    description: `Размер ${photo.name} больше ${MAX_FILE_SIZE_MB}МБ. Выберите файл поменьше.`,
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "warning",
                    isClosable: true,
                })
                continue
            }
            const extension = photo.name.split(".").pop()
            if (
                !extension ||
                VALID_PHOTO_TYPES.indexOf(extension.toLowerCase()) === -1
            ) {
                toast({
                    title: "Не валидный файл",
                    description: `Расширение файла ${photo.name} не валидно. Выберите изображение.`,
                    position: "bottom-right",
                    variant: "left-accent",
                    status: "warning",
                    isClosable: true,
                })
                continue
            }
            const uploadData = new FormData()
            uploadData.append("photo", photo)
            uploadFile({ body: uploadData as unknown as { photo: Blob } })
                .then((resp: any) => {
                    const error = getErrorMessage(resp)
                    if (error) {
                        toast({
                            title: `Ошибка при загрузке ${photo.name}.`,
                            description: error,
                            position: "bottom-right",
                            variant: "left-accent",
                            status: "warning",
                            isClosable: true,
                        })
                    } else {
                        const gotPhoto = resp.data as DtoPhotoOut
                        setUploadedPhotos((prev) => [gotPhoto].concat(prev))
                    }
                    setLoadings((prev) =>
                        prev.filter((load) => load !== photo.name)
                    )
                })
                .catch((err) => {
                    console.error(err)
                    toast({
                        title: `Ошибка при загрузке ${photo.name}.`,
                        description: String(err),
                        position: "bottom-right",
                        variant: "left-accent",
                        status: "warning",
                        isClosable: true,
                    })
                    setLoadings((prev) =>
                        prev.filter((load) => load !== photo.name)
                    )
                })

            setLoadings((prev) => prev.concat([photo.name]))
        }
    }
    const getLoadingsList: Function = (): React.ReactElement[] =>
        loadings.map((el) => (
            <Center
                key={el}
                p={5}
                bg={"gray.300"}
                borderRadius={"lg"}
                h="150px"
                maxW={"150px"}
            >
                <Spinner
                    thickness="4px"
                    speed="0.65s"
                    emptyColor="gray.200"
                    color="purpleT.500"
                    size="xl"
                />
            </Center>
        ))

    const getPhotosList: Function = (): React.ReactElement[] =>
        uploadedPhotos.map((el) => (
            <Box h={"150x"} maxW={"150px"} key={el.id} position={"relative"}>
                <IconButton
                    onClick={() => deletePhoto(el)}
                    aria-label="Удалить картинку"
                    icon={<CloseIcon />}
                    position={"absolute"}
                    top={5}
                    right={5}
                    size="sm"
                    opacity={0.8}
                    colorScheme={"tinkoff"}
                />
                <Image
                    objectFit={"cover"}
                    borderWidth={"1px"}
                    borderStyle={"solid"}
                    borderColor={"gray.300"}
                    w={"150px"}
                    h={"150px"}
                    src={el.path}
                    borderRadius={5}
                />
            </Box>
        ))
    return (
        <>
            <Modal
                onClose={() => {}}
                isOpen={isOpen}
                size={"2xl"}
                motionPreset="slideInBottom"
            >
                <ModalOverlay />
                <ModalContent>
                    <ModalBody>
                        <Flex direction={["column", "row"]} p={2}>
                            <Heading>Отзыв к экскурсии "{tour.title}"</Heading>
                            <Image
                                src={tour.photos![0].path}
                                m={2}
                                w={"100px"}
                                h={"100px"}
                                objectFit={"cover"}
                                objectPosition={"center"}
                                borderRadius={5}
                            />
                        </Flex>
                        <Flex direction={"column"}>
                            <RatingInput
                                type={"tour"}
                                onChange={updateRaiting.bind(null, "tour")}
                            />
                            <RatingInput
                                type={"guide"}
                                onChange={updateRaiting.bind(null, "guide")}
                            />
                        </Flex>
                        <FormControl mt={1}>
                            <FormLabel fontWeight={"bold"}>
                                Ваши впечатления
                            </FormLabel>
                            <Textarea
                                resize={"vertical"}
                                value={message}
                                onChange={(v) => setMessage(v.target.value)}
                            />
                        </FormControl>
                        <FormControl mt={4}>
                            <FormLabel fontWeight={"bold"}>
                                Загрузите до 3-х фотографий
                            </FormLabel>
                            <input
                                type="file"
                                multiple={true}
                                onChange={uploadPhoto}
                                style={{ display: "none" }}
                                ref={inputFileRef}
                            />
                            <Flex direction={["column", "row"]} gap={1}>
                                {loadings.length + uploadedPhotos.length <
                                    MAX_FILES_PER_REVIEW && (
                                    <Center
                                        p={6}
                                        cursor={"pointer"}
                                        borderRadius={"lg"}
                                        borderWidth={"1px"}
                                        borderStyle={"solid"}
                                        borderColor={"gray.300"}
                                        maxW={"150px"}
                                        onClick={selectFile}
                                    >
                                        <Icon
                                            as={MdAddAPhoto}
                                            color={"gray.300"}
                                            boxSize={"100px"}
                                        />
                                    </Center>
                                )}
                                {getLoadingsList()}
                                {getPhotosList()}
                            </Flex>
                        </FormControl>
                    </ModalBody>
                    <ModalFooter>
                        <Flex>
                            <Button
                                mr={3}
                                size="lg"
                                color={"black"}
                                onClick={onClose}
                                colorScheme={"gray"}
                            >
                                Отменить
                            </Button>
                            <Button size="lg" onClick={closeWithDone}>
                                Отправить
                            </Button>
                        </Flex>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </>
    )
}
