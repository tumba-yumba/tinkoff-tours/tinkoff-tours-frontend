import { Container, Img, Text } from "@chakra-ui/react"
import React from "react"
import direction from "../../assets/image/find_direction.svg"
import work from "../../assets/image/work.svg"
import adventure from "../../assets/image/adventure.svg"
import question from "../../assets/image/search_answer.svg"

interface NothingFoundProps {
    help?: string | React.ReactNode
    title?: string | React.ReactNode
    image: Images
}
type Images = "direction" | "construct" | "adventure" | "question"
function NothingFound({ title, help, image }: NothingFoundProps) {
    if (!title) {
        title = "Ничего не найдено :("
    }
    let srcImage = direction
    if (image === "construct") srcImage = work
    if (image === "adventure") srcImage = adventure
    if (image === "question") srcImage = question
    return (
        <Container p={5} mt={2}>
            <Text align={"center"} fontSize="2.5em" fontWeight={600}>
                {title}
            </Text>
            <Text align={"center"} fontWeight={400}>
                {help}
            </Text>
            <Img mt={-6} src={srcImage} />
        </Container>
    )
}
export default NothingFound
