import React, { forwardRef, useImperativeHandle, useRef } from "react"
import "./slider.css"
import { Box } from "@chakra-ui/react"
import Swiper, { ReactIdSwiperChildren } from "react-id-swiper"

interface SliderProps {
    children: ReactIdSwiperChildren
    shouldSwiperUpdate: boolean
    loop: boolean
}

export const Slider = forwardRef((props: SliderProps, ref) => {
    const { children, shouldSwiperUpdate, loop } = props
    const swiperRef = useRef(null)
    useImperativeHandle(ref, () => ({
        goNext() {
            // @ts-ignore
            if (swiperRef.current && swiperRef.current.swiper) {
                // @ts-ignore
                swiperRef.current.swiper.slideNext()
            }
        },
    }))
    return (
        <Box mt={3} mb={1} pb={8} overflow={"hidden"} position={"relative"}>
            <Swiper
                shouldSwiperUpdate={shouldSwiperUpdate}
                pagination={{
                    el: ".swiper-pagination.yellow-tinkoff",
                    type: "bullets",
                    clickable: true,
                }}
                loop={loop}
                ref={swiperRef}
                spaceBetween={30}
                centeredSlides={true}
                navigation={{
                    nextEl: ".swiper-button-next.yellow-tinkoff",
                    prevEl: ".swiper-button-prev.yellow-tinkoff",
                }}
                slidesPerView={"auto"}
            >
                {children}
            </Swiper>
        </Box>
    )
})
