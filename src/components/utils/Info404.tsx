import { Box, Center, Flex, Img, Link, Text } from "@chakra-ui/react"
import error from "../../assets/image/404_error.svg"
import { Link as Href } from "react-router-dom"
import React from "react"

function Info404() {
    return (
        <Flex
            direction={["column-reverse", "column-reverse", "row", "row"]}
            justifyContent={"center"}
        >
            <Img src={error} maxW={["100%", "100%", "50vw"]} />
            <Center>
                <Box>
                    <Text align={"center"} fontSize="200%" fontWeight={600}>
                        Упс! Ничего не найдено
                    </Text>
                    <Text align={"center"} fontWeight={400}>
                        Вероятно, это произошло из-за опечатки в адресе <br />{" "}
                        или неправильной раскладки клавиатуры.
                    </Text>
                    <Text align={"center"} fontWeight={400} mt={5}>
                        Возвращайтесь{" "}
                        <Link color={"purpleT.500"}>
                            <Href to={"/"}> на главную</Href>
                        </Link>
                        , она точно существует :)
                    </Text>
                </Box>
            </Center>
        </Flex>
    )
}

export default Info404
