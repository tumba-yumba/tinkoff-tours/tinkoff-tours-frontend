FROM node:18-alpine as build

WORKDIR /app/

ENV GENERATE_SOURCEMAP=false

ENV DISABLE_ESLINT_PLUGIN=true

COPY package*.json ./

RUN npm install 

COPY . .
