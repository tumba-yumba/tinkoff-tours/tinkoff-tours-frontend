import type { ConfigFile } from "@rtk-query/codegen-openapi"

const config: ConfigFile = {
    schemaFile: "https://tumba-yumba.fun/api/docs/doc.json",
    apiFile: "./src/store/api/emptyApi.ts",
    apiImport: "emptySplitApi",
    outputFile: "./src/store/api/exportedApi.ts",
    exportName: "exportedApi",
    hooks: true,
}

export default config
